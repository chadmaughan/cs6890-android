package com.chadmaughan.cs6890.assignment8.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class MathematiciansProvider extends ContentProvider {

	public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "mathematicians.db";

    public static final String TABLE_NAME = "mathematicians";

    public static final String ID = "id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String RESEARCH = "research";
    public static final String WIKI = "wiki";

	private SQLiteDatabase database;

	public static final Uri CONTENT_URI = Uri.parse("content://com.chadmaughan.cs6890.assignment8a/mathematicians");

	private static final int MATHEMATICIANS = 1;
	private static final int MATHEMATICIAN_ID = 2;

	private static final UriMatcher uriMatcher;
	
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI("com.chadmaughan.cs6890.assignment8a", "mathematicians", MATHEMATICIANS);
		uriMatcher.addURI("com.chadmaughan.cs6890.assignment8a", "mathematicians/#", MATHEMATICIAN_ID);
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int count = 0;
		switch(uriMatcher.match(uri)) {
		
			case MATHEMATICIANS:
				count = database.delete(TABLE_NAME, selection, selectionArgs);
				break;

			case MATHEMATICIAN_ID:
				String segment = uri.getPathSegments().get(1);
				String whereClaus = ID + "=" + segment + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ")" : "");
				count = database.delete(TABLE_NAME, whereClaus, selectionArgs);
				break;
			
			default: 
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		switch(uriMatcher.match(uri)) {
			case MATHEMATICIANS: 
				return "vnd.android.cursor.dir/vnd.chadmaughan.cs6890.assignment8";
			case MATHEMATICIAN_ID: 
				return "vnd.android.cursor.item/vnd.chadmaughan.cs6890.assignment8";
			default: 
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public Uri insert(Uri inuri, ContentValues values) {
		long rowID = database.insert(TABLE_NAME, "quake", values);
		
		if ( rowID > 0 ) {
			Uri uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
			getContext().getContentResolver().notifyChange(uri, null);
			return uri;
		}
		
		throw new SQLException("Failed to insert row into " + inuri);	
	}

	@Override
	public boolean onCreate() {

		Context context = getContext();
		MathematicianDatabaseHelper dbHelper = new MathematicianDatabaseHelper(context);

		database = dbHelper.getWritableDatabase();
		return (database == null) ? false : true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(TABLE_NAME);

		switch(uriMatcher.match(uri)) {
			case MATHEMATICIAN_ID:
				qb.appendWhere(ID + "=" + uri.getPathSegments().get(1));
				break;
			default: 
				break;
		}
		
		String orderBy;
		if(TextUtils.isEmpty(sortOrder)) {
			orderBy = LAST_NAME;
		}
		else {
			orderBy = sortOrder;
		}
		
		Cursor c = qb.query(database, projection, selection, selectionArgs, null, null, orderBy);
		c.setNotificationUri(getContext().getContentResolver(), uri);
		
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		// not used for this assignment
		return 0;
	}

	private static class MathematicianDatabaseHelper extends SQLiteOpenHelper {
	    
		//	<mathematician>
		//		<first_name>Kurt</first_name>
		//		<last_name>Godel</last_name>
		//		<research>mathematical logic</research>
		//		<wiki>http://en.wikipedia.org/wiki/Kurt_Godel</wiki>
		//	</mathematician>
	    private static final String MATHEMATICIAN_TABLE_CREATE =
	                "CREATE TABLE " + TABLE_NAME + " (" +
	                " id INTEGER PRIMARY KEY AUTOINCREMENT, " +
	                " first_name TEXT, " +
	                " last_name TEXT, " +
	                " research TEXT, " +
	                " wiki TEXT);";

	    MathematicianDatabaseHelper(Context context) {
	        super(context, DATABASE_NAME, null, DATABASE_VERSION);
	    }

	    @Override
	    public void onCreate(SQLiteDatabase db) {
	        db.execSQL(MATHEMATICIAN_TABLE_CREATE);
	    }

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}
	}

}
