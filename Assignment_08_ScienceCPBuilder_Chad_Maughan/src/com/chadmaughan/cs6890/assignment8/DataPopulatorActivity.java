package com.chadmaughan.cs6890.assignment8;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.chadmaughan.cs6890.assignment8.provider.MathematiciansProvider;

public class DataPopulatorActivity extends ListActivity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);

		Toast.makeText(getApplicationContext(), "Downloading Mathematicians", Toast.LENGTH_SHORT).show();

		try {

	    	ContentResolver cr = getContentResolver();

	    	// delete anything in the database
	    	cr.delete(MathematiciansProvider.CONTENT_URI, null, null);
	    	
        	DefaultHttpClient hc = new DefaultHttpClient();
	        
	        // load the xml file from the internet
        	// 	make sure and add the android.permission.INTERNET permission to the AndroidManifest.xml
	        HttpGet get = new HttpGet("http://dl.dropbox.com/u/16201259/teaching/ANDROID/mathematicians.xml");
	        HttpResponse httpResponse = hc.execute(get);
	        HttpEntity httpEntity = httpResponse.getEntity();
	        String xml = EntityUtils.toString(httpEntity);
	        
	        Log.v("xml", xml);

	        // parse it
	        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("mathematician");
						
	    	List<String> text = new ArrayList<String>();
	    	
			// populate the database
			for(int i = 0; i < nodes.getLength(); i++) {

				Node node = nodes.item(i);
				NodeList children = node.getChildNodes();

				text.add("*****************************");

				String firstName = children.item(1).getTextContent();
				text.add("First name: " + firstName);
				
				String lastName = children.item(3).getTextContent();
				text.add("Last name: " + lastName);
				
				String research = children.item(5).getTextContent();
				text.add("Research: " + research);

				String wiki = children.item(7).getTextContent();
				text.add("Wiki: " + wiki);

				ContentValues cv = new ContentValues();
				cv.put(MathematiciansProvider.FIRST_NAME, firstName);
				cv.put(MathematiciansProvider.LAST_NAME, lastName);
				cv.put(MathematiciansProvider.RESEARCH, research);
				cv.put(MathematiciansProvider.WIKI, wiki);
				Uri uri = cr.insert(MathematiciansProvider.CONTENT_URI, cv);
				uri.toString();
				Log.v(DataPopulatorActivity.class.getName(), "Adding mathematician: " + firstName + " " + lastName);
			}
			
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item, text);
	        this.getListView().setAdapter(adapter);
	        
	    	String w = "research = 'set theory'";	    	
	    	String[] projections = {"id, first_name", "last_name", "research", "wiki"};
	    	Cursor c = managedQuery(MathematiciansProvider.CONTENT_URI, projections, null, null, null);

	    	if(c != null) {
		    	if(c.moveToFirst()) {
		    		do {
		    			String firstName = c.getString(1);
		    			String lastName = c.getString(2);
		    			String research = c.getString(3);
		    			String wiki = c.getString(4);
	
		    			String s = firstName + " " + lastName + " " + research + " " + wiki;
	
		    			Toast t = Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT);
						t.show();
		    		} 
		    		while ( c.moveToNext() );
		    	}
	    	}	
		}
		catch(Exception e) {
			Log.e("DataPopulatorActivity", "Error retrieving mathematicians", e);
		}
    }
}