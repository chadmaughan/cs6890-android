package com.chadmaughan.cs6890.assignment2;

import java.util.logging.Level;
import java.util.logging.Logger;

public class BinaryTree {
	
	private static Logger logger = Logger.getLogger(BinaryTree.class.getName());
	
	// Root node pointer. Will be null for an empty tree.
	private Node root;
	private Node step;
	
	private int currentStepNumber = 0;
	
	public BinaryTree() {
		root = null;
	}

	private static class Node {

		Node left;
		Node right;
		int number;

		Node(int number) {
			this.left = null;
			this.right = null;
			this.number = number;
		}
	}
	
	public int getCurrentStepNumber() {
		return currentStepNumber;
	}

	public void setCurrentStepNumber(int currentStepNumber) {
		this.currentStepNumber = currentStepNumber;
	}

	public boolean lookup(int data) {
		return (lookup(root, data));
	}

	// recursive lookup
	private boolean lookup(Node node, int data) {
		if (node == null) {
			return (false);
		}

		if (data == node.number) {
			return (true);
		} 
		else if (data < node.number) {
			return (lookup(node.left, data));
		} 
		else {
			return (lookup(node.right, data));
		}
	}

	public void insert(int data) {
		root = insert(root, data);
	}

	// Recursive insert
	private Node insert(Node node, int number) {
		if (node == null) {
			node = new Node(number);
		} 
		else {
			if (number <= node.number) {
				if(logger.isLoggable(Level.INFO))
					logger.info(number + " is LESS than " + node.number + ", inserting LEFT");
				node.left = insert(node.left, number);
			} 
			else {
				if(logger.isLoggable(Level.INFO))
					logger.info(number + " is GREATER than " + node.number + ", inserting RIGHT");
				node.right = insert(node.right, number);
			}
		}

		return node;
	}

	public boolean step(int target) {
		
		boolean targetReached = false;
		
		// you just entered the binary tree
		if(step == null) {
 			step = root;
		}
		else {
			// step right
			if(target > step.number) {
				step = step.right;
				if(logger.isLoggable(Level.INFO))
					logger.info("Step right");
			}
			// step left
			else if(target < step.number) {
				step = step.left;
				if(logger.isLoggable(Level.INFO))
					logger.info("Step left");
			}
			// you've arrived, don't continue
			else {
				targetReached = true;
				if(logger.isLoggable(Level.INFO))
					logger.info("Found target");
			}
		}
		
		currentStepNumber = step.number;
		
		if(logger.isLoggable(Level.INFO))
			logger.info("Reached: " + step.number);

		return targetReached;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(logger.isLoggable(Level.INFO))
			logger.info("Starting up");		
		
		int[] numbers = { 20, 10, 30, 5, 17, 25, 40, 11, 35, 55 };

		BinaryTree bt = new BinaryTree();
		for(int i : numbers) {
			if(logger.isLoggable(Level.INFO))
				logger.info("Inserting: " + i);
			bt.insert(i);
		}
		
		int target = 55;
		
		boolean go = true;
		while(go) {
			boolean reached = bt.step(target);
			if(reached)
				go = false;
		}
	}
}
