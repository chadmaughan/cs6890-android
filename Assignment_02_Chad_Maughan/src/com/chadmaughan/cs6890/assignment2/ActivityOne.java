package com.chadmaughan.cs6890.assignment2;

import android.app.Activity;
import android.os.Bundle;

public class ActivityOne extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataSharingWrapper.step(DataSharingWrapper.ACTIVITY_ONE);
        setResult(Activity.RESULT_OK);
        finish();
    }
}
