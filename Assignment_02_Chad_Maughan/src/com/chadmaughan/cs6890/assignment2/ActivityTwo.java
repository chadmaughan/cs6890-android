package com.chadmaughan.cs6890.assignment2;

import android.app.Activity;
import android.os.Bundle;

public class ActivityTwo extends Activity {
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataSharingWrapper.step(DataSharingWrapper.ACTIVITY_TWO);
        setResult(Activity.RESULT_OK);
        finish();
    }
}
