package com.chadmaughan.cs6890.assignment2;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataSharingWrapper {

	private static Logger logger = Logger.getLogger(DataSharingWrapper.class.getName());
	
	private static BinaryTree binaryTree;

	private static int target = -1;
	
	private static Map<Integer, String> messages = null;

	public static final int ACTIVITY_ONE = 1;
	public static final int ACTIVITY_TWO = 2;
	public static final int ACTIVITY_THREE = 3;

	public static void step(int activity) {
		
		String result = "";

		boolean reached = binaryTree.step(target);
		if(reached) {
			result = "Found target";
		}
		else {
			result = "Reached " + binaryTree.getCurrentStepNumber();
		}

		if(logger.isLoggable(Level.INFO)) 
			logger.info("Placing number: " + activity + ", " + result);
		
		messages.put(activity, result);
	}
	
	public static String getNumber(int activity) {
		String result = messages.get(activity);
		if(logger.isLoggable(Level.INFO))
			logger.info("Returning number: " + activity + ", " + result);
		return result;
	}
	
	public static void setTarget(int target) {
		DataSharingWrapper.target = target;
	}
	
	public static void createBinaryTree() {

		binaryTree = new BinaryTree();
        messages = new HashMap<Integer, String>();
        
        // load up the binary tree (with examples from assignment)
		int[] numbers = { 20, 10, 30, 5, 17, 25, 40, 11, 35, 55 };
		for(int i : numbers) {
			binaryTree.insert(i);
		}		
	}
}
