package com.chadmaughan.cs6890.assignment2;

import java.util.logging.Logger;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Assignment2Activity extends Activity implements OnClickListener {
    
	private static Logger logger = Logger.getLogger(Assignment2Activity.class.getName());

	private static final int ACTIVITY_ONE_REQUEST_CODE = 1;
	private static final int ACTIVITY_TWO_REQUEST_CODE = 2;
	private static final int ACTIVITY_THREE_REQUEST_CODE = 3;

	private Button btnCoRoutine1 = null;
	private Button btnCoRoutine2 = null;
	private Button btnCoRoutine3 = null;
	private Button btnTarget = null;
	
	private EditText txtCoRoutine1 = null;
	private EditText txtCoRoutine2 = null;
	private EditText txtCoRoutine3 = null;
	private EditText txtTarget = null;

	int target = 0;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // create the binary tree and load it
        DataSharingWrapper.createBinaryTree();
        
        // inflate the buttons
        btnCoRoutine1 = (Button) findViewById(R.id.btnCo1);
        btnCoRoutine2 = (Button) findViewById(R.id.btnCo2);
        btnCoRoutine3 = (Button) findViewById(R.id.btnCo3);
        btnTarget = (Button) findViewById(R.id.btnTarget);
        
        // add click listener
        btnCoRoutine1.setOnClickListener(this);
        btnCoRoutine2.setOnClickListener(this);
        btnCoRoutine3.setOnClickListener(this);
        btnTarget.setOnClickListener(this);
        
        // inflate text edit fields
        txtCoRoutine1 = (EditText) findViewById(R.id.corout1);
        txtCoRoutine2 = (EditText) findViewById(R.id.corout2);
        txtCoRoutine3 = (EditText) findViewById(R.id.corout3);
        txtTarget = (EditText) findViewById(R.id.target);

    }
    
	public void onClick(View view) {
		
		Button btn = (Button) view;
		int id = btn.getId();
		
		if (id == R.id.btnCo1) {
			Intent i1 = new Intent(Assignment2Activity.this, ActivityOne.class);
			startActivityForResult(i1, ACTIVITY_ONE_REQUEST_CODE);
		} 
		else if (id == R.id.btnCo2) {
			Intent i2 = new Intent(Assignment2Activity.this, ActivityTwo.class);
			startActivityForResult(i2, ACTIVITY_TWO_REQUEST_CODE);
		} 
		else if (id == R.id.btnCo3) {
			Intent i3 = new Intent(Assignment2Activity.this, ActivityThree.class);
			startActivityForResult(i3, ACTIVITY_THREE_REQUEST_CODE);
		}
		else if (id == R.id.btnTarget) {

			String number = txtTarget.getText().toString();
			try {
				int parsed = Integer.parseInt(number);
				DataSharingWrapper.setTarget(parsed);
				txtTarget.setText("Target is " + number);
			}
			catch(NumberFormatException nfe) {
				txtTarget.setText("Bad number: " + number);
			}
		}
		else {
			logger.info("Unknown button id");
		}
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == ACTIVITY_ONE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				txtCoRoutine1.setText(DataSharingWrapper.getNumber(DataSharingWrapper.ACTIVITY_ONE));
			} 
			else {
				txtCoRoutine1.setText("Failed");
			}
		} 
		else if (requestCode == ACTIVITY_TWO_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				txtCoRoutine2.setText(DataSharingWrapper.getNumber(DataSharingWrapper.ACTIVITY_TWO));
			} 
			else {
				txtCoRoutine2.setText("Failed");
			}
		} 
		else if (requestCode == ACTIVITY_THREE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				txtCoRoutine3.setText(DataSharingWrapper.getNumber(DataSharingWrapper.ACTIVITY_THREE));
			} 
			else {
				txtCoRoutine3.setText("Failed");
			}
		}
	}
}