package com.chadmaughan.cs6890.assignment8;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;

public class SearchActivity extends Activity {

	private EditText editTextSearch;

	public static final Uri CONTENT_URI = Uri.parse("content://com.chadmaughan.cs6890.assignment8a/mathematicians");

	@Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);

    	String w = "research = 'set theory'";
    	
    	ContentResolver cr = getContentResolver();
    	Cursor c = cr.query(CONTENT_URI, null, w, null, null);
	}
}
