package com.chadmaughan.cs6890.assignment4;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

public class QuatrainActivity extends ListActivity implements OnInitListener {

    private static final int REQ_TTS_STATUS_CHECK = 0;
    private static TextToSpeech mTts = null;
    
	private static String[] text = null;
	private String mode;

	@Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		Intent i = this.getIntent();
		String key = i.getExtras().getString("key");
		String m = i.getExtras().getString("mode");
		
		if(m != null) {
			if(m.equals("listen")) {
				mode = m;
		        setContentView(R.layout.display_listen);
			}
			else {
				mode = m;
		        setContentView(R.layout.display_read);
			}
		}
		else {
			mode = "read";
	        setContentView(R.layout.display_read);
		}
		
		Resources resources = getResources();
        String[] rumiNumbers = resources.getStringArray(R.array.list);

		Log.v("DisplayActivity", "using key: " + key);

        // q77
		if(key.equals(rumiNumbers[0])) {
			text = resources.getStringArray(R.array.q77);
		}
		// q82
		else if(key.equals(rumiNumbers[1])) {
			text = resources.getStringArray(R.array.q82);
		}
		// q116
		else if(key.equals(rumiNumbers[2])) {
			text = resources.getStringArray(R.array.q116);
		}
		// q494
		else if(key.equals(rumiNumbers[3])) {
			text = resources.getStringArray(R.array.q494);
		}
		// q1082
		else if(key.equals(rumiNumbers[4])) {
			text = resources.getStringArray(R.array.q1082);
		}
		else {
			Log.v("DisplayActivity", "unknown key: " + key);
		}
		
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, text);
        this.getListView().setAdapter(adapter);

        startTTS();
	}
	
    private void startTTS() {
        Intent checkTTSIntent = new Intent();

        // Check if the TTS data are installed.
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, REQ_TTS_STATUS_CHECK);
    }
    
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		if(mode.equals("listen")) {
			super.onCreateOptionsMenu(menu);
			menu.add(0,1,0,"Read");
			return true;
		}
		else {
			return false;
		}
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(mode.equals("listen")) {
			switch(item.getItemId()) {
				
				case 1:
					StringBuffer buffer = new StringBuffer();
					for(String s : text) {
						buffer.append(s);
						buffer.append(" ");
					}
					mTts.speak(buffer.toString(), TextToSpeech.QUEUE_FLUSH, null);
					break;
	
				default:
					break;
			}
		}
		return true;
	}

	public void onInit(int status) {
	}

	// Called when startActivityForResult above returns
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	
        if(requestCode == REQ_TTS_STATUS_CHECK) {

        	switch ( resultCode ) {

        		// If the TTS data are installed, create a TTS object.
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_PASS:
	                mTts = new TextToSpeech(this, this);
	                Log.v("QuatrainActivity", "TTS is installed okay");
	                break;
	                
	            // If there is a data problem, let the user know through a Log message and
	            // try to install the data.
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA:
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA:
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME:
	                Log.v("QuatrainActivity", "Need to install language stuff: " + resultCode);
	                Intent installIntent = new Intent();
	                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
	                startActivity(installIntent);
	                break;
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL:
	                default:
	                    Log.e("TTS", "Failure: TTS may not be available");
            }
        }
        else {
            Log.v("QuatrainActivity", "Unprocessed TTS STATUS CODE");
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (mTts != null) {
            mTts.stop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTts != null) {
            mTts.shutdown();
        }
    }
}
