package com.chadmaughan.cs6890.assignment4;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class SearchActivity extends Activity implements OnInitListener {

    private static final int REQ_TTS_STATUS_CHECK = 0;
    private static TextToSpeech mTts = null;

	private static String[] text = null;
	private EditText editTextSearch;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);
        
		Intent i = this.getIntent();
		String key = i.getExtras().getString("key");

		Resources resources = getResources();
        String[] rumiNumbers = resources.getStringArray(R.array.list);

		Log.v("DisplayActivity", "using key: " + key);

        // q77
		if(key.equals(rumiNumbers[0])) {
			text = resources.getStringArray(R.array.q77);
		}
		// q82
		else if(key.equals(rumiNumbers[1])) {
			text = resources.getStringArray(R.array.q82);
		}
		// q116
		else if(key.equals(rumiNumbers[2])) {
			text = resources.getStringArray(R.array.q116);
		}
		// q494
		else if(key.equals(rumiNumbers[3])) {
			text = resources.getStringArray(R.array.q494);
		}
		// q1082
		else if(key.equals(rumiNumbers[4])) {
			text = resources.getStringArray(R.array.q1082);
		}
		else {
			Log.v("DisplayActivity", "unknown key: " + key);
		}

        startTTS();

	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0,1,0,"Search");
		return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		boolean foundIt = false;
		
		String s = editTextSearch.getText().toString();
		String sss = "";
		for(String ss : text) {
			if(ss.contains(s)) {
				sss = ss;
				foundIt = true;
			}
		}
		
		if(foundIt) {
			mTts.speak(sss, TextToSpeech.QUEUE_FLUSH, null);
			Toast t = Toast.makeText(getApplicationContext(), sss, Toast.LENGTH_SHORT);
			t.show();
		}
		else {
			String error = "I did not find your text";
			mTts.speak(error, TextToSpeech.QUEUE_FLUSH, null);
			Toast t = Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT);
			t.show();
		}

		return true;
	}

    private void startTTS() {
        Intent checkTTSIntent = new Intent();

        // Check if the TTS data are installed.
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, REQ_TTS_STATUS_CHECK);
    }

	public void onInit(int status) {
	}

	// Called when startActivityForResult above returns
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	
        if(requestCode == REQ_TTS_STATUS_CHECK) {

        	switch ( resultCode ) {

        		// If the TTS data are installed, create a TTS object.
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_PASS:
	                mTts = new TextToSpeech(this, this);
	                Log.v("QuatrainActivity", "TTS is installed okay");
	                break;
	                
	            // If there is a data problem, let the user know through a Log message and
	            // try to install the data.
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA:
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA:
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME:
	                Log.v("QuatrainActivity", "Need to install language stuff: " + resultCode);
	                Intent installIntent = new Intent();
	                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
	                startActivity(installIntent);
	                break;
	            case TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL:
	                default:
	                    Log.e("TTS", "Failure: TTS may not be available");
            }
        }
        else {
            Log.v("QuatrainActivity", "Unprocessed TTS STATUS CODE");
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (mTts != null) {
            mTts.stop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTts != null) {
            mTts.shutdown();
        }
    }

}
