package com.chadmaughan.cs6890.assignment4;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

public class DisplayActivity extends ListActivity {

	private static String[] text = null;

	@Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		Intent i = this.getIntent();
		String key = i.getExtras().getString("key");

		Resources resources = getResources();

		Log.v("DisplayActivity", "using key: " + key);

        // bio
		if(key.equals("bio")) {
	        setContentView(R.layout.bio);
			text = resources.getStringArray(R.array.rumi_bio);
		}
		// works
		else if(key.equals("works")) {
	        setContentView(R.layout.works);
			text = resources.getStringArray(R.array.rumi_works);
		}
		else {
			Log.v("DisplayActivity", "unknown key: " + key);
		}
		
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, text);
        this.getListView().setAdapter(adapter);

	}    
}
