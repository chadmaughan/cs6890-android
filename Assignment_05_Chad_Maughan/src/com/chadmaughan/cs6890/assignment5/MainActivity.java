package com.chadmaughan.cs6890.assignment5;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

	private final static int OPEN_LINK = 200;
	private final static int SHOW_LINK = 201;
	
	ArrayList<Mathematician> mathematicians = null;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Resources resources = getResources();

        String[] names = resources.getStringArray(R.array.mathematicians);
        String[] links = resources.getStringArray(R.array.links);

        // build the mathematicians
        mathematicians = new ArrayList<Mathematician>();

        for(int i = 0; i < names.length; i++) {
        	String name = names[i];
        	String url = links[i];
        	
        	// xml file is of format: lastname, firstname
        	String[] parts = name.trim().split(", ");

        	mathematicians.add(new Mathematician(parts[1], parts[0], url));
        }
        
        MathematicianViewArrayAdapter adapter = new MathematicianViewArrayAdapter(this, R.layout.mathematician_view, mathematicians);
        this.getListView().setAdapter(adapter);
        		
        // allow for long-click context menu
        registerForContextMenu(this.getListView());
    }
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {

		// get the position of the mathematician
		AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
		Mathematician m = mathematicians.get(adapterContextMenuInfo.position);

		Log.v("MainActivity", "Displaying menu for: " + m.getFullName());

		menu.setHeaderTitle(m.getFullName());
		menu.add(200, OPEN_LINK, Menu.NONE, "Go to wiki page");
		menu.add(200, SHOW_LINK , Menu.NONE, "Show wiki link");
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {		

		// get the position of the mathematician
		AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		Mathematician m = mathematicians.get(menuInfo.position);
		
		String url = m.getUrl();
		
		switch(item.getItemId()) {
		
			// open browser
			case OPEN_LINK:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(browserIntent);
				break;
	
			// show link
			case SHOW_LINK:
				Toast t = Toast.makeText(getApplicationContext(), url, Toast.LENGTH_SHORT);
				t.show();
				break;
				
			default:
				break;
		}

		return true;
	}
}