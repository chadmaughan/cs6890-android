package org.vkedco.android.domsamples;

/*
 ***************************************************************
 * DOMSamplesAct is the main activity of the DOMSamples application
 * that demonstrates how to create a DOM XML document, how to
 * parse an XML document, and how to save and access files
 * in /res/raw/. 
 * 
 * Set the value of xml_doc_num in /res/values/params.xml to
 * 1 to generate a DOM document programmatically and set it
 * to 2 to parse the XML file in /res/raw/mathematicians.xml 
 *
 * Errors, comments to vladimir dot kulyukin at gmail dot com
 ***************************************************************
 */

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TextView;
import org.w3c.dom.*;

public class DOMSamplesAct extends Activity {
    private Resources mRes;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mRes = getResources();
        int doc_num = mRes.getInteger(R.integer.xml_doc_num);
        TextView xml_doc_contents =
        	(TextView)findViewById(R.id.xml_doc_contents);
        switch ( doc_num ) {
        case 1: 
        	// XMLDocBuilder.buildDocOne generates a DOM document. If
        	// doc1 is generated successfully, parseXMLDoc(doc1)
        	// converts it into a formatted string that is
        	// displayed in xml_doc_contents.
        	Document doc1 = XMLDocBuilder.buildDocOne(this.getApplicationContext());
        	if ( doc1 == null ) 
        		xml_doc_contents.setText(mRes.getString(R.string.xml_doc_build_failure));
        	else
        		xml_doc_contents.setText(parseXMLDoc(doc1));
        	break;
        case 2:
        	// XMLDocBuilder.buildDocTwo parses an XML document with
        	// DOM. If doc2 is parsed successfully, xml_doc_contents
        	// is updated with an appropriate message.
        	Document doc2 = XMLDocBuilder.buildDocTwo(this.getApplicationContext());
        	if ( doc2 != null ) 
        		xml_doc_contents.setText("XML document parsed");
        	else
        		xml_doc_contents.setText(mRes.getString(R.string.xml_doc_parse_failure));
        }
    }
    
    // convert a DOM document into a formatted string.
    private String parseXMLDoc(Document doc) {
    	StringBuffer buf = new StringBuffer();
    	Element mathematician_elem = doc.getDocumentElement();
    	if ( mathematician_elem != null ) {
    		buf.append(mathematician_elem.getTagName());
    		NodeList children = mathematician_elem.getChildNodes();
    		for(int i = 0; i < children.getLength(); i++) {
    			Node child = children.item(i);
    			if ( child.getNodeType() == Node.COMMENT_NODE) {
    				buf.append("\n\tComment:");
    				buf.append("\n\t\t" + "value:\t\t\t\t" + 
    						child.getNodeValue());
    			}
    			else if ( child.getNodeType() == Node.ELEMENT_NODE) {
    				buf.append("\n\tElement:");
    				Element el = (Element) child;
    				buf.append("\n\t\t" + "tag:\t\t\t\t" + 
    						el.getTagName());
    				buf.append("\n\t\t" + "dates:\t\t\t\t" + 
    						el.getFirstChild().getNodeValue());
    				buf.append("\n\t\t" + "research area:\t" + 
    						el.getAttribute(mRes.getString(R.string.research_area_attrib)));
    			}
    			else {
    				buf.append("\n\tUnknown:\tunknown");
    			}
    		}
    		return buf.toString();
    	}
    	else {
    		buf.append(mRes.getString(R.string.xml_doc_parse_failure));
    	}
    	
    	
    	return buf.toString();
    }
}