package org.vkedco.android.domsamples;

/*
 ***************************************************************
 * XMLDocBuilder is the main activity of the DOMSamples application
 * that demonstrates how to create a DOM XML document programmatically
 * (buildDocOne) and how to parse an XML document (buildDocTwo).
 *
 * Errors, comments to vladimir dot kulyukin at gmail dot com
 ***************************************************************
 */

import java.io.InputStream;
import org.w3c.dom.*;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class XMLDocBuilder {
	
	// builds a DOM XML document the XML version of which
	// is /res/raw/mathematicians.xml
	public static Document buildDocOne(Context cntxt) {
		
		try {
			Resources res = cntxt.getResources();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();
			// Create a mathematician element
			Element mathematician = 
				doc.createElement(res.getString(R.string.mathematician_tag));
			// Create a comment comment
			Comment mathematician_comment = 
				doc.createComment("class of mathematicians");
			// Append the comment to the mathematician element
			mathematician.appendChild(mathematician_comment);
			// Append the mathematician element to the document
			doc.appendChild(mathematician);
			
			String research_area_attrib = 
				res.getString(R.string.research_area_attrib);
			// Create Georg Cantor's element
			Element georg_cantor = 
				doc.createElement(res.getString(R.string.georg_cantor_tag));
			// Create Georg Cantor's comment
			Comment georg_cantor_comment =
				doc.createComment("1845 - 1918");
			// Append the comment to the element as a child
			georg_cantor.appendChild(georg_cantor_comment);
			// Set the research area attribute
			georg_cantor.setAttribute(research_area_attrib, "set theory");
			// Append Georg Cantor's element to the mathematician root
			// element
			mathematician.appendChild(georg_cantor);
			
			// Do the same with Giuseppe Peano's element and David
			// Hilbert's element
			Element giuseppe_peano = 
				doc.createElement(res.getString(R.string.giuseppe_peano_tag));
			Comment giuseppe_peano_comment = 
				doc.createComment("1858 - 1932");
			giuseppe_peano.appendChild(giuseppe_peano_comment);
			giuseppe_peano.setAttribute(research_area_attrib, "number theory");
			mathematician.appendChild(giuseppe_peano);
			
			Element david_hilbert = 
				doc.createElement(res.getString(R.string.david_hilbert_tag));
			Comment david_hilbert_comment =
				doc.createComment("1862 - 1943");
			david_hilbert.appendChild(david_hilbert_comment);
			david_hilbert.setAttribute(research_area_attrib, "mathematical logic");
			mathematician.appendChild(david_hilbert);
			
			return doc;
		}
		catch (Exception e) {
			Log.e("DOM Exception", e.toString());
			return null;
		}
		
	}
	
	// Parse the XML document in R.raw.mathematicians 
	// (/res/raw/mathematicians.xml) and build a DOM document
	public static Document buildDocTwo(Context cntxt) {
		
		try {
			Resources res = cntxt.getResources();
			InputStream in = res.openRawResource(R.raw.mathematicians);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder parser = factory.newDocumentBuilder();
			Document doc = parser.parse(in);
			in.close();
			return doc;
		}
		catch (Exception e) {
			Log.e("DOM Exception", e.toString());
			return null;
		}
		
	}

}
