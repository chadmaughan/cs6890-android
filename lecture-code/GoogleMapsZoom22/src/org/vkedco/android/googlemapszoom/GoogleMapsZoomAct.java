package org.vkedco.android.googlemapszoom;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import android.os.Bundle;
import android.view.View;

public class GoogleMapsZoomAct extends MapActivity {
	
	private MapView mapView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mapView = (MapView)findViewById(R.id.mapview);
    }
    
    public void handleZoomClick(View v) {
    	switch ( v.getId() ) {
    	case R.id.zoomInBtn:
    		mapView.getController().zoomIn();
    		break;
    	case R.id.zoomOutBtn:
    		mapView.getController().zoomOut();
    		break;
    	}
    }

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}