package org.vkedco.android.startedsumlooperservice;

/*
 *************************************************************
 * Activity that consumes the StartedSumLooperService defined
 * in StartedSumLooperService.java. The service uses Handlers 
 * and Loopers to compute the sum of the first n catalan or 
 * fibonacci numbers and posts the results in notifications 
 * in the status bar.
 *  
 * Reference: http://en.wikipedia.org/wiki/Catalan_number
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************************
 */

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.os.Process;

public class StartedSumLooperServiceAct extends Activity 
	implements OnClickListener
{
    private Button mCatSumBtn;
	private Button mFibSumBtn;
	private Button mClearBtn;
	private Button mCrashBtn;
	private EditText mNEdtTxt;
	private Resources mRes;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		mCatSumBtn = (Button) findViewById(R.id.btnCatalanSum);
		mCatSumBtn.setOnClickListener(this);
		
		mFibSumBtn = (Button) findViewById(R.id.btnFibSum);
		mFibSumBtn.setOnClickListener(this);
		
		mClearBtn = (Button) findViewById(R.id.btnClear);
		mClearBtn.setOnClickListener(this);
		
		mCrashBtn = (Button) findViewById(R.id.btnCrash);
		mCrashBtn.setOnClickListener(this);
		
		mNEdtTxt = (EditText) findViewById(R.id.edtxtN);
		
		mRes = getResources();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		stopService(new Intent(getApplicationContext(),
				StartedSumLooperService.class));
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCatalanSum:
			Intent i1 = 
				new Intent(getApplicationContext(), 
						StartedSumLooperService.class);
			long n1 = Long.parseLong(mNEdtTxt.getText().toString().toString());
			i1.putExtra(mRes.getString(R.string.cat_key), true);
			i1.putExtra(mRes.getString(R.string.n_key), n1);
			startService(i1);
			break;
		case R.id.btnFibSum:
			Intent i2 = 
				new Intent(getApplicationContext(), 
						StartedSumLooperService.class);
			long n2 = Long.parseLong(mNEdtTxt.getText().toString().toString());
			i2.putExtra(mRes.getString(R.string.fib_key), true);
			i2.putExtra(mRes.getString(R.string.n_key), n2);
			startService(i2);
			break;
		case R.id.btnClear:
			mNEdtTxt.setText("");
			break;
		case R.id.btnCrash:
			Process.killProcess(Process.myPid());
			break;
		}
	}
}