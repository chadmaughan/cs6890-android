package org.vkedco.android.startedsumlooperservice;

/*
 *************************************************************
 * Service that uses Handlers and Loopers to compute
 * the sum of the first n catalan or fibonacci numbers and
 * posts the results in notifications in the status bar.
 *  
 * Reference: http://en.wikipedia.org/wiki/Catalan_number
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************************
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

public class StartedSumLooperService extends Service {
	
	private NotificationManager mNoteMngr;
	private Resources mRes;
	private volatile Looper mServiceLooper;
	private volatile ServiceHandler mServiceHandler;
	private long mSleepInterval = 1000L;
	
	private final class ServiceHandler extends Handler {
		
		public ServiceHandler(Looper looper) {
			super(looper);
		}
		
		// handleMessage is called when ServiceHandler.sendMessage(Message)
		// is executed.
		@Override
		public void handleMessage(Message msg) {
			// 1. Retrieve the Bundle from msg.obj
			Bundle args = (Bundle)msg.obj;
			String catSumKey = mRes.getString(R.string.cat_key);
			String fibSumKey = mRes.getString(R.string.fib_key);
			String nKey = mRes.getString(R.string.n_key);
			
			// 2. Check if the bundle contains (cat_key or fib_key) and
			// n_key
			if ( (args.containsKey(catSumKey) || args.containsKey(fibSumKey))
					&&
				  args.containsKey(nKey) )  {
				long n = args.getLong(nKey);
				// 3. if the bundle contains cat_key
				if ( args.containsKey(catSumKey) ) {
					synchronized ( this ) {
						// 4. post a notification message in the status bar
						// with the result
						poastNotificationMessage(compileCatNote(n, catalanSum(n)));
					}
				}
				else {
					synchronized ( this ) {
						// 5. post a notification message in the status bar
						// with the result
						poastNotificationMessage(compileFiboNote(n, fiboSum(n)));
					}
				}
			}
			else {
				poastNotificationMessage("Wrong arguments");
			}
			
			// 6. wait for a second
			synchronized ( this ) {
				try {
					wait(1000L);
				}
				catch ( Exception ex ) {
					
				}
			}
			
			// 7. remove the image from the status bar
			mNoteMngr.cancel(R.string.service_id);
		}
	}
	
	// Threads should be created in onCreate(), because it is
	// called only once.
	@Override
	public void onCreate() {
		// 1. Get a Notification Manager
		mNoteMngr = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		// 2. Create a new HandlerThread object
		HandlerThread handlerThr = 
			new HandlerThread("StartedSumLooperServiceThread",
							  Process.THREAD_PRIORITY_BACKGROUND);
		// 3. Start the HandlerThread
		handlerThr.start();
		// 4. Get the HandlerThread's Looper
		mServiceLooper = handlerThr.getLooper();
		// 5. Create a Handler by passing it the created Looper
		mServiceHandler = new ServiceHandler(mServiceLooper);
		mRes = getResources();
		poastNotificationMessage("creating sum looper service");
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// onStartCommand run when the service starts
		// 1. Obtain a Message object from the Handler
		Message msg = mServiceHandler.obtainMessage();
		// 2. Place startId as the first argument of the message
		msg.arg1 = startId;
		// 3. Place flags as the second argument of the message
		msg.arg2 = flags;
		// 4. Place the intent's bundle into the obj slot of
		// the message
		msg.obj = intent.getExtras();
		// 5. Send the message to the Handler.
		mServiceHandler.sendMessage(msg);
		// mServiceHandler.post(new Runnable() { ... });
		// 6. Post the notification message that the sum looper
		// service has started
		poastNotificationMessage("starting sum looper service");
		return START_NOT_STICKY;
	}
	
	@Override
	public void onDestroy() {
		poastNotificationMessage("stopping sum looper service");
		mServiceLooper.quit();
		mNoteMngr.cancel(R.string.service_id);	
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void poastNotificationMessage(String msg) {
		Notification note = new Notification(R.drawable.ornament_00, msg,
				System.currentTimeMillis());
		PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),
				0, new Intent(getApplicationContext(),
						StartedSumLooperServiceAct.class), 0);
		note.setLatestEventInfo(getApplicationContext(),
				"StartedSumLooperService", msg, pi);
        note.flags |= Notification.FLAG_ONGOING_EVENT;
		this.mNoteMngr.notify(R.string.service_id, note);
	}
	
	private long catalanSum(long n) {
		if ( n == 0L ) {
			return 1L;
		}
		else {
			long prevC = 1L;
			long sum   = 1L;
			for(long i = 1; i <= n; i++) {
				prevC *= (4 * i - 2);
				prevC /= (i + 1);
				sum += prevC;
				synchronized ( this ) {
					try {
						Thread.sleep(mSleepInterval);
					}
					catch ( Exception ex ) {
						
					}
				}
			}
			return sum;
		}
	}
	
	private long fiboSum(long n) {
		if ( n == 0L ) {
			return 1L;
		}
		else if ( n == 1L ) {
			return 2L;
		}
		else {
			long prev1 = 1L;
			long prev2 = 1L;
			long sum = 2L;
			for(int i = 2; i <= n; i++) {
				long next = prev1 + prev2;
				sum += next;
				prev1 = prev2;
				prev2 = next;
				synchronized ( this ) {
					try {
						Thread.sleep(mSleepInterval);
					}
					catch ( Exception ex ) {
						
					}
				}
			}
			return sum;
		}
	}
	
	private String compileCatNote(long n, long sum) {
		return "CatalanSum(" + n + ") = " + sum; 
	}
	
	private String compileFiboNote(long n, long sum) {
		return "FibSum(" + n + ")= " + sum;
	}

}
