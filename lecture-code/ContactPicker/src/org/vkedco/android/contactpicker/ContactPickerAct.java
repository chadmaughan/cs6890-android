package org.vkedco.android.contactpicker;

/*
 * ***********************************************
 * Slightly modified source code from Ch. 5 of 
 * "Professional Android 2 Application Development" 
 * by Rito Meier. Original source code is
 * available for download from www.wrox.com
 * ***********************************************
 */

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts.People;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.AdapterView.OnItemClickListener;

public class ContactPickerAct extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Intent intent = getIntent();
        String dataPath = intent.getData().toString();
        
        final Uri data = Uri.parse(dataPath + "people/");
        final Cursor c = managedQuery(data, null, null, null, null);
            
        // People.NAME has been depricated
        String[] from = new String[] {People.NAME};
        int[]  to = new int[] { R.id.itemTextView };
        
        // Example of a SimpleCursorAdapter construction
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                                                              R.layout.listitemlayout,
                                                              c,
                                                              from,
                                                              to);
        ListView lv = (ListView)findViewById(R.id.contactListView);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
          public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
            // Move the cursor to the selected item
            c.moveToPosition(pos);
            // Extract the row id.
            int rowId = c.getInt(c.getColumnIndexOrThrow("_id"));
            // Construct the result URI.
            Uri outURI = Uri.parse(data.toString() + rowId);
            Intent outData = new Intent();
            outData.setData(outURI);
            setResult(Activity.RESULT_OK, outData);
            finish();
          }
        });
      }
 }