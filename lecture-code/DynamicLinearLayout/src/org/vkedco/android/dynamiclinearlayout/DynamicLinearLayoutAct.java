package org.vkedco.android.dynamiclinearlayout;

/*
 * *************************************************************** 
 * File: DynamicLinearLayoutAct.java
 * DynamicLinearLayoutAct inflates a GUI laid out in main.xml.
 * The GUI allows the user to dynamically change the orientation
 * and padding properties of a linear layout with three
 * text views. It gives an example of how to specify
 * a radio group in XML and attach back end Java functionality
 * to it.
 * 
 * Bugs, comments to vladimir dot kulyukin at usu dot edu
 * **************************************************************
 */

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class DynamicLinearLayoutAct extends Activity
{
	// Inner listener class OrinetationListener implements 
	// the OnCheckedChangeListener interface. The interface
	// captures events of radio buttons being checked.
	// This listener will be attached to the first
	// radio group whose radio buttons control the orientation
	// property of the linear layout.
	class OrientationListener implements OnCheckedChangeListener
	{
		// these are the two elements that will be inflated from
		// the XML layout in main.xml
		private RadioGroup mRG = null;
		private LinearLayout mLL = null;
		
		public OrientationListener(RadioGroup rg, LinearLayout ll) {
			mRG = rg;
			mLL = ll;
		}
		
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			switch ( checkedId ) {
			// if the horizontal radio button is selected,
			// the orientation of the linear layout with three text
			// views is set to LinearLayout.HORIZONTAL.
			case R.id.horizontal_rbutton: {
				mLL.setOrientation(LinearLayout.HORIZONTAL);
				break;
			}
			// if the vertical radio button is selected,
			// the orientation of the linear layout with three text
			// views is set to LinearLayout.VERTICAL.
			case R.id.vertical_rbutton: {
				mLL.setOrientation(LinearLayout.VERTICAL);
				break;
			}	

			}
		}
	}
	
	// Inner listener class PaddingListener also implements 
	// the OnCheckedChangeListener interface. 
	// This listener will be attached to the second
	// radio group whose radio buttons control the padding
	// property of the linear layout.
	class PaddingListener implements OnCheckedChangeListener
	{
		private RadioGroup mRG = null;
		private LinearLayout mLL = null;
		
		public PaddingListener(RadioGroup rg, LinearLayout ll) {
			mRG = rg;
			mLL = ll;
		}
		
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			switch ( checkedId ) {
			case R.id.padd_5_rbutton: {
				// set left, top, right, and bottom padding to 5 pix
				mLL.setPadding(5, 5, 5, 5);
				break;
			}
			
			case R.id.padd_10_rbutton: {
				// set left, top, right, and bottom padding to 10 pix
				mLL.setPadding(10, 10, 10, 10);
				break;
			}
			
			case R.id.padd_15_rbutton: {
				// set left, top, right, and bottom padding to 15 pix
				mLL.setPadding(15, 15, 15, 15);
				break;
			}
			
			}
		}
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        RadioGroup selectOrientation = null;
    	RadioGroup selectPadding = null;
    	LinearLayout linLayout = null;
        
    	// inflate the linear layout object with three text views
        linLayout = (LinearLayout)findViewById(R.id.lin_layout);
    	
        // inflate the radio group whose radio buttons will control
        // the orientation property of the linLayout inflated object.
        selectOrientation = (RadioGroup)findViewById(R.id.select_orientation);
        // create an OrientationListener object and set it as the
        // event listener of the selectOrientation radio group.
        OnCheckedChangeListener selectionListener = 
        	new OrientationListener(selectOrientation, linLayout);
        selectOrientation.setOnCheckedChangeListener(selectionListener);
        
        // inflate the radio group whose radio buttons will control
        // the padding property of linLayout object.
        selectPadding = (RadioGroup)findViewById(R.id.select_padding);
        // create a PaddingListener object and set it as the
        // event listener of the selectPadding radio group.
        OnCheckedChangeListener paddingListener =
        	new PaddingListener(selectPadding, linLayout);
        selectPadding.setOnCheckedChangeListener(paddingListener);
    }
}