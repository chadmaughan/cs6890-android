package org.vkedco.android.sciencecpconsumer;

/*
 ***********************************************************
 * ScienceCPConsumerAct is a component of the 
 * ScienceCPConsumer application. The application retrieves
 * mathematicians' names from the content provider 
 * constructed by the ScienceCPBuilder application.
 * The activity allows the user to enter a research
 * area (e.g., "set theory") and retrieves the name of
 * a mathematician whose research area matches the
 * user's entered string.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
*************************************************************
*/

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ScienceCPConsumerAct extends Activity 
{
	private Button searchBtn;
	private Button clearBtn;
	private EditText srchArea;
	private Uri content_uri;
	private ContentResolver cr;
	private static final int COL_FN_IX = 1;
	private static final int COL_LN_IX = 2;
	private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Resources res = getResources();
        searchBtn = (Button)findViewById(R.id.srch_button);
        clearBtn = (Button)findViewById(R.id.clr_button);
        srchArea = (EditText)findViewById(R.id.srch_input);
        content_uri = Uri.parse(res.getString(R.string.content_provider));
        cr = getContentResolver();
        context = this.getApplicationContext();
        
        searchBtn.setOnClickListener(new OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				Editable e = srchArea.getText();
				String str = e.toString();
				String msg = "";
				try {
					Cursor c =
						cr.query(content_uri, null, 
								"research=?", 
								new String[] { str }, 
								null);
					Log.v("CHECK 1", "CHECK 1");
					if ( c == null ) {
						msg += "Null cursor";
					}
					else if  ( c.moveToFirst() ) {
						String fn = c.getString(COL_FN_IX);
						String ln = c.getString(COL_LN_IX);
						msg += fn;
						msg += " ";
						msg += ln;
					}
					else {
						msg += "No records in cursor";
					}
				}
				catch ( Exception ex ) {
					srchArea.setText("");
					msg += "Database Exception";
				}
				Toast t = Toast.makeText(context, msg, Toast.LENGTH_LONG);
				t.show();
			}	
        });
        
        clearBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				srchArea.setText("");
			}	
        });
    }
    
}