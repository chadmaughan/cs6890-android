package org.vkedco.android.sharedprefs2;

/*
 ***************************************************************
 * SharedPrefsReceiver is loads and toasts the SharedPreferences
 * object persisted by SharedPrefsSaver.
 * 
 * Errors, comments to vladimir dot kulyukin at gmail dot com
 ***************************************************************
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class SharedPrefsReceiver extends Activity {
	
	private static SharedPreferences sharedPrefs = null;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctxt = getApplicationContext();
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(ctxt);   
        toastPrefs();
	}
	
	private void toastPrefs() {
    	Resources res = getResources();
    	String pref1_key = res.getString(R.string.pref1_key);
    	String pref2_key = res.getString(R.string.pref2_key);
    	Toast pref1Toast = Toast.makeText(this, 
    			pref1_key + " = " + sharedPrefs.getString(pref1_key, ""),
    			Toast.LENGTH_LONG);
    	Toast pref2Toast = Toast.makeText(this,
    			pref2_key + " = " + sharedPrefs.getString(pref2_key, ""),
    			Toast.LENGTH_LONG);
    	pref1Toast.show();
    	pref2Toast.show();
    }
	

}
