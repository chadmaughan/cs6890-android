package org.vkedco.android.sharedprefs2;

/*
 ***************************************************************
 * SharedPrefsSave takes the values of two shared preferences
 * from the user, creates a SharedPreferences object, persists
 * it and requests Android to launch SharedPrefsReceiver, an
 * activity that retrieves the shared preferences through
 * the application's context. 
 * 
 * The point of this application is to illustrate how different
 * activities can use the application's context to pass shared
 * preferences to each other.
 * 
 * Errors, comments to vladimir dot kulyukin at gmail dot com
 ***************************************************************
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SharedPrefsSaver extends Activity {
	
	private SharedPreferences sharedPrefs = null;
	private TextView prefTV1 = null;
	private EditText prefET1 = null;
	private TextView prefTV2 = null;
	private EditText prefET2 = null;
	private Button passPrefsBtn = null;
	private Button clrPrefsBtn = null;
	private Button finishBtn = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Context ctxt = getApplicationContext();
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
        prefTV1 = (TextView)findViewById(R.id.prefTV1);
    	prefET1 = (EditText)findViewById(R.id.prefET1);
    	prefTV2 = (TextView)findViewById(R.id.prefTV2);
    	prefET2 = (EditText)findViewById(R.id.prefET2);
    	passPrefsBtn = (Button)findViewById(R.id.passPrefsButton);
    	clrPrefsBtn = (Button)findViewById(R.id.clearButton);
    	finishBtn = (Button)findViewById(R.id.quitButton);
    	
    	loadPrefs();
    	
    	passPrefsBtn.setOnClickListener(
    				new OnClickListener() {
						@Override
						public void onClick(View v) {
							savePrefs();
							requestToLaunchPrefsReceiver();
						}
    				});
    	
    	clrPrefsBtn.setOnClickListener(
    				new OnClickListener() {

						@Override
						public void onClick(View v) {
							prefET1.setText("");
							prefET2.setText("");
						}
    					
    				}
    			);
    	finishBtn.setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						finish();
					}
					
				}
			);
    }
    
    // persist SharedPreferences object
    private void savePrefs() {
    	Resources res = getResources();
    	Editor prefEditor = sharedPrefs.edit();
    	prefEditor.putString(res.getString(R.string.pref1_key), 
    			prefET1.getText().toString());
    	prefEditor.putString(res.getString(R.string.pref2_key), 
    			prefET2.getText().toString());
    	prefEditor.commit();
    }
    
    // load previously persisted SharedPreferences object
    private void loadPrefs() {
    	Resources res = getResources();
    	prefET1.setText(sharedPrefs.getString(res.getString(R.string.pref1_key), ""));
    	prefET2.setText(sharedPrefs.getString(res.getString(R.string.pref2_key), ""));
    }
    
    private void requestToLaunchPrefsReceiver() {
    	Intent i = new Intent(this, SharedPrefsReceiver.class);
    	startActivity(i);
    }
}