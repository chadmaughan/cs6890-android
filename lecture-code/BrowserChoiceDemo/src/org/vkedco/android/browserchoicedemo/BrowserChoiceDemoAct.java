package org.vkedco.android.browserchoicedemo;

/*
 *************************************************
 * BrowserChoiceDemoAct is the activity
 * class that registers itself as a web browser
 * in AndroidManifest.xml.
 * 
 * bugs, comments to vladimir dot kulyukin at 
 * usu dot edu
 ************************************************* 
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class BrowserChoiceDemoAct extends Activity {
	static final private int WIKI_DIALOG = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        this.showDialog(WIKI_DIALOG);
    }
    
    // this handler is called when showDialog() is called.
    // this handler creates a dialog on demand.
    // after the initial creation, whenever showDialog() is
    // called onPrepareDialog() is called.
    public Dialog onCreateDialog(int id) {
    	switch(id) {
    	case (WIKI_DIALOG): 
    		LayoutInflater li = LayoutInflater.from(this);
    		View wikiView = li.inflate(R.layout.link_details, null);
    		AlertDialog.Builder quakeDialog = new AlertDialog.Builder(this);
    		quakeDialog.setTitle(getResources().getString(R.string.kurt_godel_name));
    		quakeDialog.setView(wikiView);
    		return quakeDialog.create();
    	}
    	return null;
    }
    
    // onPrepareDialog should be overriden if the dialog will
    // be different every time showDialog() is called.
    public void onPrepareDialog(int id, Dialog dialog) {
    	switch (id) {
    	case (WIKI_DIALOG):
    		String wikiText = "Wikipedia article on " +
    			getResources().getString(R.string.kurt_godel_name) +
    			"\n is at " + getResources().getString(R.string.kurt_godel_wiki_link)
    			+ "\n";
    		AlertDialog wikiDialog = (AlertDialog)dialog;
    		TextView tv = (TextView)wikiDialog.findViewById(R.id.link_details_text);
    		tv.setText(wikiText);
    		break;
    	}
    }
    
}