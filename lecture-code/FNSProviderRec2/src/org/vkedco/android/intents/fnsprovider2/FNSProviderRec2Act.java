package org.vkedco.android.intents.fnsprovider2;

/*
 * *********************************************************
 * FNSConsumer2.java is part a simple broadcast system.
 * This is the application that needs to compute the sum of 
 * the first n Fibonacci numbers. There are two other 
 * applications, FNSProviderIter2 and FNSProviderRec2, that 
 * compute the sum of the first n Fibonacci numbers.
 * 
 * FNSConsumer2 broadcasts its request to compute the sum of 
 * the first n Fibonacci numbers � it is a broadcaster.
 * The second and third applications, FNSProviderIter2 and
 * FNSProviderRec1, register themselves as broadcast receivers 
 * that respond to the specific type of request to compute 
 * the sum of the first n Fibonacci numbers.
 * 
 * FNSProviderRec2 � receives broadcasts to compute the nth Fibonacci 
 * sum and broadcasts the result computed recursively. Since
 * broadcast receivers have a five second termination limit
 * imposed by Android, it may also be a good idea to check
 * for the value of n and refuse to compute it if it is too big.
 * This is not done in the source code, however.
 * 
 * Comments, bugs to vladimir dot kulyukin at gmail dot com
 * *****************************************************************
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class FNSProviderRec2Act extends BroadcastReceiver {

	// 1. Set the action string for the intent that
	// broadcasts the results of the computation
	public static final String FIB_SUM_COMPUTED 
		= "org.vkedco.android.intents.FIB_SUM_COMPUTED";

	@Override
	public void onReceive(Context context, Intent intent) {
		// 2. Get the bundle of the broadcast intent
		Bundle input_b = intent.getExtras();
		// 3. Check if the bundle contains the key "CRITERION"
		// and do the computation only if the criterion is
		// equals "BEAUTY" or "BOTH"
		if (input_b.containsKey("CRITERION")
				&& 
				( input_b.getString("CRITERION").equalsIgnoreCase("BEAUTY") ||
				  input_b.getString("CRITERION").equalsIgnoreCase("BOTH") )
				) 
		{
			// 4. get the value of N from the bundle
			long n = input_b.getLong("N");
			// 5. compute the nth Fibonacci sum
			long sum = nth_fib_sum(n);
			// 6. create an intent with the action FIB_SUM_COMPUTED
			Intent fibSumComputedIntent = new Intent(FIB_SUM_COMPUTED);
			// 7. create a bundle for the intent
			Bundle output_b = new Bundle();
			// 8. put <FIB_SUM, sum> into the bundle
			output_b.putLong("FIB_SUM", sum);
			// 9. put <SOURCE, FNSProviderRec2Act> into the bundle
			output_b.putString("SOURCE", "FNSProviderRec2Act");
			// 10. put the bundle into the intent
			fibSumComputedIntent.putExtras(output_b);
			// 11. broadcast the intent into the context
			context.sendBroadcast(fibSumComputedIntent);
		}
	}

	// Nth Fibonacci number is computed recursively
	private static long nth_fib_num(long n) {
		if (n == 0)
			return 0;
		else if (n == 1)
			return 1;
		else if (n > 1)
			return nth_fib_num(n - 2) + nth_fib_num(n - 1);
		else
			return -1;
	}

	private static long nth_fib_sum(long n) {
		long sum = 0;
		for (long i = 0; i <= n; i++)
			sum += nth_fib_num(i);
		return sum;
	}
}