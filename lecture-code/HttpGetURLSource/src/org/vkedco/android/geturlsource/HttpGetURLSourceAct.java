package org.vkedco.android.geturlsource;

/*
 ***************************************************************************
 * Get the source of a URI with org.apache.http.client.HttpClient
 * using HTTP GET as implemented in org.apache.http.client.methods.HttpGet.
 * 
 * The URI is supplied by the user through a GUI. The HttpGetURLSourceAct
 * activity runs several trials to get the source code of the URL specified
 * by the URI.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 ***************************************************************************
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HttpGetURLSourceAct extends Activity
	implements OnClickListener
{
    private static Button   mSubmitBtn;
    private static Button   mClearBtn;
    private static EditText mUrlEntry;
    private static EditText mUrlSource;
    private static int      mNumRetries;
    private static final String LOGPROMPT = "HttpGetURLSourceAct";
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
         mSubmitBtn  = (Button)this.findViewById(R.id.submitBtn);
         mClearBtn   = (Button)this.findViewById(R.id.clearBtn);
         mUrlEntry   = (EditText)this.findViewById(R.id.url_entry);
         mUrlSource  = (EditText)this.findViewById(R.id.url_source);
         mNumRetries = getResources().getInteger(R.integer.num_retries);
         
         mSubmitBtn.setOnClickListener(this);
         mClearBtn.setOnClickListener(this);
    }
    
	@Override
	public void onClick(View v) {
		switch ( v.getId() ) {
		
		case R.id.submitBtn: {
			URI uri = null;
			try {
				uri = URI.create(mUrlEntry
						.getText()
						.toString()
						.toString());
				Log.v(LOGPROMPT, uri.toString());
			}
			catch ( Exception e ) {
				Toast.makeText(getApplicationContext(),
						"invalid url", Toast.LENGTH_LONG).show();
			}
			finally {
				if ( uri != null ) {
					try {
						Log.v(LOGPROMPT, "about to retrieve url source");
						mUrlSource.setText(doHttpGetWithRetries(uri, mNumRetries));
						Log.v(LOGPROMPT, "source retrieval done");
					}
					catch ( Exception e ) {
						e.printStackTrace();
					}
				}
			}
			break;
		}
		
		case R.id.clearBtn: {
			mUrlSource.setText("");
			break;
		}
		
		}
		
	}
	
	private String doHttpGetWithRetries(URI uri, int num_retries) {
    	int count = 0;
    	while ( count < num_retries ) {
    		try {
    			String res = tryHttpGetOnce(uri);
    			return res;
    		}
    		catch ( Exception ex ) {
    			Log.v("HTTPGET", ex.toString());
    			if ( count < num_retries ) {
    				count++;
    			}
    		}
    	}
    	return "source retrieval failure";
    }
    
    private String tryHttpGetOnce(URI uri) throws Exception {
    	BufferedReader in = null;
    	try {
    		HttpClient hc = new DefaultHttpClient();
    		HttpGet req = new HttpGet();
    		req.setURI(uri);
    		HttpResponse res = hc.execute(req);
    		in = new BufferedReader(new InputStreamReader(res
    							.getEntity()
    							.getContent()));
    		StringBuffer sb = new StringBuffer("");
    		String currLine = "";
    		String LNSPRTR = System.getProperty("line.separator");
    		while ( (currLine = in.readLine()) != null ) {
    			sb.append(currLine + LNSPRTR);
    		}
    		in.close();
    		return sb.toString();
    	}
    	finally {
    		if ( in != null ) {
    			try {
    				in.close();
    			}
    			catch ( IOException e ) {
    				mUrlSource.setText(e.toString());
    			}
    		}
    	}
    }
}