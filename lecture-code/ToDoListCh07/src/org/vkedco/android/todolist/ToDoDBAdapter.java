package org.vkedco.android.todolist;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

// Based on Ch. 07 in "Professional Android 2 Application Development"
// by Rito Meier
public class ToDoDBAdapter {
	// define database, table, and version
	private static final String DB_NAME     = "todoList.db";
	private static final String DB_TABLE    = "todoItem";
	private static final int    DB_VERSION  = 1;
	
	private SQLiteDatabase   db;
	private final Context    context;
	private toDoDBOpenHelper dbHelper;
	
	// publish constants for table column names
	public static final String ID_COL_NAME            = "_id";
	public static final String TASK_COL_NAME          = "task";
	public static final String CREATION_DATE_COL_NAME = "creation_date";
	
	// publish constants for table column numbers
	public static final int ID_COL_NUM                = 0;
	public static final int TASK_COL_NUM              = 1;
	public static final int CREATION_DATE_COL_NUM     = 2;
	
	// toDoDBOpenHelper class creates the table in the database
	private static class toDoDBOpenHelper extends SQLiteOpenHelper {
		
		public toDoDBOpenHelper(Context context, String name, 
				CursorFactory factory, int version) {
			super(context, name, factory, version);
		}
		
		// table creation string constant
		private static final String DB_CREATE =
			"create table " + DB_TABLE + 
			" (" + 
			ID_COL_NAME            + " integer primary key autoincrement, " + 
			TASK_COL_NAME          + " text not null, " + 
			CREATION_DATE_COL_NAME + " long" +
			");";

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DB_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, 
				int newVersion) {
			Log.w("TaskDBAdapter", "Upgrading from version " +
					oldVersion + " to " +
					newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
			onCreate(db);
		}
	}
	
	// initialize the context and the helper objects
	public ToDoDBAdapter(Context context) {
		this.context = context;
		dbHelper = new toDoDBOpenHelper(context, DB_NAME,
				null, DB_VERSION);
	}
	
	// open either writeable or, if that is impossible,
	// readable database
	public void open() throws SQLiteException {
		try {
			db = dbHelper.getWritableDatabase();
			Log.w("OPEN DB: ", "WRITEABLE DB CREATED");
		}
		catch ( SQLiteException ex ) {
			Log.w("OPEN DB: ", "READABLE DB CREATED");
			db = dbHelper.getReadableDatabase();
		}
	}
	
	public void close() {
		db.close();
	}
	
	// Strongly typed insertion method
	public long insertTask(ToDoItem task) {
		ContentValues newTaskValues = new ContentValues();
		newTaskValues.put(TASK_COL_NAME, task.getTask());
		newTaskValues.put(CREATION_DATE_COL_NAME, task.getDate().getTime());
		long insertedRowIndex = 
			db.insert(DB_TABLE, null, newTaskValues);
		Log.w("INSERT TASK", "Inserted " + insertedRowIndex 
				+ task.toString());
		return insertedRowIndex;
	}
	
	// remove a row by row index
	public boolean removeTaskByRowIndex(long rowIndex) {
		return ( db.delete(DB_TABLE, ID_COL_NAME + 
				"=" + rowIndex, null) > 0 );
	}
	
	// take a ToDoItem tdi and remove a row from the table
	// if the row's creation date is the same as tdi's creation date
	public boolean removeTaskByCreationDate(ToDoItem tdi) {
		return ( 
				db.delete(DB_TABLE, 
					CREATION_DATE_COL_NAME + "=" + 
					tdi.getDate().getTime(), 
					null) > 0 
				);
	}
	
	// update the name of the task
	public boolean updateTask(long rowIndex, String task) {
		ContentValues newValue = new ContentValues();
		newValue.put(TASK_COL_NAME, task);
		
		return ( db.update(DB_TABLE, newValue, 
				ID_COL_NAME + "=" + rowIndex, null) > 0 );
	}
	
	// retrieve the cursor to iterate over all rows in
	// the table
	public Cursor getAllToDoItemsCursor() {
		return db.query(DB_TABLE, 
				new String[] { 
					ID_COL_NAME, 
					TASK_COL_NAME, 
					CREATION_DATE_COL_NAME 
					},
				null, null, null, null, null);
	}
	
	// retrieve the row whose ID is equal to rowIndex
	public Cursor setCursorToDoItem(long rowIndex) throws SQLException {
		Cursor rslt = 
			db.query(true, DB_TABLE, 
				new String[] {
					ID_COL_NAME, 
					TASK_COL_NAME
					},
				ID_COL_NAME + "=" + rowIndex, 
				null, null, null, null, null);
		if ((rslt.getCount() == 0 || !rslt.moveToFirst()) ) {
			throw new SQLException("No to do items found for row: " + rowIndex);
		}
		return rslt;
	}
	
	// retrieve a row whose ID is equal to rowIndex and
	// turn it into a ToDoItem object.
	public ToDoItem getToDoItem(long rowIndex) throws SQLException {
		Cursor cursor = db.query(true, DB_TABLE,
				new String[] { ID_COL_NAME, TASK_COL_NAME },
				ID_COL_NAME + "=" + rowIndex, 
				null, null, null, null, null);
		if ((cursor.getCount() == 0) || !cursor.moveToFirst() ) {
			throw new SQLException("No to do item found for row: " + rowIndex);
		}
		
		String task = cursor.getString(TASK_COL_NUM);
		long created = cursor.getLong(CREATION_DATE_COL_NUM);
		Date date = new Date(created);
		
		ToDoItem rslt = new ToDoItem(task, date);
		return rslt;
	}
	
}
