package org.vkedco.android.todolist;

/*
 *************************************************
 * ToDoListAct is the main activity of of
 * the Todo List application described in 
 * Ch. 2, 4, 5, 6, and 7 of "Professional Android 2 
 * Application Development" by Rito Meier.
 * 
 * The code has been modified to let the user
 * specify the chapter whose code the user wants
 * to test. Take a look at /res/strings.xml for
 * details.
 ************************************************* 
 */

import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class ToDoListAct extends Activity {

	static final private int ADD_NEW_TODO = Menu.FIRST;
	static final private int REMOVE_TODO  = Menu.FIRST + 1;
	static final private int TOAST_TODO   = Menu.FIRST + 2;
	private EditText myEditText = null;
	private ListView myListView = null;
	// set to true when a new item is being added to the
	// todo list
	private boolean addingNew = false;
	
	// this is a simple ArrayAdapter<String> from Ch 02
	private ArrayAdapter<String> aryadptr;
	private ArrayList<String> todoItems;
	// this is an ArrayAdapter<ToDoItem> that
	// shows how a text view can be customized in Ch 03
	private ArrayList<ToDoItem> todoItemsList;
	private ArrayAdapter<ToDoItem> aryadptr2;
	// ToDoItemAdapter is a cutomized ArrayAdapter from Ch 05, 06, 07
	private ToDoItemAdapter aryadptr3;
	
	// DB Adapter class for Ch 07
	ToDoDBAdapter toDoDBAdapter;
	
	private static int chapterNum = 0;
	
	// Ch 06 additions: part 01
	// Instance state of ToDoListAct consists of three variables:
	// 		- is a new item being added?
	//		- what text exists in the new item entry textbook?
	//		- what is the current selection item?
	private static final String TEXT_ENTRY_KEY = "TEXT_ENTRY_KEY";
	private static final String ADDING_ITEM_KEY = "ADDING_ITEM_KEY";
	private static final String SELECTED_INDEX_KEY = "SELECTED_INDEX_KEY";
	
	// Ch 06: additions: part 02
	@Override
	protected void onPause() {
		super.onPause();
		
		SharedPreferences uiState = this.getPreferences(Activity.MODE_PRIVATE);
		SharedPreferences.Editor edtr = uiState.edit();
		
		edtr.putString(TEXT_ENTRY_KEY, myEditText.getText().toString());
		edtr.putBoolean(ADDING_ITEM_KEY, addingNew);
		edtr.commit();
	}
	
	// Ch 06: additions: part 03
	private void restoreUIState() {
		SharedPreferences settings = getPreferences(Activity.MODE_PRIVATE);
		
		String text = settings.getString(TEXT_ENTRY_KEY, "");
		Boolean adding = settings.getBoolean(ADDING_ITEM_KEY, false);
		
		if ( adding ) {
			addNewItem();
			myEditText.setText(text);
		}
	}
	
	// Ch 06: additiongs: part 04
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putInt(SELECTED_INDEX_KEY,
				myListView.getSelectedItemPosition());
		super.onSaveInstanceState(savedInstanceState);
	}
	
	// Ch 06: additions: part 05
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		int pos = -1;
		
		if ( savedInstanceState != null ) {
			if ( savedInstanceState.containsKey(SELECTED_INDEX_KEY) ) {
				pos = savedInstanceState.getInt(SELECTED_INDEX_KEY, -1);
			}
		}
		
		myListView.setSelection(pos);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		myListView = (ListView) findViewById(R.id.myListView);
		myEditText = (EditText) findViewById(R.id.myEditText);
		
		int resID;
		chapterNum = Integer.valueOf(getResources().getString(R.string.chapter_num));
		switch ( chapterNum ) {
		case 2: {
			// use aryadptr to connect todoItems to myListView
			resID = android.R.layout.simple_list_item_1;
			todoItems = new ArrayList<String>();
			aryadptr = new ArrayAdapter<String>(this, resID, todoItems);
			myListView.setAdapter(aryadptr);
			break;
		}
		case 4: {
			// use aryadptr2 to connect todoItemList to myListView
			resID = R.layout.todolist_item;
			todoItemsList = new ArrayList<ToDoItem>();
			aryadptr2 = new ArrayAdapter<ToDoItem>(this, resID, todoItemsList);
			myListView.setAdapter(aryadptr2);
			break;
		}
		case 5: {
			// use aryadptr3, a custom ArrayAdapter, to
			// connect todoItemList to myListView
			resID = R.layout.todolist_item_ch5;
			todoItemsList = new ArrayList<ToDoItem>();
			aryadptr3 = new ToDoItemAdapter(this, resID, todoItemsList);
			myListView.setAdapter(aryadptr3);
			break;
		}
		// Chapter 06: additions: part 05
		case 6: {
			resID = R.layout.todolist_item_ch5;
			todoItemsList = new ArrayList<ToDoItem>();
			aryadptr3 = new ToDoItemAdapter(this, resID, todoItemsList);
			myListView.setAdapter(aryadptr3);
			restoreUIState();
			break;
		}
		// Chapter 07 initializations
		case 7: {
			resID = R.layout.todolist_item_ch5;
			todoItemsList = new ArrayList<ToDoItem>();
			aryadptr3 = new ToDoItemAdapter(this, resID, todoItemsList);
			myListView.setAdapter(aryadptr3);
			restoreUIState();
			toDoDBAdapter = new ToDoDBAdapter(this);
			toDoDBAdapter.open();
			populateTodoList();
			break;
		}
		default:
			finish();
		}

		myEditText.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
						
						switch ( chapterNum ) {
						case 2: {
							// 1. This adds a new item at the beginning of the
							// list.
							// todoItems.add(0,
							// myEditText.getText().toString());
							// 2. This adds a new item at the end of the list.
							todoItems.add(myEditText.getText().toString());
							// 3. notify the ArrayAdapter of the
							// change
							aryadptr.notifyDataSetChanged();
							// 4. Here is a useful thing we can add to
							// our code when we debug
							Log.v("TODOLIST:", myEditText.getText().toString()
									+ " added");
							myEditText.setText("");
							cancelAdd();
							return true;
						}
						case 4: {
							ToDoItem newItem = new ToDoItem(myEditText
									.getText().toString());
							todoItemsList.add(newItem);
							aryadptr2.notifyDataSetChanged();
							cancelAdd();
							return true;
						}
						case 5: {
							ToDoItem newItem = new ToDoItem(myEditText.getText().toString());
							todoItemsList.add(newItem);
							aryadptr3.notifyDataSetChanged();
							cancelAdd();
							return true;
						}
						case 6: {
							ToDoItem newItem = new ToDoItem(myEditText.getText().toString());
							todoItemsList.add(newItem);
							aryadptr3.notifyDataSetChanged();
							cancelAdd();
							return true;
						}
						// Chapter 07
						case 7: {
							ToDoItem newItem = new ToDoItem(myEditText.getText().toString());
							toDoDBAdapter.insertTask(newItem);
							updateArray();
							myEditText.setText("");
							aryadptr3.notifyDataSetChanged();
							cancelAdd();
							return true;
						}
						default: finish();
						}
					}
					return false;
				} else {
					return false;
				}
			}
		});
		// 4. Another debug message.
		Log.v("TODOLIST:", "onCreate() done...");

		// We register the context menu with
		// myListView.
		registerForContextMenu(myListView);
	}
	
	// Chapter 07: addition 02
	Cursor toDoListCursor;
	private void populateTodoList() {
		// get a cursor items that points to iterate over all
		// rows in the table of todo items
		toDoListCursor = toDoDBAdapter.getAllToDoItemsCursor();
		startManagingCursor(toDoListCursor);
		updateArray();
	}
	
	private void updateArray() {
		// update the cursor object 
		toDoListCursor.requery();
		// clear the array list of todo items
		todoItemsList.clear();
		
		// use the cursor to extract todo items and repopulate
		// todoItemsList
		if ( toDoListCursor.moveToFirst() ) {
			do {
				String task  = 
					toDoListCursor.getString(ToDoDBAdapter.TASK_COL_NUM);
				long created = 
					toDoListCursor.getLong(ToDoDBAdapter.CREATION_DATE_COL_NUM);
				
				ToDoItem newItem = new ToDoItem(task, new Date(created));
				todoItemsList.add(newItem);
			} while ( toDoListCursor.moveToNext() );
		}
		
		aryadptr3.notifyDataSetChanged();
	}

	// This method creates the main menu
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		// 0 is the group id
		// ADD_NEW_TODO is the item id
		// Menu.NONE is the sort-order id
		// R.string.remove is the item title
		MenuItem itemAdd = menu.add(0, ADD_NEW_TODO, Menu.NONE,
				R.string.add_new);
		MenuItem itemRem = menu.add(0, REMOVE_TODO, Menu.NONE, R.string.remove);

		// This is where we add icons to the menu items.
		itemAdd.setIcon(R.drawable.add_item);
		itemRem.setIcon(R.drawable.remove_item);

		// When 'A' is pressed on the virtual keyboard,
		// onOptionsItemSelected will be executed
		// for ADD_NEW_TODO
		itemAdd.setShortcut('0', 'a');
		// When 'R' is pressed on the virtual keyboard,
		// onOptionsItemSelected will be executed for
		// REMOVE_TODO.
		itemRem.setShortcut('1', 'r');

		return true;
	}
	
	// what to do when the Activity menu's item is clicked.
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		int index = myListView.getSelectedItemPosition();

		switch (item.getItemId()) {
		// the user wants to remove the item
		case REMOVE_TODO: {
			// if addingNew is true, i.e., if the user
			// wanted to add a new item and wants to
			// remove it, cancel the add.
			if (addingNew) {
				cancelAdd();
			} else {
				removeItem(index);
			}
			return true;
		}
		case ADD_NEW_TODO: {
			addNewItem();
			return true;
		}
		}

		return false;
	}

	// onPrepareOptionsMenu is implemented to modify a Menu
	// based on the application's current state immediately
	// before the menu is displayed. When addingNew is true,
	// removeTitle is set to R.string.cancel, because
	// we were in the process of adding a new item
	// to the todo list but decided not to do it.
	// When addingNew is false, removeTitle is
	// set to R.string.remove.
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		int idx = myListView.getSelectedItemPosition();

		String removeTitle = getString(addingNew ? R.string.cancel
				: R.string.remove);

		// fetch the removeItem
		MenuItem removeItem = menu.findItem(REMOVE_TODO);
		// set its title either to R.string.cancel
		// or R.string.remove.
		removeItem.setTitle(removeTitle);
		// Make it visible either when addingNew is true
		// or the index of the selected item in myListView
		// is greater than -1, i.e., there is something
		// selected in myListView.
		removeItem.setVisible(addingNew || idx > -1);

		return true;
	}

	// addingNew is false and make myEditText invisible
	private void cancelAdd() {
		addingNew = false;
		myEditText.setVisibility(View.GONE);
	}

	// set addingNew to true and make myEditText visible
	// and focused.
	private void addNewItem() {
		addingNew = true;
		myEditText.setVisibility(View.VISIBLE);
		myEditText.requestFocus();
	}

	// remove the ith item from the sequence of
	// objects connected to myListView via the
	// ArrayAdapter.
	private void removeItem(int index) {
		
		switch ( chapterNum ) {
		case 2: {
			todoItems.remove(index);
			aryadptr.notifyDataSetChanged();
			break;
		}
		// this is the case that is handled when the array adapter is handling
		// an array list of ToDoItems
		case 4: {
			this.todoItemsList.remove(index);
			this.aryadptr2.notifyDataSetChanged();
			break;
		}
		// this is the case when aryadptr3 is bound to a ToDoItemAdapter.
		case 5: {
			this.todoItemsList.remove(index);
			this.aryadptr3.notifyDataSetChanged();
			break;
		}
		case 6: {
			this.todoItemsList.remove(index);
			this.aryadptr3.notifyDataSetChanged();
			break;
		}
		case 7: {
			ToDoItem tdi = todoItemsList.get(index);
			boolean rslt = this.toDoDBAdapter.removeTaskByCreationDate(tdi);
			if ( rslt ) {
				Log.w("REMOVE ITEM:", tdi.toString() + " removed");
			}
			else {
				Log.w("REMOVE ITEM:", tdi.toString() + " not removed");
			}
			this.updateArray();
			break;
		}
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if ( chapterNum == 7 ) {
			toDoDBAdapter.close();
		}
	}

	// When an item is selected in myListView, then
	// a ContextMenu is activated with one removeItem
	// in it.
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("Context Item Selection");
		menu.add(0, REMOVE_TODO, Menu.NONE, R.string.remove);
		menu.add(0, TOAST_TODO, Menu.NONE, R.string.toast);
	}

	// This is an method that is called when a item is selected
	// in the context menu.
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case REMOVE_TODO: {
			// We need to get the position of the MenuItem item.
			// This is done through the AdapterView.AdapterContextMenuInfo
			// object. First, we obtain that object by calling
			// item.getMenuInfo().
			// Second, we obtain the index of the object from
			// menuInfo.position.
			AdapterView.AdapterContextMenuInfo menuInfo;
			menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
			int index = menuInfo.position;
			removeItem(index);
			return true;
		}
		case TOAST_TODO: {
			AdapterView.AdapterContextMenuInfo menuInfo;
			menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
			String toast_text = "";
			switch ( chapterNum ) {
			case 2: {
				toast_text = todoItems.get(menuInfo.position);
				break;
			}
			// this is the case that is handled when the array adapter is handling
			// an array list of ToDoItems
			case 4: {
				toast_text = todoItemsList.get(menuInfo.position).toString();
				break;
			}
			// this is the case when aryadptr3 is bound to a ToDoItemAdapter.
			case 5: {
				toast_text = todoItemsList.get(menuInfo.position).toString();
				break;
			}
			case 6: {
				toast_text = todoItemsList.get(menuInfo.position).toString();
				break;
			}
			case 7: {
				toast_text = todoItemsList.get(menuInfo.position).toString();
				break;
			}
			}
			Toast t = Toast.makeText(this, toast_text, Toast.LENGTH_LONG);
			t.show();
			return true;
		}
		}

		return false;
	}
}