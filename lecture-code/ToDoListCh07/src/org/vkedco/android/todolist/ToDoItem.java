package org.vkedco.android.todolist;

/*
* ToDoItem is part of the Todo List application 
* described in Ch. 2, 4, and 5 of "Professional Android 2 
* Application Development" by Rito Meier.
*/

import java.text.SimpleDateFormat;
import java.util.Date;

public class ToDoItem {

	private String mTask;
	private Date mDate;
	
	public String getTask() {
		return mTask;
	}
	
	public Date getDate() {
		return mDate;
	}
	
	public ToDoItem(String task, Date date) {
		mTask = task;
		mDate = date;
	}
	
	public ToDoItem(String task) {
		this(task, new Date(java.lang.System.currentTimeMillis()));
	}
	
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
		String dateString = sdf.format(mDate);
		return "(" + dateString + ") " + mTask;
	}
	
	
}
