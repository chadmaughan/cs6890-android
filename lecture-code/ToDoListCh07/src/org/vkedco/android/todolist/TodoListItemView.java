package org.vkedco.android.todolist;

/*
* ToDoListItemView is part of the Todo List application 
* described in Ch. 2, 4, and 5 of "Professional Android 2 
* Application Development" by Rito Meier.
*/

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

public class TodoListItemView extends TextView {
	private Paint mMarginPaint;
	private Paint mLinePaint;
	private int mPaperColor;
	private float mMarginDim;
	
	public TodoListItemView(Context context, AttributeSet attrs, 
			int defStyle) 
	{
		super(context, attrs, defStyle);
		init();
	}
	
	public TodoListItemView(Context context) {
		super(context);
		init();
	}
	
	public TodoListItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	private void init() {
    	// 1. Get a reference to our resource table.
    	Resources myResources = getResources();
    	
    	// 2. Create the paint brushes we will use in the onDraw method
    	// to paint the margins. Anti-Aliasing is a method of making the 
    	// eye believe that rough edges are smooth.
    	mMarginPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    	mMarginPaint.setColor(myResources.getColor(R.color.notepad_margin));
    	
    	// 3. Create the paint brushes we will use in the onDraw method
    	// to paint the lines.
    	mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    	mLinePaint.setColor(myResources.getColor(R.color.notepad_lines));
    	
    	// 3. Get the paper background color and the margin width
    	mPaperColor = myResources.getColor(R.color.notepad_paper);
    	mMarginDim = myResources.getDimension(R.dimen.notepad_margin);
    }
    
    public void onDraw(Canvas canvas) {
    	// Color as paper
    	canvas.drawColor(mPaperColor);
    	
    	// This is a good exercise.
    	int mh = getMeasuredHeight();
    	int mw = getMeasuredWidth();
    	// Draw ruled lines
    	// draw line from top left to top right
    	canvas.drawLine(5, 5, mw-5, 5, mLinePaint);
    	// draw line from top right to bottom right
    	canvas.drawLine(mw-5, 5, mw-5, mh-5, mLinePaint);
    	// draw line from bottom right to bottom left
    	canvas.drawLine(mw-5, mh-5, 5, mh-5, mLinePaint);
    	// draw line from bottom left to top left
    	canvas.drawLine(5, mh-5, 5, 5, mLinePaint);
    	
    	// Draw margin
    	canvas.drawLine(mMarginDim, 0, mMarginDim, getMeasuredHeight(),
    			mMarginPaint);
    	
    	// Save the state of the canvas
    	canvas.save();
    	// move the text mMarginDim pixels left and 0 pixels down.
    	canvas.translate(mMarginDim, 0);
    	
    	// Use the TextView to render the text.
    	super.onDraw(canvas);
    	// every save() must be matched with restore()
    	canvas.restore();
    }

}
