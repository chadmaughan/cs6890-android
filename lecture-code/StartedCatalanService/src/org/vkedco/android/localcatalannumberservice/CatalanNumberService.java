package org.vkedco.android.localcatalannumberservice;

/*
 *************************************************************
 * The started service of an application that computes Catalan
 * numbers and displays them one by one in the status bar
 * via a NotificationManager object. Below are the first 10 
 * Catalan numbers.
 * 
 * 0 | 1 | 2 | 3 | 4  | 5  |  6  |  7  |  8   |  9   | 10    |
 * -----------------------------------------------------------
 * 1 | 1 | 2 | 5 | 14 | 42 | 132 | 429 | 1430 | 4862 | 16796 |
 * -----------------------------------------------------------
 * 
 * Reference: http://en.wikipedia.org/wiki/Catalan_number
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************************
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Process;

public class CatalanNumberService extends Service {
	private long mPrevCatNum = 1L;
	private long mUpperBound = 100000L;
	private NotificationManager mNoteMngr = null;
	private String mMsg = "";
	private long mSleepInterval = 7000L; 
	
	// 1. Define a worker thread that implements the Runnable
	//    interface. This is just an example of how one can spawn
	//    a thread in a service. IntentService and Looper are
	//    safer alternatives.
	class CatalanServiceWorker implements Runnable {
		public void run() {
			long n = 0L;
			while ( mPrevCatNum <= mUpperBound ) {
				mMsg = "";
				mMsg += ("C(" + n + ")=" + mPrevCatNum);
				CatalanNumberService.this.handleNotificationMessage(mMsg);
				// Go to sleep to simulate a time-consuming computation
				try {
					Thread.sleep(mSleepInterval);
				}
				catch ( Exception ex ) {
					// Stop the service in case of exception
					CatalanNumberService.this.stopSelf();
				}
				nextC(++n);
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		// This is a started (local) service so null is returned in onBind().
		// If it were a bound service, we would have to return an
		// IBinder.
		return null;
	}

	public void onCreate() {
		super.onCreate();
		// Get a NotificationManager object from Android
		mNoteMngr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Post a message on the status bar that the service has been
		// started
		handleNotificationMessage("starting background catalan service");
		// Create and start a worker thread.
		Thread thr = new Thread(null, 
				new CatalanServiceWorker(),
				"CatalanNumberService");
		thr.start();	
	}

	// Start a sticky thread
	public int onStartCommand(Intent i, int flags, int startId) {
		super.onStartCommand(i, flags, startId);
		return START_STICKY;
	}

	// This method is called by Android when the service is destroyed
	public void onDestroy() {
		super.onDestroy();
		// Post a notification message in the status bar
		handleNotificationMessage("stopping background catalan service");
		// Stop the service
		stopSelf();
		// Remove icon from the status bar
		mNoteMngr.cancel(R.string.app_id);
		// wait a second to make sure that the message has a chance
		// to show on the status bar
		synchronized (this ) {
			try {
				wait(1000);
			}
			catch ( Exception ex ) {}
		}
		// If you uncomment the following line, the hosting process is killed.
		// Process.killProcess(Process.myPid());
	}
	
	// Post a notification message in the status bar
	public void handleNotificationMessage(String msg) {
		Notification note = new Notification(R.drawable.ornament_00, msg,
				System.currentTimeMillis());
		PendingIntent pi = PendingIntent.getActivity(getApplicationContext(),
				0, new Intent(getApplicationContext(),
						StartedCatalanServiceAct.class), 0);
		note.setLatestEventInfo(getApplicationContext(),
				"Catalan Number Service", msg, pi);
		this.mNoteMngr.notify(R.string.app_id, note);
	}

	// Compute the next Catalan number.
	private void nextC(long n) {
		if ( n > 0L ) {
			mPrevCatNum *= (4 * n - 2);
			mPrevCatNum /= (n + 1);
		}
	}
}
