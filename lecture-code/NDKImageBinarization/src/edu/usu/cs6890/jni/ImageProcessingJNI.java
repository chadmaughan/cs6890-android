package edu.usu.cs6890.jni;

/*
 ******************************************************* 
 * ImageProcessingJNI.java is an an example of how to 
 * interface to C++ code in an Android application.
 * 
 * The application binarizes grayscale images saved on
 * the emulator in /mnt/sdcard/images/grayscale. The
 * binarized images are saved in /mnt/sdcard/images/binary.
 * The C++ program that does binarizes is defined in
 * /jni/Binarizer.cpp.
 * 
 * Errors, comments to vladimir dot kulyukin at gmail dot com
 *******************************************************
 */

public class ImageProcessingJNI
{
	static
	{
		System.loadLibrary("processJNI");
	}

	public static native void binarize(int[] image, int[] binaryimage,
			int size, int threshold);
}
