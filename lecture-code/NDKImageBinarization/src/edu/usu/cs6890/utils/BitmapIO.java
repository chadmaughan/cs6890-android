package edu.usu.cs6890.utils;

/*
 ******************************************************* 
 * BitmapIO.java is a utility for manipulating bitmaps.
 * 
 * Errors, comments to vladimir dot kulyukin at gmail dot com
 *******************************************************
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import android.graphics.Bitmap;
import android.graphics.Color;

public class BitmapIO
{
	public static Bitmap exportToBitmap(int[] imageArray, int width, int height)
	{
		Bitmap bitmapImage = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);

		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				int val = imageArray[j * width + i];
				bitmapImage.setPixel(i, j, Color.argb(255, val, val, val));
			}
		}
		return bitmapImage;
	}

	public static void exportToFile(int[] imageArray, int width, int height,
			String dir, String filename) throws IOException
	{

		Bitmap bitmap = exportToBitmap(imageArray, width, height);
		exportToFile(bitmap, dir, filename);
		bitmap.recycle();
		bitmap = null;
	}

	public static void exportToFile(Bitmap bitmap, String dir, String filename)
			throws IOException
	{
		File directoryFile = new File(dir);
		directoryFile.mkdirs();

		FileOutputStream out = new FileOutputStream(dir + filename + ".png");

		bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
		out.close();
	}

	public static int[] getLuminanceArray(Bitmap bitmap)
	{
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();

		int[] image = new int[width * height];
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
			{
				image[i * width + j] = Color.green(bitmap.getPixel(j, i));
			}
		return image;
	}
}
