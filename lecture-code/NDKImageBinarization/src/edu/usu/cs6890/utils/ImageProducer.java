package edu.usu.cs6890.utils;

/******************************************************************
 * File implementation of the ImageProducer interface. This class 
 * allows you to obtain images stored from a directory.
 * 
 * @author Aliasgar Kutiyanawala
 * ****************************************************************
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


public class ImageProducer implements Iterable<Bitmap>
{
	private ArrayList<String> listOfImageFiles = new ArrayList<String>();
	private int index = 0;

	/**
	 * Constructs an instance of the FileImageProducer class
	 * 
	 * @param directory
	 *            - path of the directory containing the images
	 * @throws FileNotFoundException
	 */
	public ImageProducer(String directory) throws FileNotFoundException
	{
		File dir = new File(directory);
		if (!dir.exists())
			throw new FileNotFoundException(directory);

		String[] filenames = dir.list();
		for (String filename : filenames)
		{
			// display(directory + filename + "\n");
			if (filename.contains(".bmp") || filename.contains(".jpg")
					|| filename.contains(".png") || filename.contains("gif")
					|| filename.contains("jpeg"))
				listOfImageFiles.add(directory + filename);
		}
	}

	/**
	 * @return true if the image producer has another image
	 */
	public Bitmap getNextImage()
	{
		// TODO Auto-generated method stub
		if (!listOfImageFiles.isEmpty())
		{
			String filename = listOfImageFiles.remove(0);
			System.out.println("\n" + filename);
			Bitmap bitmap = BitmapFactory.decodeFile(filename);
			return bitmap;
		}
		return null;
	}

	/**
	 * 
	 * Gets the next image from the image producer
	 * 
	 * @return Bitmap containing the image
	 */
	public boolean hasNextImage()
	{
		System.out.println("\n***" + listOfImageFiles.size());

		if (listOfImageFiles.isEmpty())
			return false;
		else
			return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Bitmap> iterator()
	{
		return new Iterator<Bitmap>()
		{
			@Override
			public boolean hasNext()
			{
				return hasNextImage();
			}

			@Override
			public Bitmap next()
			{
				return getNextImage();
			}

			@Override
			public void remove()
			{

			}
		};
	}
}
