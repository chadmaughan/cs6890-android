package edu.usu.cs6890.main;

/*
 ******************************************************* 
 * ImageBinarizationUsingNDKActivity.java is an an example
 * that uses C++ code through ImageProcessingJNI.java.
 * The application binarizes grayscale images saved on
 * the emulator in /mnt/sdcard/images/grayscale. The
 * binarized images are saved in /mnt/sdcard/images/binary.
 * The C++ program that does binarizes is defined in
 * /jni/Binarizer.cpp.
 * 
 * Errors, comments to vladimir dot kulyukin at gmail dot com
 *******************************************************
 */

import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;
import edu.usu.cs6890.jni.ImageProcessingJNI;
import edu.usu.cs6890.utils.BitmapIO;
import edu.usu.cs6890.utils.ImageProducer;

public class ImageBinarizationUsingNDKActivity extends Activity
{
	private static final int THRESHOLD = 127;
	private TextView textView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		textView = new TextView(this.getApplicationContext());

		FrameLayout frameLayout = new FrameLayout(this.getApplicationContext());
		setContentView(frameLayout);
		frameLayout.addView(textView);

		ImageProducer imageProducer;

		textView.setText("Binarizing Images\n");
		try
		{
			imageProducer = new ImageProducer("/sdcard/images/grayscale/");

			String directory = "/sdcard/images/binary/";

			int imageNum = 0;
			for (Bitmap bitmap : imageProducer)
			{
				if (bitmap != null)
				{
					int height = bitmap.getHeight();
					int width = bitmap.getWidth();

					int[] grayScaleImage = BitmapIO.getLuminanceArray(bitmap);

					int size = grayScaleImage.length;
					int[] binaryImage = new int[size];

					textView.append("\nBinarizing image " + imageNum);
					ImageProcessingJNI.binarize(grayScaleImage, binaryImage,
							size, THRESHOLD);

					textView.append("\nWriting image " + imageNum
							+ " to file ...");

					String filename = "binary_image_" + imageNum;
					BitmapIO.exportToFile(binaryImage, width, height,
							directory, filename);

					textView.append("Done!");
					imageNum++;
					bitmap.recycle();
				}

				bitmap = null;
			}
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}