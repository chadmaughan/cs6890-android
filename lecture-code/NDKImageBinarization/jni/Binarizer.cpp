#include <math.h>
#include <jni.h>
#include <cpu-features.h>


#define JNIEXPORT
#define JNICALL

#ifdef __cplusplus
extern "C"
{
#endif

JNIEXPORT void JNICALL
Java_edu_usu_cs6890_jni_ImageProcessingJNI_binarize(
		JNIEnv *env, jclass jc, jintArray image, jintArray binarizedImage, jint size, jint threshold)
{
	jboolean isCopy; // copy flags
	// Java to C++ arrays
	jint *im_gray = env->GetIntArrayElements(image, 0);
	jint *im_binary = env->GetIntArrayElements(binarizedImage, &isCopy);


	//binarize image based on global thresholding
	for (int i = 0; i < size; i++)
	{
		if (im_gray[i] < threshold)
			im_binary[i] = 0;
		else
			im_binary[i] = 255;
	}

	//We haven't changed anything in im_gray so no need to copy it back to the buffer
	env->ReleaseIntArrayElements(image, im_gray, JNI_ABORT);
		
	// Copy the contents of the buffer back into array and free the buffer
	if (isCopy == JNI_TRUE)
		env->ReleaseIntArrayElements(binarizedImage, im_binary, 0);
	else
		env->ReleaseIntArrayElements(binarizedImage, im_binary, JNI_ABORT);
}


#ifdef __cplusplus
}
#endif
