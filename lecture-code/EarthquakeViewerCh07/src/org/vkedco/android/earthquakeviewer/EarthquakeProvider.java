package org.vkedco.android.earthquakeviewer;

/*
 ***************************************************************
 * EarthquakeProvider is the content provider for
 * the EarthquakeViewer application described in 
 * Chapter 5, 6, 7 of "Pro Android 2 
 * Application Development" by Rito Meier.
 * 
 * Several errors and typos are fixed.
 ***************************************************************
 */

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class EarthquakeProvider extends ContentProvider {
	// URI for this provider
	public static final Uri CONTENT_URI =
		Uri.parse("content://org.vkedco.provider.earthquakeviewer/earthquakes");
	
	// The database of earthquakes
	private SQLiteDatabase      earthquakeDB;
	
	// Define all private constants
	private static final String TAG              = "EarthquakeProvider";
	private static final String DB_NAME          = "earthquakes.db";
	private static final int    DB_VERSION       = 1;
	private static final String EARTHQUAKE_TABLE = "earthquakes";
	
	// Publish column names
	public static final String KEY_ID            = "_id";
	public static final String KEY_DATE          = "date";
	public static final String KEY_DETAILS       = "details";
	public static final String KEY_LOCATION_LAT  = "latitude";
	public static final String KEY_LOCATION_LNG  = "longitude";
	public static final String KEY_MAGNITUDE     = "magnitude";
	public static final String KEY_LINK          = "link";
	
	// Publish column indexes
	public static final int DATE_COLUMN          = 1;
	public static final int DETAILS_COLUMN       = 2;
	public static final int LONGITUDE_COLUMN     = 3;
	public static final int LATITUDE_COLUMN      = 4;
	public static final int MAGNITUDE_COLUMN     = 5;
	public static final int LINK_COLUMN          = 6;
	
	// Constants that differentiate b/w different URI requests
	private static final int QUAKES   = 1;
	private static final int QUAKE_ID = 2;
	
	private static final UriMatcher uriMatcher;
	
	// construct a static UriMatcher object to add two URIs:
	// the first will service the URI for all QUAKES; the
	// second will service the URI for individual quakes.
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		// a URI that ends in earthquakes corresponds to a request
		// for all earthquakes
		uriMatcher.addURI("org.vkedco.provider.earthquakeviewer", 
				"earthquakes", QUAKES);
		// a URI that ends in "earthquake" with a number
		// corresponds to a request to retrieve a specific earthquake
		uriMatcher.addURI("org.vkedco.provider.earthquakeviewer",
				"earthquakes/#", QUAKE_ID);
	}
	
	// DB Helper class
	private static class earthquakeDBHelper extends SQLiteOpenHelper {
		
		// db creation table command
		private static final String DB_CREATE = 
			"create table "    + EARTHQUAKE_TABLE + " ("
			+ KEY_ID           + " integer primary key autoincrement, "
			+ KEY_DATE         + " INTEGER, "
			+ KEY_DETAILS      + " TEXT, "
			+ KEY_LOCATION_LAT + " FLOAT, "
			+ KEY_LOCATION_LNG + " FLOAT, "
			+ KEY_MAGNITUDE    + " FLOAT, "
			+ KEY_LINK         + " TEXT);";
		
		public earthquakeDBHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DB_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from versoin " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + EARTHQUAKE_TABLE);
			onCreate(db);
		}
		
	}
	
	@Override
	public boolean onCreate() {
		Context context = getContext();
		earthquakeDBHelper dbHelper
			= new earthquakeDBHelper(context, DB_NAME, null, DB_VERSION);
		earthquakeDB = dbHelper.getWritableDatabase();
		return ( earthquakeDB == null) ? false : true;
	}
	
	@Override
	public String getType(Uri uri) {
		switch ( uriMatcher.match(uri) ) {
		// if uriMatcher returns QUAKES
		case QUAKES: 
			return "vnd.android.cursor.dir/vnd.vkedco.earthquakeviewer";
		// if uriMatcher returns QUAKE_ID
		case QUAKE_ID: 
			return "vnd.android.cursor.item/vnd.vkedco.earthquakeviewer";
		default: throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, 
			String selection, String[] selectionArgs,
			String sort) 
	{
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(EARTHQUAKE_TABLE);
		switch(uriMatcher.match(uri)) {
		// if this is a row query, add the where clause with the
		// appropriate row number.
		case QUAKE_ID: {
			qb.appendWhere(KEY_ID + "=" + uri.getPathSegments().get(1));
			//for(String s : uri.getPathSegments())
			//	Log.w(TAG, s);
			break;
		}
		default: break;
		
		}
		
		String orderBy;
		// if the sort string is empty, sort by date
		if ( TextUtils.isEmpty(sort) ) {
			orderBy = KEY_DATE;
		}
		else {
			orderBy = sort;
		}
		
		// execute the query against the database
		Cursor c = qb.query(earthquakeDB, 
							projection, 
							selection, 
							selectionArgs, 
							null, null, 
							orderBy);
		// notify the content resolver about the change
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}
	
	@Override
	public Uri insert(Uri inuri, ContentValues initValues) {
		long rowID = earthquakeDB.insert(EARTHQUAKE_TABLE, "quake",
				initValues);
		
		if ( rowID > 0 ) {
			Uri uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
			getContext().getContentResolver().notifyChange(uri, null);
			return uri;
		}
		throw new SQLException("Failed to insert row into " + inuri);
	}


	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		int count;
		switch ( uriMatcher.match(uri) ) {
		case QUAKES:
			count = earthquakeDB.delete(EARTHQUAKE_TABLE, where, whereArgs);
			break;
		case QUAKE_ID:
			String segment = uri.getPathSegments().get(1);
			count = earthquakeDB.delete(EARTHQUAKE_TABLE,
					KEY_ID + "=" + segment
					+ (!TextUtils.isEmpty(where) ? " AND ("
							+ where + ")" : ""), whereArgs);
			break;
		default: throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
	

	@Override
	public int update(Uri uri, ContentValues values, String where, 
			String[] whereArgs) 
	{
		int count;
		switch ( uriMatcher.match(uri) ) {
		case QUAKES:
			count = earthquakeDB.update(EARTHQUAKE_TABLE,
					values, where, whereArgs);
			break;
		case QUAKE_ID:
			String segment = uri.getPathSegments().get(1);
			count = earthquakeDB.update(EARTHQUAKE_TABLE,
					values,
					KEY_ID + "=" + segment
					+ (!TextUtils.isEmpty(where) ? " AND ("
							+ where + ")" : ""),
							whereArgs);
			break;
		default: throw new IllegalArgumentException("Uknown URI " + uri);
		}
		
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

}
