package org.vkedco.android.earthquakeviewer;

/*
 ***************************************************************
 * EarthquakeViewerAct is the main activity of the EarthquakeViewer
 * application described in Chapter 5, 6, 7 of "Pro Android 2 
 * Application Development" by Rito Meier.
 * 
 * The code has been modified to let the user
 * specify if the earthquakes should be loaded from
 * a database by setting the value of R.bool.db_load_flag
 ***************************************************************
 */

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class EarthquakeViewerAct extends Activity {
	
	ListView earthquakeListView;
	ArrayAdapter<Quake> aryadptr;
	ArrayList<Quake> earthquakes = new ArrayList<Quake>();
	
	private static final int QUAKE_DIALOG = 1;
	private static final int MENU_UPDATE = Menu.FIRST;
	private static final int MENU_PREFERENCES = Menu.FIRST+1;
	private static final int SHOW_PREFERENCES = 1;
	
	Quake selectedQuake;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        earthquakeListView = (ListView)findViewById(R.id.earthquakeListView);

        earthquakeListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> av, View v, int index,
					long arg3) {
				selectedQuake = earthquakes.get(index);
				showDialog(QUAKE_DIALOG);
			}
        	
        });
        
        
        int layoutID = android.R.layout.simple_expandable_list_item_1;
        aryadptr = new ArrayAdapter<Quake>(this, layoutID, earthquakes);
        earthquakeListView.setAdapter(aryadptr);
        
        updateFromPreferences();
        refreshEarthquakes();
    }
    
    public Dialog onCreateDialog(int id) {
    	switch(id) {
    	case (QUAKE_DIALOG): 
    		LayoutInflater li = LayoutInflater.from(this);
    		View quakeDetailsView = li.inflate(R.layout.quake_details, null);
    		AlertDialog.Builder quakeDialog = new AlertDialog.Builder(this);
    		quakeDialog.setTitle("Quake Time");
    		quakeDialog.setView(quakeDetailsView);
    		return quakeDialog.create();
    	}
    	return null;
    }
    
    public void onPrepareDialog(int id, Dialog dialog) {
    	switch (id) {
    	case (QUAKE_DIALOG):
    		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
    		String dateString = sdf.format(selectedQuake.getDate());
    		String quakeText = "Magnitude " + selectedQuake.getMagnitude() +
    			"\n" + selectedQuake.getDetails() + "\n" + selectedQuake.getLink();
    		AlertDialog quakeDialog = (AlertDialog)dialog;
    		quakeDialog.setTitle(dateString);
    		TextView tv = (TextView)quakeDialog.findViewById(R.id.quakeDetailsTextView);
    		tv.setText(quakeText);
    		break;
    	}
    }
    
    private void refreshEarthquakes() {
    	URL url;
    	try {
    		String quakeFeed = getString(R.string.quake_feed);
    		url = new URL(quakeFeed);
    		URLConnection connection;
    		connection = url.openConnection();
    		Log.v("refreshEarthQuakes()", "connection opened...");
    		HttpURLConnection httpConnection = (HttpURLConnection)connection;
    		int responseCode = httpConnection.getResponseCode();
    		
    		if ( responseCode != HttpURLConnection.HTTP_OK ) {
    			Log.v("refreshEarthQuakes()", "response NOT HTTP_OK...");
    			httpConnection.disconnect();
    			quakeFeed = getString(R.string.quake_feed_backup);
    			url = new URL(quakeFeed);
    			connection = url.openConnection();
    			Log.v("refreshEarthQuakes()", "backup connection opened...");
    			httpConnection = (HttpURLConnection)connection;
        		responseCode = httpConnection.getResponseCode();
    		}
    		
    		if ( responseCode == HttpURLConnection.HTTP_OK ) {
    			Log.v("refereshEarthQuakes()", "response HTTP_OK");
    			InputStream in = httpConnection.getInputStream();
    			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    			DocumentBuilder db = dbf.newDocumentBuilder();
    			Document dom = db.parse(in);
    			Element docEle = dom.getDocumentElement();
    			earthquakes.clear();
    			if ( getResources().getBoolean(R.bool.db_load_flag) ) {
    				Log.w("refreshEarthQuakes()", "loadQuakesFromProvider()");
    				loadQuakesFromProvider();
    			}
    			NodeList nl = docEle.getElementsByTagName("entry");
    			if ( nl != null && nl.getLength() > 0 ) {
    				for(int i = 0; i < nl.getLength(); i++) {
    					Element entry = (Element)nl.item(i);
    					Element title = (Element)entry.getElementsByTagName("title").item(0);
    					Element g = (Element)entry.getElementsByTagName("georss:point").item(0);
    					Element when = (Element)entry.getElementsByTagName("updated").item(0);
    					Element link = (Element)entry.getElementsByTagName("link").item(0);
    					String details = title.getFirstChild().getNodeValue();
    					String hostname = "http://earthquake.usgs.gov";
    					String linkString = hostname + link.getAttribute("href");
    					String point = g.getFirstChild().getNodeValue();
    					String dt = when.getFirstChild().getNodeValue();
    					SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd'T'hh:mm:ss'Z'");
    					Date qdate = new GregorianCalendar(0, 0, 0).getTime();
    					try {
    						qdate = sdf.parse(dt);
    					}
    					catch ( ParseException e ) {
    						e.printStackTrace();
    					}
    					
    					String[] location = point.split(" ");
    					Location l = new Location("GPS Fix");
    					l.setLatitude(Double.parseDouble(location[0]));
    					l.setLongitude(Double.parseDouble(location[1]));
    					
    					String magnitudeString = details.split(" ")[1];
    					int end = magnitudeString.length() - 1;
    					double magnitude = Double.parseDouble(magnitudeString.substring(0, end));
    					details = details.split(",")[1].trim();
    					
    					Quake quake = new Quake(qdate, details, l, magnitude, linkString);
    					addNewQuake(quake);
    				}
    			}	
    		}
    	}
    	catch ( MalformedURLException e ) {
    		Log.v("refreshEarthQuakes()", "MalformedURLException...");
    		e.printStackTrace();
    	}
    	catch ( IOException e ) {
    		Log.v("refreshEarthQuakes()", "IOException...");
    		e.printStackTrace();
    	}
    	catch ( ParserConfigurationException e ) {
    		Log.v("refreshEarthQuakes()", "ParserConfigurationException...");
    		e.printStackTrace();
    	}
    	catch ( SAXException e ) {
    		Log.v("refreshEarthQuakes()", "SAXException...");
    		e.printStackTrace();
    	}
    	finally {
    	
    	}
    }
    
    /*
    // addNewQuake prior to preferences
    private void addNewQuake(Quake q) {
		earthquakes.add(q);
		aryadptr.notifyDataSetChanged();
	}
	*/ 
    private void addNewQuake(Quake q) {
    	if ( getResources().getBoolean(R.bool.db_load_flag) ) {
    		addNewDBQuake(q);
    		return;
    	}
    	
    	if ( q.getMagnitude() > this.minimumMagnitude ) {
    		earthquakes.add(q);
    		aryadptr.notifyDataSetChanged();
    	}
    }
    
    private void addNewDBQuake(Quake q) {
    	ContentResolver cr = getContentResolver();
    	String w = 
    		EarthquakeProvider.KEY_DATE + " = " +
    		q.getDate().getTime();
    	
    	if ( cr.query(EarthquakeProvider.CONTENT_URI, 
    			null, w, null, null).getCount()==0 ) {
    		ContentValues values = new ContentValues();
    		
    		values.put(EarthquakeProvider.KEY_DATE,
    				q.getDate().getTime());
    		values.put(EarthquakeProvider.KEY_DETAILS,
    				q.getDetails());
    		
    		double lat = q.getLocation().getLatitude();
    		double lng = q.getLocation().getLongitude();
    		values.put(EarthquakeProvider.KEY_LOCATION_LAT,
    				lat);
    		values.put(EarthquakeProvider.KEY_LOCATION_LNG,
    				lng);
    		values.put(EarthquakeProvider.KEY_LINK,
    				q.getLink());
    		values.put(EarthquakeProvider.KEY_MAGNITUDE,
    				q.getMagnitude());
    		cr.insert(EarthquakeProvider.CONTENT_URI, values);
    		//this.earthquakes.add(q);
    		addQuakeToArray(q);
    	}
    }
    
    private void addQuakeToArray(Quake q) {
		if ( q.getMagnitude() > this.minimumMagnitude ) {
			this.earthquakes.add(q);
			aryadptr.notifyDataSetChanged();
		}
	}
    
    private void loadQuakesFromProvider() {
    	this.earthquakes.clear();
    	ContentResolver cr = getContentResolver();
    	Cursor c = cr.query(EarthquakeProvider.CONTENT_URI, 
    			null, null, null, null);
    	
    	if ( c == null ) return;
    	
    	if ( c.moveToFirst() ) {
    		do {
    			Long datems = c.getLong(EarthquakeProvider.DATE_COLUMN);
    			String details = c.getString(EarthquakeProvider.DETAILS_COLUMN);
    			Float lat = c.getFloat(EarthquakeProvider.LATITUDE_COLUMN);
    			Float lng = c.getFloat(EarthquakeProvider.LONGITUDE_COLUMN);
    			Double mag = c.getDouble(EarthquakeProvider.MAGNITUDE_COLUMN);
    			String link = c.getString(EarthquakeProvider.LINK_COLUMN);
    			
    			Location loc = new Location("gps_fix");
    			loc.setLongitude(lng);
    			loc.setLatitude(lat);
    			Date date = new Date(datems);
    			Quake q = new Quake(date, details, loc, mag, link);
    			addQuakeToArray(q);
    		} while ( c.moveToNext() );
    	}
    	
    }
    
    public boolean onCreateOptionsMenu(Menu menu) {
    	super.onCreateOptionsMenu(menu);
    	
    	menu.add(0, MENU_UPDATE, Menu.NONE, R.string.menu_update);
    	menu.add(0, MENU_PREFERENCES, Menu.NONE, R.string.menu_preferences);
    	
    	return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem mi) {
    	super.onOptionsItemSelected(mi);
    	
    	switch ( mi.getItemId() ) {
    	case MENU_UPDATE: {
    		updateFromPreferences();
    		refreshEarthquakes();
    		
    		return true;
    	}
    	case MENU_PREFERENCES: {
    		Intent i = new Intent(this, PreferencesAct.class);
    		startActivityForResult(i, SHOW_PREFERENCES);
    		return true;
    	}
    	}
    	
    	return false;
    }
    
    // Ch 06: Addition
    int minimumMagnitude = 0;
    boolean autoUpdate = false;
    int updateFreq = 0;
    
    // Ch 06: Addition
    private void updateFromPreferences() {
    	Context context = getApplicationContext();
    	SharedPreferences prefs =
    		PreferenceManager.getDefaultSharedPreferences(context);
    	
    	// get the index of the minimum magnitude value in
    	// minMagValues[]. 
    	int minMagIndex = prefs.getInt(PreferencesAct.PREF_MIN_MAG, 0);
    	//Toast t2 = Toast.makeText(this,
		//		"updateFromPreferences(): freqIndex = " + minMagIndex,
		//		Toast.LENGTH_LONG);
		//t2.show();
    	
    	if ( minMagIndex < 0 )
    		minMagIndex = 0;
    	
    	// get the index of the index in freqValues[]
    	int freqIndex = prefs.getInt(PreferencesAct.PREF_UPDATE_FREQ, 0);
    	
    	if ( freqIndex < 0 )
    		freqIndex = 0;
    	
    	autoUpdate = prefs.getBoolean(PreferencesAct.PREF_AUTO_UPDATE, false);
    	
    	Resources res = getResources();
    	int[] minMagValues = res.getIntArray(R.array.magnitude_values);
    	int[] freqValues = res.getIntArray(R.array.update_freq_values);
    	
    	minimumMagnitude = minMagValues[minMagIndex];
    	updateFreq = freqValues[freqIndex];
    }
    
    // Ch 06 addition
    @Override
    public void onActivityResult(int reqCode, int rsltCode, Intent data) {
    	super.onActivityResult(reqCode, rsltCode, data);
    	
    	if ( reqCode == SHOW_PREFERENCES ) {
    		if ( rsltCode == Activity.RESULT_OK ) {
    			updateFromPreferences();
    			refreshEarthquakes();
    		}
    	}
    }
}