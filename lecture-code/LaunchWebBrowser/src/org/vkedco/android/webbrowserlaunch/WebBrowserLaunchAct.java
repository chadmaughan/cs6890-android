package org.vkedco.android.webbrowserlaunch;

/*
 *************************************************
 * WebBrowserLaunchAct is the activity
 * that launches a browser intent, which, if
 * BrowserChoiceDemo is installed, causes
 * BrowserChoiceDemo to be added to the list
 * of web browsers that the user is asked
 * to choose from.
 * 
 * bugs, comments to vladimir dot kulyukin at 
 * usu dot edu
 ************************************************* 
 */

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class WebBrowserLaunchAct extends Activity {
  
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Intent myIntent = new Intent(Intent.ACTION_VIEW,
        		Uri.parse("http://en.wikipedia.org/wiki/Kurt_G�del"));
        this.startActivity(myIntent);
    }
}