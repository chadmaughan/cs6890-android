package org.vkedco.android.menuharness;

/*
 * *********************************************************
 * MenuHarnessAct.java is a harness for testing menus 
 * constructed programmatically.
 * 
 * I used some material in Ch. 5, "Pro Android 2" by
 * Sayed Hashimi, Satya Komatineni, and Dave MacLean
 * as inspiration.
 * 
 * Comments, bugs to vladimir dot kulyukin at gmail dot com
 * **********************************************************
 */

import android.app.Activity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MenuHarnessAct extends Activity {
	
	private Menu mMenu = null;
	private TextView mMessageBoard = null;
	
	// Create an Activity
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mMessageBoard = (TextView)findViewById(R.id.messageBoard);
       
        // Set the context menu if show_context_menu is true
        String str = getResources().getString(R.string.show_context_menu);
    	if ( str.equalsIgnoreCase("true") ) {
    		registerForContextMenu(mMessageBoard);
    	}
    }
    
    // onCreateOptionsMenu is the method that is automatically
    // called by Android when Activity is created.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	mMenu = menu;
    	
    	// add regular menu items to the menu created by Android
    	addRegularMenuItems(menu);
    	String str = getResources().getString(R.string.show_sec_menu_flag);
    	if ( str.equalsIgnoreCase("true") ) {
    		addSecondaryMenuItems(menu);
    	}
    	
    	String str2 = getResources().getString(R.string.show_submenu_flag);
    	if ( str2.equalsIgnoreCase("true") ) {
    		addSubMenu(menu);
    	}
    	
    	
    	return true;
    }
    
    // This method does the work of creating the menu
    private void addRegularMenuItems(Menu menu) {
    	int mainGroup = Menu.FIRST;
    	
    	menu.add(mainGroup, 1, 1, "append");
    	// If show_icon_flag is set to true, set the icon
    	// of the toast menu item
    	String str = getResources().getString(R.string.show_icon_flag);
    	if ( str.equalsIgnoreCase("true") ) {
    		MenuItem mi = menu.add(mainGroup, 2, 2, "toast");
    		mi.setIcon(R.drawable.menu_icon);
    	}
    	else {
    		menu.add(mainGroup, 2, 2, "toast");
    	}
    	menu.add(mainGroup, 3, 3, "clear");
    	
    	menu.add(mainGroup, 4, 4, "hide sec. menu");
    	menu.add(mainGroup, 5, 5, "show sec. menu");
    	
    	menu.add(mainGroup, 6, 6, "enable sec. menu");
    	menu.add(mainGroup, 7, 7, "disable sec. menu");
    	
    	menu.add(mainGroup, 8, 8, "check sec. menu");
    	menu.add(mainGroup, 9, 9, "uncheck sec. menu");
    }
    
    // populate menu with the secondary items
    private void addSecondaryMenuItems(Menu menu) {
    	int base = Menu.CATEGORY_SECONDARY;
    	
    	menu.add(base, base+1, base+1, "sec. item 1");
    	menu.add(base, base+2, base+2, "sec. item 2");
    	menu.add(base, base+3, base+3, "sec. item 3");
    	menu.add(base, base+4, base+4, "sec. item 4");
    	menu.add(base, base+5, base+5, "sec. item 5");
    }
    
    // add the submenu to the menu
    private void addSubMenu(Menu menu) {
    	int base = Menu.FIRST + 200;
    	
    	SubMenu sm = menu.addSubMenu(base, base+1, Menu.NONE, "Submenu");
    	String str = getResources().getString(R.string.show_submenu_icon);
		if ( str.equalsIgnoreCase("true") ) {
			sm.setIcon(R.drawable.submenu_icon);
		}
    	sm.add(base, base+2, base+2, "sub item 1");
    	sm.add(base, base+3, base+3, "sub item 2");
    	sm.add(base, base+4, base+4, "sub item 3");
    }
    
    // This is the method that handles clicks on the menu items
    // of the main menu.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	if ( item.getItemId() == 1 ) {
    		appendText("append selected...\n");
    	}
    	else if ( item.getItemId() == 2 ) {
    		appendText("toast selected...\n");
    		Toast t = Toast.makeText(this, mMessageBoard.getText().toString(), 
    				Toast.LENGTH_LONG);
    		t.show();
    	}
    	else if ( item.getItemId() == 3 ) {
    		clearText();
    	}
    	else if ( item.getItemId() == 4 ) {
    		appendMenuItemText(item);
    		mMenu.setGroupVisible(Menu.CATEGORY_SECONDARY, false);
    	}
    	else if ( item.getItemId() == 5 ) {
    		appendMenuItemText(item);
    		mMenu.setGroupVisible(Menu.CATEGORY_SECONDARY, true);
    	}
    	else if ( item.getItemId() == 6 ) {
    		appendMenuItemText(item);
    		mMenu.setGroupEnabled(Menu.CATEGORY_SECONDARY, true);
    	}
    	else if ( item.getItemId() == 7 ) {
    		appendMenuItemText(item);
    		mMenu.setGroupEnabled(Menu.CATEGORY_SECONDARY, false);
    	}
    	else if ( item.getItemId() == 8 ) {
    		appendMenuItemText(item);
    		mMenu.setGroupCheckable(Menu.CATEGORY_SECONDARY, true, false);
    	}
    	else if ( item.getItemId() == 9 ) {
    		appendMenuItemText(item);
    		mMenu.setGroupCheckable(Menu.CATEGORY_SECONDARY, false, false);
    	}
    	else {
    		this.appendMenuItemText(item);
    	}
    	
    	return true;
    }
    
    // This method method is called when a context menu is
    // constructed. The second argument is a view for which
    // the context menu is being created.
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, 
    		ContextMenuInfo menuInfo) {
    	menu.setHeaderTitle("Context Menu");
    	menu.add(500, 500, 500, "context item 1");
    	menu.add(501, 501, 501, "context item 2");
    }
    
    // This method is called when a context menu item is selected.
    @Override
    public boolean onContextItemSelected(MenuItem item) {
    	if ( item.getItemId() == 500 ) {
    		Toast t = Toast.makeText(this, "context item 1 selected",
    				Toast.LENGTH_LONG);
    		t.show();
    		return true;
    	}
    	else if ( item.getItemId() == 501 ) {
    		Toast t = Toast.makeText(this, "context item 2 selected",
    				Toast.LENGTH_LONG);
    		t.show();
    		return true;
    	}
    	else { 
    		return false;
    	}
    }
    
    private void appendText(String text) {
    	mMessageBoard.setText(mMessageBoard.getText() + text);
    }
    
    private void appendMenuItemText(MenuItem menuItem) {
    	String title = menuItem.getTitle().toString();
    	mMessageBoard.setText(mMessageBoard.getText() + "\n" + title);
    }
    
    private void clearText() {
    	mMessageBoard.setText("");
    }
    
    
}