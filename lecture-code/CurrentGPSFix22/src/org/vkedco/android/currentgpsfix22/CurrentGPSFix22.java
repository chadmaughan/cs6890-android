package org.vkedco.android.currentgpsfix22;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

// Runs on Android 2.2, minSDK=8
public class CurrentGPSFix22 extends Activity {
	private LocationManager  locMngr;
	private LocationListener locLstn;
	private TextView latValTV;
	private TextView lngValTV;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        latValTV = (TextView)findViewById(R.id.latValTV);
        lngValTV = (TextView)findViewById(R.id.lngValTV);
        
        locMngr = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        locLstn = new LocationListener() {

			@Override
			public void onLocationChanged(Location loc) {
				if ( loc != null ) {
					double lat = loc.getLatitude();
					double lng = loc.getLongitude();
					latValTV.setText(""+lat);
					lngValTV.setText(""+lng);
					String mssg = "";
					mssg += "[lat: " + lat + ", ";
					mssg += "lng: "  + lng + "]";
					Toast.makeText(getBaseContext(), mssg, Toast.LENGTH_LONG).show();	
				}
			}

			@Override
			public void onProviderDisabled(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderEnabled(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
				// TODO Auto-generated method stub
				
			}
        	
        };
        
        this.locMngr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
        		this.locLstn);
    }
}