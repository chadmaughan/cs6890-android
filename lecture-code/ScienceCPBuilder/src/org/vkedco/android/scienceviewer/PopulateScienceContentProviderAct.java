package org.vkedco.android.scienceviewer;

/*
 ***********************************************************
 * PopulateScienceContentProvider is a component of the 
 * ScienceCPBuilder application. The application application 
 * 1) parses an XML resource stored in a Dropbox folder with 
 * a list of five famous mathematicians; 2) creates a SQLite 
 * database with a table where every mathematician element 
 * becomes a record; 3) encapsulates that database in a content 
 * provider; and 4)  registers that content provider on the 
 * Android device. The XML resource file dynamically parsed 
 * by the application is available at 
 * http://dl.dropbox.com/u/16201259/teaching/ANDROID/mathematicians.xml. 
 * The XML resource remains in the cloud and is not a static 
 * resource. The resource is parsed into a SQLite database at 
 * run time.
 * 
 * PopulateScienceContentProvider populates the database 
 * encapsulated by the content provider defined in 
 * ScienceContentProvider.java with records of mathematicians.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
*************************************************************
*/

import java.util.ArrayList;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class PopulateScienceContentProviderAct extends Activity 
{
	private ArrayList<Mathematician> mathematicians = null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mathematicians = new ArrayList<Mathematician>();
        populateContentProvider();
        TextView tv = (TextView)this.findViewById(R.id.tv);
        tv.setText(mathematiciansToString());
    }
   
    private void populateContentProvider() {
    	XMLDocBuilder.buildDoc(getApplicationContext(), mathematicians);
    	ContentResolver cr = getContentResolver();
    	
    	for(Mathematician m : mathematicians) {
    		ContentValues cvals = new ContentValues();
    		cvals.put(ScienceContentProvider.COL_FN_NM, m.firstName);
    		cvals.put(ScienceContentProvider.COL_LN_NM, m.lastName);
    		cvals.put(ScienceContentProvider.COL_RSRCH_NM, m.research);
    		cvals.put(ScienceContentProvider.COL_WIKI_NM, m.wiki);
    		Log.w(ScienceContentProvider.TAG, "success");
    		cr.insert(ScienceContentProvider.CONTENT_URI, cvals);
    	}
    }
    
    private String mathematiciansToString() 
    {
    	if ( mathematicians == null ) return "DB failure";
    	
    	StringBuilder sb = new StringBuilder();
    	String separator = "****************\n";
    	for(Mathematician m : mathematicians) {
    		sb.append(m.toString());
    		sb.append(separator);
    	}
    	
    	return sb.toString();
    }
}