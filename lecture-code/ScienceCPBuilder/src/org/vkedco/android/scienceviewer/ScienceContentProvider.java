package org.vkedco.android.scienceviewer;

/*
 ***********************************************************
 * ScienceContentProvider is the content provider component of the 
 * ScienceCPBuilder application. The application application 
 * 1) parses an XML resource stored in a Dropbox folder with 
 * a list of five famous mathematicians; 2) creates a SQLite 
 * database with a table where every mathematician element 
 * becomes a record; 3) encapsulates that database in a content 
 * provider; and 4)  registers that content provider on the 
 * Android device. The XML resource file dynamically parsed 
 * by the application is available at 
 * http://dl.dropbox.com/u/16201259/teaching/ANDROID/mathematicians.xml. 
 * The XML resource remains in the cloud and is not a static 
 * resource. The resource is parsed into a SQLite database at 
 * run time. 
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
*************************************************************
*/

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;


public class ScienceContentProvider extends ContentProvider 
{
	private SQLiteDatabase scienceDB;
	public static final String  TAG = "ScienceContentProvider";
	private static final String DB_NAME = "science.db";
	private static final int    DB_VERSION = 1;
	private static final String MATHEMATICIANS_TABLE = "mathematicians";
	
	// URI for this provider
	public static final String AUTHORITY = "org.vkedco.provider.science";
	public static final Uri    CONTENT_URI =
		Uri.parse("content:" + "//" + 
					AUTHORITY +
					"/" + 
					MATHEMATICIANS_TABLE);
	
	public static final String COL_ID_NM    = "_id";
	
	public static final String COL_FN_NM    = "first_name";
	public static final int    COL_FN_IX    = 1;
	
	public static final String COL_LN_NM    = "last_name";
	public static final int    COL_LN_IX    = 2;
	
	public static final String COL_RSRCH_NM = "research";
	public static final int    COL_RSRCH_IX = 3;
	
	public static final String COL_WIKI_NM  = "wiki";
	public static final int    COL_WIKI_IX  = 4;
	
	private static final int   MATH_ALL 	= 1;
	private static final int   MATH_ONE 	= 2;
	
	private static final String MATH_ALL_MIME = 
		"vnd.android.cursor.dir/vnd.vkedco.scienceviewer";
	private static final String MATH_ONE_MIME = 
		"vnd.android.cursor.item/vnd.vkedco.scienceviewer";
	
	private static final UriMatcher uriMatcher;
	
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		// a URI that ends in mathematicians corresponds to a request
		// for all earthquakes
		
		uriMatcher.addURI(AUTHORITY, 
						  MATHEMATICIANS_TABLE, 
						  MATH_ALL);
		// a URI that ends in "mathematicians" with a number
		// corresponds to a request to retrieve a specific earthquake
		uriMatcher.addURI(AUTHORITY, 
						  MATHEMATICIANS_TABLE + "/#",
						  MATH_ONE);
	}
	
	private static class scienceDBHelper extends SQLiteOpenHelper 
	{
		// db creation table command
		private static final String CREATE_TABLE = 
			"create table "    + MATHEMATICIANS_TABLE + 
			" ("
			+ COL_ID_NM    + " integer primary key autoincrement, "
			+ COL_FN_NM    + " TEXT, "
			+ COL_LN_NM    + " TEXT, "
			+ COL_RSRCH_NM + " TEXT, "
			+ COL_WIKI_NM  + " TEXT"
			+ ");";
		
		public scienceDBHelper(Context context, String name, 
							   CursorFactory factory, int version) 
		{
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) 
		{
			db.execSQL(CREATE_TABLE);
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
		{
			Log.w(TAG, "Upgrading database from versoin " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE);
			onCreate(db);
		}
		
	}
	
	@Override
	public boolean onCreate() {
		Context context = getContext();
		scienceDBHelper dbHelper
			= new scienceDBHelper(context, DB_NAME, null, DB_VERSION);
		scienceDB = dbHelper.getWritableDatabase();
		return ( scienceDB == null) ? false : true;
	}

	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		int count;
		switch ( uriMatcher.match(uri) ) {
		case MATH_ALL:
			count = scienceDB.delete(MATHEMATICIANS_TABLE, where, whereArgs);
			break;
		case MATH_ONE:
			String segment = uri.getPathSegments().get(1);
			count = scienceDB.delete(MATHEMATICIANS_TABLE,
					COL_ID_NM + "=" + segment
					+ (!TextUtils.isEmpty(where) ? " AND ("
							+ where + ")" : ""), whereArgs);
			break;
		default: throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		switch ( uriMatcher.match(uri) ) {
		// if uriMatcher returns MATH_ALL
		case MATH_ALL: 
			return MATH_ALL_MIME;
		// if uriMatcher returns MATH_ONE
		case MATH_ONE: 
			return MATH_ONE_MIME;
		default: throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public Uri insert(Uri inuri, ContentValues initValues) {
		long rowID = scienceDB.insert(MATHEMATICIANS_TABLE, "mathematician",
				initValues);
		
		if ( rowID > 0 ) {
			Uri uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
			getContext().getContentResolver().notifyChange(uri, null);
			return uri;
		}
		throw new SQLException("Failed to insert row into " + inuri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) 
	{
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(MATHEMATICIANS_TABLE);
		switch( uriMatcher.match(uri) ) {
		// if this is a row query, add the where clause with the
		// appropriate row number.
		case MATH_ONE: {
			qb.appendWhere(COL_ID_NM + "=" + uri.getPathSegments().get(1));
			//for(String s : uri.getPathSegments())
			//	Log.w(TAG, s);
			break;
		}
		default: break;
		
		}
		
		String orderBy;
		// if the sort string is empty, sort by date
		if ( TextUtils.isEmpty(sortOrder) ) {
			orderBy = COL_LN_NM;
		}
		else {
			orderBy = sortOrder;
		}
		
		// execute the query against the database
		Cursor c = qb.query(scienceDB, 
							projection, 
							selection, 
							selectionArgs, 
							null, null, 
							orderBy);
		// notify the content resolver about the change
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String where,
			String[] whereArgs) {
		int count;
		switch ( uriMatcher.match(uri) ) {
		case MATH_ALL:
			count = scienceDB.update(MATHEMATICIANS_TABLE,
					values, where, whereArgs);
			break;
		case MATH_ONE:
			String segment = uri.getPathSegments().get(1);
			count = scienceDB.update(MATHEMATICIANS_TABLE,
					values,
					COL_ID_NM + "=" + segment
					+ (!TextUtils.isEmpty(where) ? " AND ("
							+ where + ")" : ""),
							whereArgs);
			break;
		default: throw new IllegalArgumentException("Uknown URI " + uri);
		}
		
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

}
