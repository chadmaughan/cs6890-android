package org.vkedco.android.scienceviewer;

/*
 ***********************************************************
 * Mathematician class whose objects are constructed from
 * XML.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 ***********************************************************
 */

public class Mathematician {
	
	public String firstName;
	public String lastName;
	public String research;
	public String wiki;
	
	public Mathematician(String fn, String ln, String rs, String wk)
	{
		firstName = fn;
		lastName  = ln;
		research  = rs;
		wiki      = wk;
	}
	
	public Mathematician()
	{
		firstName = "";
		lastName  = "";
		research  = "";
		wiki      = "";
	}
	
	public String toString() 
	{
		return (
				"First Name: " + firstName + "\n" +
				"Last  Name: " + lastName  + "\n" +
				"Research:   " + research  + "\n" +
				"Wiki:       " + wiki      + "\n"
				);
	}

}
