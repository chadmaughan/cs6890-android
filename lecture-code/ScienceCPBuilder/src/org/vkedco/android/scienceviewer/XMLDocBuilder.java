package org.vkedco.android.scienceviewer;

/*
 ***********************************************************
 * XMLDocBuilder is a component of the 
 * ScienceCPBuilder application. The application application 
 * 1) parses an XML resource stored in a Dropbox folder with 
 * a list of five famous mathematicians; 2) creates a SQLite 
 * database with a table where every mathematician element 
 * becomes a record; 3) encapsulates that database in a content 
 * provider; and 4)  registers that content provider on the 
 * Android device. The XML resource file dynamically parsed 
 * by the application is available at 
 * http://dl.dropbox.com/u/16201259/teaching/ANDROID/mathematicians.xml. 
 * The XML resource remains in the cloud and is not a static 
 * resource. The resource is parsed into a SQLite database at 
 * run time.
 * 
 * XMLDocBuilder parses the XML document and populates an array 
 * with Mathematician objects defined Mathematician.java.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 * 
 * The entire XML file is given below for easy of reference.

   <?xml version="1.0" encoding="utf-8" ?>
   <mathematicians>
 
    <mathematician>
       <first_name>Kurt</first_name>
       <last_name>Godel</last_name>
       <research>mathematical logic</research>
       <wiki>http://en.wikipedia.org/wiki/Kurt_Godel</wiki>
    </mathematician>

    <mathematician>
       <first_name>David</first_name>
       <last_name>Hilbert</last_name>
       <research>invariant theory</research>
       <wiki>http://en.wikipedia.org/wiki/David_Hilbert</wiki>
    </mathematician>

    <mathematician>
       <first_name>Giuseppe</first_name>
       <last_name>Peano</last_name>
       <research>number theory</research>
       <wiki>http://en.wikipedia.org/wiki/Giuseppe_Peano</wiki>
    </mathematician>

    <mathematician>
       <first_name>Georg</first_name>
       <last_name>Cantor</last_name>
       <research>set theory</research>
       <wiki>http://en.wikipedia.org/wiki/Georg_Cantor</wiki>
    </mathematician>

    <mathematician>
       <first_name>Alonzo</first_name>
       <last_name>Church</last_name>
       <research>theory of computation</research>
       <wiki>http://en.wikipedia.org/wiki/Alonzo_Church</wiki>
    </mathematician>

</mathematicians>
   
*************************************************************
*/

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import org.w3c.dom.*;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class XMLDocBuilder {
	
	public static Document buildDoc(Context cntxt, ArrayList<Mathematician> mlst) 
	{
		Document doc = null;
		
		try {
			Resources res = cntxt.getResources();
			String rssFeed = res.getString(R.string.rss_feed);
			URL url = new URL(rssFeed);
			URLConnection connection = url.openConnection();
			Log.v("XMLDocBuilder.buildDoc()", "connection opened...");
			HttpURLConnection httpConnection = (HttpURLConnection)connection;
			int responseCode = httpConnection.getResponseCode();
			if ( responseCode == HttpURLConnection.HTTP_OK ) {
				InputStream in = httpConnection.getInputStream();
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder parser = factory.newDocumentBuilder();
				doc = parser.parse(in);
				Element docRoot = doc.getDocumentElement();
				NodeList listOfMathematicians = 
					docRoot.getElementsByTagName(res.getString(R.string.mathematician_tag));
				Log.v("LIST LENGTH:", Integer.toString(listOfMathematicians.getLength()));
				if ( listOfMathematicians != null ) {
					for(int i = 0; i < listOfMathematicians.getLength(); i++) {
						Element mathematician = (Element)listOfMathematicians.item(i);
						Element firstNameEl = 
							(Element)mathematician.
								getElementsByTagName(res.getString(R.string.first_name_tag)).item(0);
						String first_name = firstNameEl.getFirstChild().getNodeValue();
						
						Element lastNameEl =
							(Element)mathematician.
								getElementsByTagName(res.getString(R.string.last_name_tag)).item(0);
						String last_name = lastNameEl.getFirstChild().getNodeValue();
						
						Element researchEl =
							(Element)mathematician.
								getElementsByTagName(res.getString(R.string.research_tag)).item(0);
						String research = researchEl.getFirstChild().getNodeValue();
						
						Element wikiEl =
							(Element)mathematician.
								getElementsByTagName(res.getString(R.string.wiki_tag)).item(0);
						String wiki = wikiEl.getFirstChild().getNodeValue();
					
						mlst.add(new Mathematician(first_name, last_name,
									research, wiki));
					}
				}
			}
			return doc;
		}
		catch (Exception e) {
			Log.e("DOM Exception", e.toString());
			return null;
		}
		
	}

}
