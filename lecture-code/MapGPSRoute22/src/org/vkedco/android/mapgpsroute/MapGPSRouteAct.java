package org.vkedco.android.mapgpsroute;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

public class MapGPSRouteAct extends MapActivity {
	private String locStr = "";
	private TextView currentLocTV; 
	private String addressStr = "";
	private MapController mapController;
	private Geocoder gcdr;
	private LocationManager  locMngr;
	private LocationListener locLstnr;
	private MapView mapView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        currentLocTV = (TextView)findViewById(R.id.currentLocTV);
        mapView = (MapView)findViewById(R.id.mapView);
        mapController = mapView.getController();
        
        mapView.setSatellite(true);
        mapView.setStreetView(true);
        mapView.displayZoomControls(false);
        
        mapController.setZoom(30);
        gcdr = new Geocoder(this, Locale.getDefault());
        
        locLstnr = new LocationListener() {

			@Override
			public void onLocationChanged(Location loc) {
				processLocation(loc);
			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatusChanged(String provider, 
					int status, Bundle extras) {
				// TODO Auto-generated method stub
				
			}
		
        };
        
        locMngr = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        locMngr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 
        		0, 0, locLstnr);
    }

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void processLocation(Location loc) {
		
		if ( loc != null ) {
			Double lat = loc.getLatitude()*1E6;
			Double lng = loc.getLongitude()*1E6;
			GeoPoint gp = new GeoPoint(lat.intValue(),
					lng.intValue());
			mapController.animateTo(gp);
			getLocationInfo(loc);
		}
	}
	
	private void getLocationInfo(Location loc) {
		double lat = loc.getLatitude();
		double lng = loc.getLongitude();
		locStr = "";
		locStr += "Lat: " + lat + "\n";
		locStr += "Lng: " + lng;
		try {
			List<Address> addressList =
				gcdr.getFromLocation(lat, lng, 1);
			StringBuilder sb = new StringBuilder();
			if ( addressList.size() > 0 ) {
				Address address = addressList.get(0);
				
				for(int i = 0; i < address.getMaxAddressLineIndex(); i++)
					sb.append(address.getAddressLine(i)).append("\n");
				
				sb.append(address.getLocality()).append("\n");
				sb.append(address.getPostalCode()).append("\n");
				sb.append(address.getCountryName());
				this.addressStr += sb.toString();
			}
			else {
				addressStr = "No address available";
			}
		}
		catch ( IOException ex) {
			addressStr = "Exception: No address available";
		}
		this.currentLocTV.setText("Current Location Info:\n"
				+ locStr + "\n" + addressStr);
		
	}
}