package org.vkedco.android.activitydatasharing;

/*
 * *************************************************************** 
 * File: ActivityTwo.java
 * One of the four activities of this application. It is launched
 * when the user presses the "Run 1" button in the GUI constructed
 * by onCreate() of ActivityDataSharingAct.java
 * 
 * Bugs, comments to vladimir dot kulyukin at usu dot edu
 * **************************************************************
 */

import java.util.Date;

import android.app.Activity;
import android.os.Bundle;

public class ActivityThree extends Activity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Place the message under the key for ActivityThree stored
        // as a constant string in ActivityDataExchange. The
        // value of the message is "Run at " plus the string representation
        // of the current Date object.
        ActivityDataExchange.putMessage(ActivityDataExchange.getActivityThreeKey(), 
        		"Run at " + new Date().toString());
        // set the result to Activity.RESULT_OK. This is critical, because
        // it is this result that will be checked in ActivityDataSharingAct.java
        // in the onActivityResult method that will be run when the activity 
        // finishes.
        setResult(Activity.RESULT_OK);
        finish();
    }

}
