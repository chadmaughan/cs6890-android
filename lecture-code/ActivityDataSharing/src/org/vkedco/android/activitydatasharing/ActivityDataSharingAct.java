package org.vkedco.android.activitydatasharing;

/*
 * *************************************************************** 
 * File: ActivityDataSharingAct.java
 * ActivityDataSharingAct is the main activity (one of the four
 * activities of this application) launched when the application 
 * starts. 
 * 
 * This activity constructs the GUI and the message board in ActivityDataExchange, 
 * defines how the other three activities, ActivityOne, ActivityTwo, 
 * ActivityThree, are launched through Intents on when
 * the appropriate buttons (Run 1, Run 2, and Run 3) are clicked.
 * 
 * Bugs, comments to vladimir dot kulyukin at usu dot edu
 * **************************************************************
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ActivityDataSharingAct extends Activity 
	implements OnClickListener
{
	// Three buttons and three edit texts that will
	// be inflated from infosharing_table.xml
	private Button runActivity1Button = null;
	private Button runActivity2Button = null;
	private Button runActivity3Button = null;
	private EditText activity1Output = null;
	private EditText activity2Output = null;
	private EditText activity3Output = null;
	
	// activity requests code necessary for Intent construction
	private static final int ACTIVITY_ONE_REQCODE = 1;
	private static final int ACTIVITY_TWO_REQCODE = 2;
	private static final int ACTIVITY_THREE_REQCODE = 3;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infosharing_table);
        
        // inflate the buttons
        runActivity1Button = (Button)findViewById(R.id.run_activity_1_button);
        runActivity2Button = (Button)findViewById(R.id.run_activity_2_button);
        runActivity3Button = (Button)findViewById(R.id.run_activity_3_button);
        
        // assign this class as the one that implements the
        // OnClickListener interface
        runActivity1Button.setOnClickListener(this);
        runActivity2Button.setOnClickListener(this);
        runActivity3Button.setOnClickListener(this);
        
        // inflate the three edit text views where the
        // activities will be posting their output
        activity1Output = (EditText)findViewById(R.id.activity_1_output);
        activity2Output = (EditText)findViewById(R.id.activity_2_output);
        activity3Output = (EditText)findViewById(R.id.activity_3_output);
        
        // create the message board inside ActivityDataExchange
        ActivityDataExchange.createMessageBoard();
    }

	@Override
	public void onClick(View view) {
		Button btn = (Button) view;
		int id = btn.getId();
		if ( id == R.id.run_activity_1_button ) {
			// create an explicit intent to launch ActivityOne
			Intent i1 = new Intent(ActivityDataSharingAct.this, ActivityOne.class);
			// start the intent resolution process under a specific request code.
			startActivityForResult(i1, ACTIVITY_ONE_REQCODE);
		}
		else if ( id == R.id.run_activity_2_button ) {
			Intent i2 = new Intent(ActivityDataSharingAct.this, ActivityTwo.class);
			startActivityForResult(i2, ACTIVITY_TWO_REQCODE);
		}
		else if ( id == R.id.run_activity_3_button ) {
			Intent i3 = new Intent(ActivityDataSharingAct.this, ActivityThree.class);
			startActivityForResult(i3, ACTIVITY_THREE_REQCODE);
		}
	}
	
	
	// When an activity returns, this method will be called. This is where
	// the activity request codes defined above are needed to figure out
	// which activity has finished and what the returned result is.
	// For example, if ActivityOne has returned, which can be checked
	// by comparing requestCode with ACTIVITY_ONE_REQCODE, check if the 
	// result is Activity.RESULT_OK and, if so, modify the appropriate edit 
	// text object with the message the activity has put into the message 
	// board under its key. 
	// Apply the same logic to the other two activities.
	// In case an activity returns some result other than Activity.RESULT_OK,
	// modify the appropriate edit text with the constant failure
	// message string from defined in ActivitDataExchange.
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if ( requestCode == ACTIVITY_ONE_REQCODE ) {
			if ( resultCode == Activity.RESULT_OK ) {
				activity1Output.setText(ActivityDataExchange.
						getMessage(ActivityDataExchange.
								getActivityOneKey()));
			}
			else {
				activity1Output.setText(ActivityDataExchange.
						getActivityFailureMessage());	
			}
		}
		else if ( requestCode == ACTIVITY_TWO_REQCODE ) {
			if ( resultCode == Activity.RESULT_OK ) {
				activity2Output.setText(ActivityDataExchange.
						getMessage(ActivityDataExchange.
								getActivityTwoKey()));
			}
			else {
				activity2Output.setText(ActivityDataExchange.
						getActivityFailureMessage());
			}
		}
		else if ( requestCode == ACTIVITY_THREE_REQCODE ) {
			if ( resultCode == Activity.RESULT_OK ) {
				activity3Output.setText(ActivityDataExchange.
						getMessage(ActivityDataExchange.
								getActivityThreeKey()));
			}
			else {
				activity3Output.setText(ActivityDataExchange.
						getActivityFailureMessage());
			}
		}
	}
}