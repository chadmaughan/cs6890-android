package org.vkedco.android.activitydatasharing;

/*
 * *************************************************************** 
 * File: ActivityDataExchange.java
 * ActivityDataExchange is a class that hosts a message board
 * implemented as a hash table into which and from which
 * the activities post and read messages. Each message
 * consists of a key and a value, both implemented as strings.
 * 
 * bugs, comments to vladimir dot kulyukin at usu dot edu
 * **************************************************************
 */

import java.util.Hashtable;

public class ActivityDataExchange
{
	private static Hashtable<String, String> mMessageBoard = null;
	// constant keys used by the activities for message posting
	// and retrieving messages.
	private static final String ACTIVITY_ONE_KEY   = "Activity 1";
	private static final String ACTIVITY_TWO_KEY   = "Activity 2";
	private static final String ACTIVITY_THREE_KEY = "Activity 3";
	private static final String ACTIVITY_FAILURE   = "Failed to run/return";
	
	public static String getMessage(String key) {
		return mMessageBoard.get(key);
	}
	
	public static String putMessage(String key, String msg) {
		return mMessageBoard.put(key, msg);
	}
	
	public static String getActivityOneKey() {
		return ACTIVITY_ONE_KEY;
	}
	
	public static String getActivityTwoKey() {
		return ACTIVITY_TWO_KEY;
	}
	
	public static String getActivityThreeKey() {
		return ACTIVITY_THREE_KEY;
	}
	
	public static String getActivityFailureMessage() {
		return ACTIVITY_FAILURE;
	}
	
	public static void createMessageBoard() {
		if ( mMessageBoard == null )
			mMessageBoard = new Hashtable<String, String>();
	}
}
