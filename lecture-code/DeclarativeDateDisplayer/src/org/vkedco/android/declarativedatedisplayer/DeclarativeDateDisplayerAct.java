package org.vkedco.android.declarativedatedisplayer;

/*
 * **********************************************************
 * DeclarativeDateDisplayer.java builds a simple Android GUI
 * that consists of a text view, an edit view, and a button
 * arranged into a horizontal linear layout. When pressed, 
 * the button updates the string in the edit text with the
 * current date.
 * 
 * The GUI is laid out in XML in /res/layout/main.xml 
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 * **********************************************************
 */

import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class DeclarativeDateDisplayerAct extends Activity 
{
    
	Button dateUpdateButton = null;
	Button clearDateButton = null;
	EditText dateMessage = null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        dateUpdateButton = (Button)findViewById(R.id.date_update_button);
        dateUpdateButton.setOnClickListener(
        		new OnClickListener() {
					public void onClick(View v) {
						dateMessage.setText(new Date().toString());
					}
        			
        		});
        
        clearDateButton = (Button)findViewById(R.id.clear_date_button);
        clearDateButton.setOnClickListener(
        		new OnClickListener() {
        			
					public void onClick(View v) {
						dateMessage.setText("");	
					}
        			
        		}
        );
        
        dateMessage = (EditText)findViewById(R.id.date_message);
    }

	
}