package org.vkedco.android.listviewarrayadapter1;

/*
 * ********************************************************************
 * An example of how to define a ListActivity that
 * 
 * 1) uses an ArrayAdapter on a statically allocated 
 * array of strings with five literary works by
 * Jelalludin Rumi to generate the child views for its 
 * list view container;
 * 2) Define an onItemClick method that displays a Toast
 * with the information about the parent, view, position,
 * and id of the clicked view;
 * 3) enables run-time word completion in its list view. 
 * 
 * Bugs, comments, questions to vladimir dot kulyukin at usu dot edu
 * ********************************************************************
 */

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ListViewArrayAdapter1Act extends ListActivity 
	implements OnItemClickListener
{
	// Some works of Jelalluddin Rumi
	private static final String[] mRumiWorks = {
		"Spiritual Couplets",
		"The Works of Shams of Tabriz",
		"In It What's In It",
		"Seven Sessions",
		"The Letters"
	};
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // No need to setContentView here. What is done below
        // automatically adds a ListView to fill the entire screen.
        // setContentView(R.layout.main);
        
        // 1. Get the list view of the current activity
        ListView lv = this.getListView();
        // 2. Create an adapter
        ArrayAdapter<String> adptr = 
        	new ArrayAdapter<String>(this, 
    			android.R.layout.simple_list_item_1, 
    			mRumiWorks);
        // 3. Bind the adapter to the list view
        lv.setAdapter(adptr);
        // 4. Enable word completion
        lv.setTextFilterEnabled(true);
        // 5. set this class as the implementation of
        // the OnItemClickListener interface
        lv.setOnItemClickListener(this);
        
        /*
        // This is another, more succinct, way of doing the
        // above w/o creating local variables.
        this.getListView().setAdapter(new ArrayAdapter<String>(this, 
    			android.R.layout.simple_list_item_1, 
    			mRumiWorks));
        this.getListView().setTextFilterEnabled(true);
        this.getListView().setOnItemClickListener(this);
        */
    }

    /*
     * parent is a ListView (this is the container)
     * view is a TextView (this is the child view)
     * position is the position of the child view inside the parent container
     * id is the id of the TextView
     */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		String msg = "";
		msg += "PARENT = " + parent.toString() + "\n";
		msg += "VIEW = " + view.toString() + "\n";
		msg += "VIEW'S TEXT = " + 
			((TextView)view).getText().toString() 
				+ "\n";
		msg += "POSITION = " + Integer.toString(position) + "\n";
		msg += "ID = " + Long.toString(id);
		// Make a toast and display it on the screen
		Toast t = Toast.makeText(this, msg, Toast.LENGTH_LONG);
		t.show();	
	}
}