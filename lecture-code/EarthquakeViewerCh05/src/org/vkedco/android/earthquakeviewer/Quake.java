package org.vkedco.android.earthquakeviewer;

import java.util.Date;
import java.text.SimpleDateFormat;
import android.location.Location;

public class Quake {
	
	private Date mDate;
	private String mDetails;
	private Location mLocation;
	private double mMagnitude;
	private String mLink;
	
	public Date getDate() { return mDate; }
	public String getDetails() { return mDetails; }
	public Location getLocation() { return mLocation; }
	public double getMagnitude() { return mMagnitude; }
	public String getLink() { return mLink; }
	
	public Quake(Date d, String det, Location loc, double magn,
			String link) {
		mDate = d;
		mDetails = det;
		mLocation = loc;
		mMagnitude = magn;
		mLink = link;
	}

	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH.mm");
		String dateStr = sdf.format(mDate);
		return dateStr + ": " + mMagnitude + " " + mDetails;
	}
	
	
}
