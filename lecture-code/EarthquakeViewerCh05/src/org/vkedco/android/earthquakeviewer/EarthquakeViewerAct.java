package org.vkedco.android.earthquakeviewer;

/*
 ***************************************************************
 * EarthquakeViewerAct is the main activity of the EarthquakeViewer
 * application described in Chapter 5 of "Pro Android 2 Application
 * Development" by Rito Meier.
 ***************************************************************
 */

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class EarthquakeViewerAct extends Activity {
	
	ListView earthquakeListView;
	ArrayAdapter<Quake> aryadptr;
	ArrayList<Quake> earthquakes = new ArrayList<Quake>();
	
	private static final int QUAKE_DIALOG = 1;
	private static final int MENU_UPDATE = Menu.FIRST;
	private static final int MENU_PREFERENCES = Menu.FIRST+1;
	private static final int SHOW_PREFERENCES = 1;
	
	Quake selectedQuake;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        earthquakeListView = (ListView)findViewById(R.id.earthquakeListView);

        earthquakeListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> av, View v, int index,
					long arg3) {
				selectedQuake = earthquakes.get(index);
				showDialog(QUAKE_DIALOG);
			}
        	
        });
        
        
        int layoutID = android.R.layout.simple_expandable_list_item_1;
        aryadptr = new ArrayAdapter<Quake>(this, layoutID, earthquakes);
        earthquakeListView.setAdapter(aryadptr);
        
        refreshEarthquakes();
    }
    
    public Dialog onCreateDialog(int id) {
    	switch(id) {
    	case (QUAKE_DIALOG): 
    		LayoutInflater li = LayoutInflater.from(this);
    		View quakeDetailsView = li.inflate(R.layout.quake_details, null);
    		AlertDialog.Builder quakeDialog = new AlertDialog.Builder(this);
    		quakeDialog.setTitle("Quake Time");
    		quakeDialog.setView(quakeDetailsView);
    		return quakeDialog.create();
    	}
    	return null;
    }
    
    public void onPrepareDialog(int id, Dialog dialog) {
    	switch (id) {
    	case (QUAKE_DIALOG):
    		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
    		String dateString = sdf.format(selectedQuake.getDate());
    		String quakeText = "Magnitude " + selectedQuake.getMagnitude() +
    			"\n" + selectedQuake.getDetails() + "\n" + selectedQuake.getLink();
    		AlertDialog quakeDialog = (AlertDialog)dialog;
    		quakeDialog.setTitle(dateString);
    		TextView tv = (TextView)quakeDialog.findViewById(R.id.quakeDetailsTextView);
    		tv.setText(quakeText);
    		break;
    	}
    }
    
    private void refreshEarthquakes() {
    	URL url;
    	try {
    		String quakeFeed = getString(R.string.quake_feed);
    		url = new URL(quakeFeed);
    		URLConnection connection;
    		connection = url.openConnection();
    		Log.v("refreshEarthQuakes()", "connection opened...");
    		HttpURLConnection httpConnection = (HttpURLConnection)connection;
    		int responseCode = httpConnection.getResponseCode();
    		
    		if ( responseCode != HttpURLConnection.HTTP_OK ) {
    			Log.v("refreshEarthQuakes()", "response NOT HTTP_OK...");
    			httpConnection.disconnect();
    			quakeFeed = getString(R.string.quake_feed_backup);
    			url = new URL(quakeFeed);
    			connection = url.openConnection();
    			Log.v("refreshEarthQuakes()", "backup connection opened...");
    			httpConnection = (HttpURLConnection)connection;
        		responseCode = httpConnection.getResponseCode();
    		}
    		
    		if ( responseCode == HttpURLConnection.HTTP_OK ) {
    			Log.v("refereshEarthQuakes()", "response HTTP_OK");
    			InputStream in = httpConnection.getInputStream();
    			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    			DocumentBuilder db = dbf.newDocumentBuilder();
    			Document dom = db.parse(in);
    			Element docEle = dom.getDocumentElement();
    			earthquakes.clear();
    			NodeList nl = docEle.getElementsByTagName("entry");
    			if ( nl != null && nl.getLength() > 0 ) {
    				for(int i = 0; i < nl.getLength(); i++) {
    					Element entry = (Element)nl.item(i);
    					Element title = (Element)entry.getElementsByTagName("title").item(0);
    					Element g = (Element)entry.getElementsByTagName("georss:point").item(0);
    					Element when = (Element)entry.getElementsByTagName("updated").item(0);
    					Element link = (Element)entry.getElementsByTagName("link").item(0);
    					String details = title.getFirstChild().getNodeValue();
    					String hostname = "http://earthquake.usgs.gov";
    					String linkString = hostname + link.getAttribute("href");
    					String point = g.getFirstChild().getNodeValue();
    					String dt = when.getFirstChild().getNodeValue();
    					SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd'T'hh:mm:ss'Z'");
    					Date qdate = new GregorianCalendar(0, 0, 0).getTime();
    					try {
    						qdate = sdf.parse(dt);
    					}
    					catch ( ParseException e ) {
    						e.printStackTrace();
    					}
    					
    					String[] location = point.split(" ");
    					Location l = new Location("GPS Fix");
    					l.setLatitude(Double.parseDouble(location[0]));
    					l.setLongitude(Double.parseDouble(location[1]));
    					
    					String magnitudeString = details.split(" ")[1];
    					int end = magnitudeString.length() - 1;
    					double magnitude = Double.parseDouble(magnitudeString.substring(0, end));
    					details = details.split(",")[1].trim();
    					
    					Quake quake = new Quake(qdate, details, l, magnitude, linkString);
    					addNewQuake(quake);
    				}
    			}	
    		}
    	}
    	catch ( MalformedURLException e ) {
    		Log.v("refreshEarthQuakes()", "MalformedURLException...");
    		e.printStackTrace();
    	}
    	catch ( IOException e ) {
    		Log.v("refreshEarthQuakes()", "IOException...");
    		e.printStackTrace();
    	}
    	catch ( ParserConfigurationException e ) {
    		Log.v("refreshEarthQuakes()", "ParserConfigurationException...");
    		e.printStackTrace();
    	}
    	catch ( SAXException e ) {
    		Log.v("refreshEarthQuakes()", "SAXException...");
    		e.printStackTrace();
    	}
    	finally {
    	
    	}
    }
    
    private void addNewQuake(Quake q) {
		earthquakes.add(q);
		aryadptr.notifyDataSetChanged();
	}
    
    public boolean onCreateOptionsMenu(Menu menu) {
    	super.onCreateOptionsMenu(menu);
    	
    	menu.add(0, MENU_UPDATE, Menu.NONE, R.string.menu_update);
    	menu.add(0, MENU_PREFERENCES, Menu.NONE, R.string.menu_preferences);
    	
    	return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem mi) {
    	super.onOptionsItemSelected(mi);
    	
    	switch ( mi.getItemId() ) {
    	case MENU_UPDATE: {
    		refreshEarthquakes();
    		return true;
    	}
    	case MENU_PREFERENCES: {
    		Intent i = new Intent(this, PreferencesAct.class);
    		startActivityForResult(i, SHOW_PREFERENCES);
    		return true;
    	}
    	}
    	
    	return false;
    }
}