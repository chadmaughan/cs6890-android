package org.vkedco.android.dragdroptouchdemo;

/*************************************************
 * Circle class defines Circle objects drawn
 * by Painter defined in Painter.java.  
 * 
 * Painter draws a 3x3 TicTacToe board on
 * the green background, then it draws
 * 5 red circles and 5 blue crosses.
 *
 * The user can drag and drop circles all
 * over canvas (e.g., place them on the board).
 * 
 * bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************
 */

public class Circle {
	public float mRADIUS = 20;
	public float mX = 30;
	public float mY = 30;
	public float mStartX;
	public float mStartY;
	public float mOffsetX;
	public float mOffsetY;
	
	public Circle(float x, float y, float r) {
		mX = x;
		mY = y;
		mRADIUS = r;
	}
	
	public void setX(float x) { mX = x; }
	
	public void setY(float y) { mY = y; }
	
	public void setInitX(float x) { mStartX = x; }
	
	public void setInitY(float y) { mStartY = y; }
	
	public void setOffsetX(float x) { mOffsetX = x; }
	
	public void setOffsetY(float y) { mOffsetY = y; }
	
	public float getX() { return mX; }
	
	public float getY() { return mY; }
	
	public float getR() { return mRADIUS; }
	
	public float getInitX() { return mStartX; }
	
	public float getInitY() { return mStartY; }
	
	public float getOffsetX() { return mOffsetX; }
	
	public float getOffsetY() { return mOffsetY; }
}



