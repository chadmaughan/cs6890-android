package org.vkedco.android.dragdroptouchdemo;

/*************************************************
 * Board drawn by Painter defined in Painter.java.  
 * Painter draws a 3x3 TicTacToe board on
 * the green background, then it draws
 * 5 red circles and 5 blue crosses.
 *
 * The user can drag and drop circles all
 * over canvas (e.g., place them on the board).
 * 
 * bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************
 */

public class Board {
	
	public int mTLX = 0;
	public int mTLY = 0;
	public int mBRX = 0;
	public int mBRY = 0;
	public int mColWidth = 0;
	public int mRowHeight = 0;

	public Board(int tlx, int tly, int brx, int bry, int rowh, int colw) {
		mTLX = tlx;
		mTLY = tly;
		mBRX = brx;
		mBRY = bry;
		mRowHeight = rowh;
		mColWidth = colw;
	}
}
