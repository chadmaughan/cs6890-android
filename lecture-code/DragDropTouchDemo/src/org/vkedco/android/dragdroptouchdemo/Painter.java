package org.vkedco.android.dragdroptouchdemo;

/*************************************************
 * Painter.java implements the View of the
 * DragDropTouchAct Activity. The activity
 * demonstrates the basics of how to handle
 * touch screen events. This view is declared
 * in /layout/main.xml. 
 * 
 * Painter draws a 3x3 TicTacToe board on
 * the green background, then it draws
 * 5 red circles and 5 blue crosses.
 *
 * The user can drag and drop circles all
 * over canvas (e.g., place them on the board).
 * 
 * bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************
 */

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class Painter extends View {
	private Paint mBackgroundPaint = null;
	private Paint mForeCirclePaint = null;
	private Paint mForeCrossPaint  = null;
	private Paint mForeBoardPaint = null;
	private Board mBoard = null;
	private ArrayList<Circle> mCircles = null;
	private ArrayList<Cross>  mCrosses = null;
	
	public Painter(Context context, AttributeSet atrs) {
		super(context, atrs);

		mBoard  = new Board(50, 100, 260, 310, 70, 70);
		mBackgroundPaint = new Paint();
		mBackgroundPaint.setColor(Color.GREEN);
		
		mForeBoardPaint = new Paint();
		mForeBoardPaint.setColor(Color.BLACK);
		mForeBoardPaint.setAntiAlias(true);
		
		mForeCirclePaint = new Paint();
		mForeCirclePaint.setColor(Color.RED);
		mForeCirclePaint.setAntiAlias(true);
		
		mForeCrossPaint = new Paint();
		mForeCrossPaint.setColor(Color.BLUE);
		mForeCrossPaint.setAntiAlias(true);
		
		createCircles();
		createCrosses();
	}
	
	private void createCircles() {
		mCircles = new ArrayList<Circle>();
		mCircles.add(new Circle(30,  30, 20));
		mCircles.add(new Circle(100, 30, 20));
		mCircles.add(new Circle(170, 30, 20));
		mCircles.add(new Circle(240, 30, 20));
		mCircles.add(new Circle(310, 30, 20));
	}
	
	private void createCrosses() {
		mCrosses = new ArrayList<Cross>();
		mCrosses.add(new Cross(30,  350, 20));
		mCrosses.add(new Cross(100, 350, 20));
		mCrosses.add(new Cross(170, 350, 20));
		mCrosses.add(new Cross(240, 350, 20));
		mCrosses.add(new Cross(310, 350, 20));
	}
	
	// This method redraws the entire canvas
	public void draw(Canvas canvas) {
		int width = canvas.getWidth();
		int height = canvas.getHeight();
		// 1. draw background rectangle that covers the entire
		// canvas
		canvas.drawRect(0, 0, width, height, mBackgroundPaint);
		// 2. draw board on the canvas
		drawBoard(canvas);
		// 3. draw red circles on canvas
		drawCirclesOnCanvas(canvas);
		// 4. draw blue crosses on canvas
		drawCrossesOnCanvas(canvas);
		// 5. force redraw
		invalidate();
	}
	
	public boolean onTouchEvent(MotionEvent event) {
		// 1. get the x and y of MotionEvent
		float x = event.getX();
		float y = event.getY();
		// 2. find circle closest to x and y
		Circle cr  = findCircleClosestToTouchEvent(x, y);
		// 3. find cross closest to x and y
		Cross  cx  = findCrossClosestToTouchEvent(x, y);
		// 4. compute euclid distances to closest circle and
		// closest cross
		float dtcr = euclidDist(cr.getX(), cr.getY(), x, y);
		float dtcx = euclidDist(cx.getMidX(), cx.getMidY(), x, y);
		// 5. if distance to closest circle is smaller
		//    handle the circle; otherwise, handle the cross
		if ( dtcr < dtcx ) {
			handleTouchedCircle(event, cr);
		}
		else {
			handleTouchedCross(event, cx);
		}
		return true;
	}
	
	// draw all Circles in mCricles
	private void drawCirclesOnCanvas(Canvas canvas) {
		for(Circle c : mCircles) {
			canvas.drawCircle(c.getX(), c.getY(), c.getR(), 
					mForeCirclePaint);
		}
	}
	
	private void drawCrossesOnCanvas(Canvas canvas) {
		for(Cross cx : mCrosses) {
			//drawCrossOnCanvas(canvas, c);
			canvas.drawLine(cx.getTLX(), cx.getTLY(), 
					cx.getBRX(), cx.getBRY(), mForeCrossPaint);
			canvas.drawLine(cx.getTLX(), cx.getBRY(),
					cx.getBRX(), cx.getTLY(), mForeCrossPaint);
		}
	}
	
	private static float euclidDist(float x1, float y1, float x2, float y2) {
		return (float) Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
	}
	
	private Circle findCircleClosestToTouchEvent(float x, float y) {
		Circle c = mCircles.get(0);
		float dist = euclidDist(c.getX(), c.getY(), x, y);
		float tempdist = 0;
		for(Circle cr : mCircles) {
			tempdist = euclidDist(cr.getX(), cr.getY(), x, y);
			if ( tempdist < dist ) {
				c = cr;
				dist = tempdist;
			}
		}
		return c;
	}
	
	private Cross findCrossClosestToTouchEvent(float x, float y) {
		Cross c = mCrosses.get(0);
		float dist = euclidDist(c.getMidX(), c.getMidY(), x, y);
		float tempdist = 0;
		for(Cross cx : mCrosses) {
			tempdist = euclidDist(cx.getMidX(), cx.getMidY(), x, y);
			if ( tempdist < dist ) {
				c = cx;
				dist = tempdist;
			}
		}
		return c;
	}

	private void handleTouchedCircle(MotionEvent event, Circle c) {
		int action = event.getAction();
		switch(action) {
		case MotionEvent.ACTION_DOWN:
			c.setInitX(c.getX());
			c.setInitY(c.getY());
			c.setOffsetX(event.getX());
			c.setOffsetY(event.getY());
			break;
		case MotionEvent.ACTION_MOVE:
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:
			c.setX(c.getInitX() + event.getX() - c.getOffsetX());
			c.setY(c.getInitY() + event.getY() - c.getOffsetY());
			break;
		}
	}
	
	private void handleTouchedCross(MotionEvent event, Cross cx) {
		int action = event.getAction();
		switch(action) {
		case MotionEvent.ACTION_DOWN:
			cx.setInitTLX(cx.getTLX());
			cx.setInitTLY(cx.getTLY());
			cx.setOffsetX(event.getX());
			cx.setOffsetY(event.getY());
			break;
		case MotionEvent.ACTION_MOVE:
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:
			cx.setTLX(cx.getInitTLX() + event.getX() - cx.getOffsetX());
			cx.setTLY(cx.getInitTLX() + event.getY() - cx.getOffsetY());
			break;
		}
	}
	
	private void drawBoard(Canvas canvas) {
		// draw borders as a rectangle
		canvas.drawLine(mBoard.mTLX, mBoard.mTLY, 
				mBoard.mBRX, mBoard.mTLY, mForeBoardPaint);
		canvas.drawLine(mBoard.mBRX, mBoard.mTLY,
				mBoard.mBRX, mBoard.mBRY, mForeBoardPaint);
		canvas.drawLine(mBoard.mBRX, mBoard.mBRY,
				mBoard.mTLX, mBoard.mTLY + 3 * mBoard.mRowHeight,
				mForeBoardPaint);
		canvas.drawLine(mBoard.mTLX, mBoard.mTLY + 3 * mBoard.mRowHeight,
				mBoard.mTLX, mBoard.mTLY, mForeBoardPaint);
		
		
		// draw rows
		canvas.drawLine(mBoard.mTLX, mBoard.mTLY + mBoard.mRowHeight,
				mBoard.mBRX, mBoard.mTLY + mBoard.mRowHeight, 
				mForeBoardPaint);
		canvas.drawLine(mBoard.mTLX, mBoard.mTLY + 2 * mBoard.mRowHeight,
				mBoard.mBRX, mBoard.mTLY + 2 * mBoard.mRowHeight, 
				mForeBoardPaint);
		
		// draw columns
		canvas.drawLine(mBoard.mTLX + mBoard.mColWidth, mBoard.mTLY,
				mBoard.mTLX + mBoard.mColWidth, mBoard.mBRY,
				mForeBoardPaint);
		canvas.drawLine(mBoard.mTLX + 2 * mBoard.mColWidth, mBoard.mTLY,
				mBoard.mTLX + 2 * mBoard.mColWidth, mBoard.mBRY,
				mForeBoardPaint);
	}
}
