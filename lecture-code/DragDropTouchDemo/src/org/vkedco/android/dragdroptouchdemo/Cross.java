package org.vkedco.android.dragdroptouchdemo;

/*************************************************
 * Cross class defines Cross objects drawn
 * by Painter defined in Painter.java.  
 * 
 * Painter draws a 3x3 TicTacToe board on
 * the green background, then it draws
 * 5 red circles and 5 blue crosses.
 *
 * The user can drag and drop circles all
 * over canvas (e.g., place them on the board).
 * 
 * bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************
 */

public class Cross {
	public float mSIDE = 20;
	public float mTLX  = 70;
	public float mTLY  = 30;
	public float mStartTLX;
	public float mStartTLY;
	public float mOffsetX;
	public float mOffsetY;
	
	public Cross(float topLeftX, float topLeftY, float side) {
		mTLX  = topLeftX;
		mTLY  = topLeftY;
		mSIDE = side;
	}
	
	public void setInitTLX(float x) { mStartTLX = x; }
	
	public float getInitTLX() { return mStartTLX; }
	
	public void setInitTLY(float y) { mStartTLY = y; }
	
	public float getInitTLY() { return mStartTLY; }
	
	public void setTLX(float x) { mTLX = x; }
	
	public void setTLY(float y) { mTLY = y; }
	
	public void setOffsetX(float x) { mOffsetX = x; }
	
	public float getOffsetX() { return mOffsetX; }
	
	public float getOffsetY() { return mOffsetY; }
	
	public void setOffsetY(float y) { mOffsetY = y; }
	
	public float getBRX() { return mTLX + mSIDE; }
	
	public float getBRY() { return mTLY + mSIDE; }
	
	public float getMidX() { return mTLX + mSIDE/2.0f; }
	
	public float getMidY() { return mTLY + mSIDE/2.0f; }
	
	public float getTLX() { return mTLX; }
	
	public float getTLY() { return mTLY; }
}




