package org.vkedco.android.dragdroptouchdemo;

/*************************************************
 * Main activity DragDropTouchDemo application. 
 * The activity demonstrates the basics of how to 
 * handle touch screen events. This view is declared
 * in /layout/main.xml and implemented in Painter.java 
 * 
 * Painter draws a 3x3 TicTacToe board on
 * the green background, then it draws
 * 5 red circles and 5 blue crosses.
 *
 * The user can drag and drop circles all
 * over canvas (e.g., place them on the board).
 * 
 * bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************
 */

import android.app.Activity;
import android.os.Bundle;

public class DragDropTouchAct extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}