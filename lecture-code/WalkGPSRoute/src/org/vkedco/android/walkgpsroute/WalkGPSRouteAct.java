package org.vkedco.android.walkgpsroute;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

public class WalkGPSRouteAct extends Activity {
	private LocationManager  locMngr;
	private LocationListener locLstn;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        locMngr = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        locLstn = new LocationListener() {

			@Override
			public void onLocationChanged(Location loc) {
				if ( loc != null ) {
					double lat = loc.getLatitude();
					double lng = loc.getLongitude();
					String mssg = "";
					mssg += "[lat: " + lat + ", ";
					mssg += "lng: "  + lng + "]";
					Toast.makeText(getBaseContext(), mssg, Toast.LENGTH_LONG).show();	
				}
			}

			@Override
			public void onProviderDisabled(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderEnabled(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
				// TODO Auto-generated method stub
				
			}
        	
        };
        
        this.locMngr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
        		this.locLstn);
    }
}