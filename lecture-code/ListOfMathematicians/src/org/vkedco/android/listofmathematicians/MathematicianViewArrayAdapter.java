package org.vkedco.android.listofmathematicians;

/*
 *************************************************
 * MathematicianViewAdapter is a class whose instances
 * represent the names of famous mathematicians
 * displayed in a ListView.
 * 
 * bugs, comments to vladimir dot kulyukin at 
 * usu dot edu
 ************************************************* 
 */

import java.util.List;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class MathematicianViewArrayAdapter extends ArrayAdapter<Mathematician> {
	private int mResID;
	private Context mContext;
	
	public MathematicianViewArrayAdapter(Context c, int resID, List<Mathematician> items) {
		super(c, resID, items);
		mResID = resID;
		mContext = c;
	}
	
	// override getView method to return RelativeLayouts
	// to display Mathematician objects in the list view.
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		// mv is mathematician's view
		RelativeLayout mv;
		Mathematician m = getItem(position);
		String fn = m.getFirstName();
		String ln = m.getLastName();
		
		if ( convertView == null ) {
			mv = new RelativeLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
			vi.inflate(mResID, mv, true);
		}
		else {
			mv = (RelativeLayout)convertView;
		}
		
		// inflate the last_name and first_name text views
		// of the relative layout
		((TextView) mv.findViewById(R.id.last_name)).setText(ln + ",");
		((TextView) mv.findViewById(R.id.first_name)).setText(fn);
		
		// inflate the image view of the relative layout
		ImageView imgv = (ImageView) mv.findViewById(R.id.image);
		Resources res = mContext.getResources();
		// set the drawable of the image view to the
		// appropriate image
		if ( m.getLastName().equalsIgnoreCase("Church") ) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.alonzo_church));
		}
		else if ( m.getLastName().equalsIgnoreCase("Hilbert")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.david_hilbert));
		}
		else if ( m.getLastName().equalsIgnoreCase("Cantor")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.georg_cantor));
		}
		else if ( m.getLastName().equalsIgnoreCase("Peano")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.giuseppe_peano));
		}
		else if ( m.getLastName().equalsIgnoreCase("G�del")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.kurt_godel));
		}
		else {
			imgv.setImageDrawable(res.getDrawable(R.drawable.menu_icon));
		}
		
		return mv;
		
	}
}
