package org.vkedco.android.listofmathematicians;

/*
 *************************************************
 * ListOfMathematiciansAct is the main activity
 * class of ListOfMathematicians application.
 * The application demonstrates how to customize
 * views and array adapters.
 * 
 * bugs, comments to vladimir dot kulyukin at 
 * usu dot edu
 ************************************************* 
 */

import java.util.ArrayList;
import android.app.ListActivity;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class ListOfMathematiciansAct extends ListActivity {
	private static String[] mComputabilityPillars = null;
    private static Resources mRes = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // modify the value of view_type integer
        // parameter in /res/values/params.xml to
        // run the appropriate populateListView method.
        mRes = getResources();
        switch ( mRes.getInteger(R.integer.view_type) ) {
        case 1: 
        	populateListViewOne();
        	break;
        case 2:
        	populateListViewTwo();
        	break;
        case 3:
        	populateListViewThree();
        	break;
        }
        
        getListView().setTextFilterEnabled(true);
    }
    
    // use the standard ArrayAdapter<String>
    private void populateListViewOne() {
    	mComputabilityPillars = mRes.getStringArray(R.array.computability_pillars);
        
        getListView().setAdapter(new ArrayAdapter<String>(this,
        		android.R.layout.simple_expandable_list_item_1,
        		mComputabilityPillars));
    }
    
    // 1) customize views through Mathematician,
    // MathematicianView and /res/layout/mathematician_view_1.xml
    // 2) customize ArrayAdapter as ArrayAdapter<Mathematician>
    private void populateListViewTwo() {
    	mComputabilityPillars = mRes.getStringArray(R.array.computability_pillars);
    	ArrayList<Mathematician> mathematicians = 
    		new ArrayList<Mathematician>();
    	for(String name : mComputabilityPillars) {
    		String[] fnln = name.trim().split(" ");
    		mathematicians.add(new Mathematician(fnln[0], fnln[1]));
    	}
    	getListView().setAdapter(new ArrayAdapter<Mathematician>(this,
    			R.layout.mathematician_view_1,
    			mathematicians));
    }
    
    // customize populating Views in ListViews through
    // a custom array adapter MathematicianViewArrayAdapter
    private void populateListViewThree() {
    	mComputabilityPillars = mRes.getStringArray(R.array.computability_pillars);
    	ArrayList<Mathematician> mathematicians = 
    		new ArrayList<Mathematician>();
    	for(String name : mComputabilityPillars) {
    		String[] fnln = name.trim().split(" ");
    		mathematicians.add(new Mathematician(fnln[0], fnln[1]));
    	}
    	MathematicianViewArrayAdapter adptr =
    		new MathematicianViewArrayAdapter(this, 
    				R.layout.mathematician_view_2,
    				mathematicians);
    	getListView().setAdapter(adptr);
    }
}