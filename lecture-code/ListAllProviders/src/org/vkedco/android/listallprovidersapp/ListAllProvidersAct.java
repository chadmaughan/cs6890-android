package org.vkedco.android.listallprovidersapp;

import java.util.List;
import android.app.ListActivity;
import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class ListAllProvidersAct extends ListActivity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);
        
        LocationManager locMngr =
        	(LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locMngr.getAllProviders();
        this.getListView().setAdapter(new
        		ArrayAdapter<String>(this,
        				android.R.layout.simple_list_item_1,
        				providers)); 				
    }
}