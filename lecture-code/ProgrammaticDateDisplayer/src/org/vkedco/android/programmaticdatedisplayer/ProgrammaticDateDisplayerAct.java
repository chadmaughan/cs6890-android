package org.vkedco.android.programmaticdatedisplayer;

/*
 * **********************************************************
 * ProgrammaticDateDisplayerAct.java builds a simple Android GUI
 * that consists of a text view, an edit view, and a button
 * arranged into a horizontal linear layout. When pressed, 
 * the button updates the string in the edit text with the
 * current date.
 * 
 * The GUI and the back end functionality are developed in code, 
 * i.e., programmatically.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 * **********************************************************
 */

import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProgrammaticDateDisplayerAct extends Activity 
	implements OnClickListener
{
	LinearLayout mainLinLayout = null;
	
	TextView dateLabel = null;
	LayoutParams dateLabelLayoutParams = null;
	
	EditText dateMessage = null;
	LayoutParams dateMessageLayoutParams = null;
	
	Button dateUpdateButton = null;
	LayoutParams dateUpdateButtonLayoutParams = null;
	private final static int DATE_UPDATE_BUTTON_ID = 0;
	
	Button clearDateButton = null;
	LayoutParams clearDateButtonLayoutParams = null;
	private final static int CLEAR_DATE_BUTTON_ID = 1;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // 1. Construct the main linear layout
        mainLinLayout = new LinearLayout(this.getApplicationContext());
        mainLinLayout.setOrientation(LinearLayout.VERTICAL);
        
        // 2. construct an object that holds the layout params
        // for the text view (date label) and use it to construct the
        // text view object
        dateLabelLayoutParams = 
        	new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        			ViewGroup.LayoutParams.WRAP_CONTENT);
        
        dateLabel = new TextView(this.getApplicationContext());
        dateLabel.setLayoutParams(dateLabelLayoutParams);
        dateLabel.setTextSize(20);
        dateLabel.setText("Date and Time");
        
        // 3. construct an object that holds the layout parameters
        // for the edit text (date message) and the use it to
        // construct the edit text object.
        dateMessageLayoutParams =
        	new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        			ViewGroup.LayoutParams.WRAP_CONTENT);
        dateMessage = new EditText(this.getApplicationContext());
        dateMessage.setLayoutParams(dateMessageLayoutParams);
        dateMessage.setText("This is where date and time appear");
        dateMessage.setTextSize(20);
        // let's set the date message object to be not focusable.
        // if we don't, the focus will be set on this object instead
        // of on the button.
        dateMessage.setFocusable(false);
        
        // 4. construct an object for holding the button params
        // and use it to construct the button object for updating date
        // and time.
        dateUpdateButtonLayoutParams =
        	new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        			ViewGroup.LayoutParams.WRAP_CONTENT);
        dateUpdateButton = new Button(this.getApplicationContext());
        dateUpdateButton.setLayoutParams(dateUpdateButtonLayoutParams);
        dateUpdateButton.setText("Update Date");
        dateUpdateButton.setId(DATE_UPDATE_BUTTON_ID);
        // 4.1. assign this class, i.e., ProgrammaticDateDisplayerAct, as
        // the implementor of the OnClickListener interface.
        dateUpdateButton.setOnClickListener(this);
        
        // 5. construct an object for holding the clear button's params
        // and use it to construct the button object for
        // clearing the text of the data message edit text.
       clearDateButtonLayoutParams =
        	new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        			ViewGroup.LayoutParams.WRAP_CONTENT);
        clearDateButton = new Button(this.getApplicationContext());
        clearDateButton.setLayoutParams(clearDateButtonLayoutParams);
        clearDateButton.setText("Clear Date");
        clearDateButton.setId(CLEAR_DATE_BUTTON_ID);
        clearDateButton.setOnClickListener(this);
        
        // 6. add all the child views to the mainLinLayout
        mainLinLayout.addView(dateLabel);
        mainLinLayout.addView(dateMessage);
        mainLinLayout.addView(dateUpdateButton);
        mainLinLayout.addView(clearDateButton);

        // 7. set the application's screen to mainLinLayout
        setContentView(mainLinLayout);
    }

	@Override
	public void onClick(View view) {
		Button btn = (Button) view;
		int id = btn.getId();
		if ( id == this.DATE_UPDATE_BUTTON_ID ) 
			dateMessage.setText(new Date().toString());
		else if ( id == this.CLEAR_DATE_BUTTON_ID )
			dateMessage.setText("");
		
	}
}