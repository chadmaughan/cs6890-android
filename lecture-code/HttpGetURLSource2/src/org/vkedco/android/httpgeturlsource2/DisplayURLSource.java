package org.vkedco.android.httpgeturlsource2;

/*
 *************************************************************
 * In this case application, the HttpURLSource2Act activity
 * retrieves the source of a user-supplied URL and saves
 * it in a String member variable (mURLSource) of HttpGetApp.
 * DisplayURLSource, this activity, retrieves the url source 
 * from the member variable and displays it in an EditText.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************************
 */

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

public class DisplayURLSource extends Activity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.urlsource);
        HttpGetApp app = (HttpGetApp)this.getApplication();
        EditText urlSource = (EditText)findViewById(R.id.url_source);
        urlSource.setText(app.getUrlSource());
	}
}
