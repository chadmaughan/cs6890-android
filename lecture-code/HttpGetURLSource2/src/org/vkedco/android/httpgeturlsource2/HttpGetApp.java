package org.vkedco.android.httpgeturlsource2;

/*
 *************************************************************
 * HttpGetApp extends Application and illustrates how 
 * an Application singleton can be used to share
 * information between activities in the same application.
 * In this case application, the HttpURLSource2Act activity
 * retrieves the source of a user-supplied URL and saves
 * it in a String member variable (mURLSource) of HttpGetApp.
 * The second activity of this application, DisplayURLSource, 
 * retrieves the url source from the member variable and 
 * displays it in an EditText.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************************
 */

import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.app.Application;
import android.util.Log;

public class HttpGetApp extends Application {
	private static final String LOGPROMPT = "HttpGetApp";
	private HttpClient mHttpClient = null;;
	private String     mUrlSource = "";
	
	public void onCreate() {
		super.onCreate();
		mHttpClient = makeThreadSafeClient();
	}
	
	public void onLowMemory() {
		super.onLowMemory();
		shutdownHttpClient();
	}
	
	public HttpClient getHttpClient() {
		return mHttpClient;
	}
	
	// this is to check that we can get the stuff from the
	// app singleton object.
	public String getUrlSource() {
		return mUrlSource;
	}
	
	public void setURLSource(String urlSource) {
		this.mUrlSource = urlSource;
	}
	
	private HttpClient makeThreadSafeClient() {
		HttpParams httpParams    = makeHttpParams();
		SchemeRegistry schemeReg = makeSchemeRegistry();
		ClientConnectionManager connMan = new
			ThreadSafeClientConnManager(httpParams, schemeReg);
		DefaultHttpClient clnt = new DefaultHttpClient(connMan, httpParams);
		Log.v(LOGPROMPT, "HttpClient created");
		return clnt;
	}
	
	private HttpParams makeHttpParams() {
		HttpParams httpParams = new BasicHttpParams();
		HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(httpParams, HTTP.UTF_8);
		HttpProtocolParams.setUseExpectContinue(httpParams, true);
		return httpParams;
	}
	
	private SchemeRegistry makeSchemeRegistry() {
		SchemeRegistry schemeReg = new SchemeRegistry();
		schemeReg.register(new Scheme("http",
				PlainSocketFactory.getSocketFactory(), 80));
		schemeReg.register(new Scheme("https",
				SSLSocketFactory.getSocketFactory(), 443));
		return schemeReg;
	}
	
	private void shutdownHttpClient() {
		if ( mHttpClient != null ) {
			if ( mHttpClient.getConnectionManager() != null ) {
				mHttpClient.getConnectionManager().shutdown();
			}
		}
	}
}
