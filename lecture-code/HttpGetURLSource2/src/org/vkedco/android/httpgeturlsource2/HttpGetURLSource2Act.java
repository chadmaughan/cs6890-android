package org.vkedco.android.httpgeturlsource2;

/*
 *************************************************************
 * The main activity of of an application that retrieves
 * the URL source. The application illustrates how
 * Application class can be extended and used to share
 * information between activities in the same application.
 * In this case application, HttpURLSource2Act (this activity)
 * retrieves the source of a user-supplied URL and saves
 * it in a String member variable of HttpGetApp, which
 * extends Application. The second activity of this application,
 * DisplayURLSource, retrieves the url source from the
 * member variable and displays it in an EditText.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 *************************************************************
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HttpGetURLSource2Act extends Activity {
	private static Button mSubmitBtn;
    private static Button mClearBtn;
    private static EditText mUrlEntry;
    private HttpClient mHttpClient;
    private static final String LOGPROMPT = "HttpGetURLSource2Act";
    private int mNumRetries = 1;
    private HttpGetApp mApp = null;
    private Activity mThisAct = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mApp = (HttpGetApp)this.getApplication();
        mHttpClient = mApp.getHttpClient();
        mSubmitBtn  = (Button)findViewById(R.id.submitBtn);
        mClearBtn   = (Button)findViewById(R.id.clearBtn);
        mUrlEntry   = (EditText)findViewById(R.id.url_entry);
        mNumRetries = getResources().getInteger(R.integer.num_retries);
        mThisAct = this;
        
        mSubmitBtn.setOnClickListener(
       		 new OnClickListener() {
					@Override
					public void onClick(View v) {
						URI uri = null;
						try {
							uri = URI.create(mUrlEntry
									.getText()
									.toString()
									.toString());
							Log.v("HTTPGET", uri.toString());
						}
						catch ( Exception e ) {
							Toast.makeText(getApplicationContext(),
									"invalid url", Toast.LENGTH_LONG).show();
						}
						finally {
							if ( uri != null ) {
								try {
									Log.v(LOGPROMPT, "check 0");
									mApp.setURLSource(doHttpGetWithRetries(uri, 
														mNumRetries));
									Intent i = new Intent(mThisAct, DisplayURLSource.class);
									mThisAct.startActivity(i);
									Log.v(LOGPROMPT, "check 1");
									
								}
								catch ( Exception e ) {
									e.printStackTrace();
								}
							}
						}
						
					} 
       		 });
        
        mClearBtn.setOnClickListener(
       		 new OnClickListener() {

					@Override
					public void onClick(View v) {
						mUrlEntry.setText("");
						
					} 
       		 });
	}
    
    
    private String doHttpGetWithRetries(URI uri, int num_retries) {
    	int count = 0;
    	while ( count < num_retries ) {
    		try {
    			String res = tryHttpGetOnce(uri);
    			return res;
    		}
    		catch ( Exception ex ) {
    			Log.v(LOGPROMPT, ex.toString());
    			if ( count < num_retries ) {
    				count++;
    			}
    		}
    	}
    	return "source retrieval failure";
    }
    
    private String tryHttpGetOnce(URI uri) throws Exception {
    	BufferedReader in = null;
    	try {

    		HttpGet req = new HttpGet();
    		req.setURI(uri);
    		HttpResponse res = mHttpClient.execute(req);
    		in = new BufferedReader(new InputStreamReader(res
    							.getEntity()
    							.getContent()));
    		StringBuffer sb = new StringBuffer("");
    		String line = "";
    		String SP = System.getProperty("line.separator");
    		while ( (line = in.readLine()) != null ) {
    			sb.append(line + SP);
    		}
    		in.close();
    		
    		return sb.toString();
    	}
    	finally {
    		if ( in != null ) {
    			try {
    				in.close();
    			}
    			catch ( IOException e ) {
    				e.printStackTrace();
    			}
    		}
    	}
    }
}