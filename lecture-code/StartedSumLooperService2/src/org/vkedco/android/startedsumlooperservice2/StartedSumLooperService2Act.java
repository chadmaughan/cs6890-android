package org.vkedco.android.startedsumlooperservice2;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StartedSumLooperService2Act extends Activity 
	implements OnClickListener
{
	private Button    mCatSumBtn;
	private Button    mFibSumBtn;
	private Button    mClearBtn;
	private Button    mCatSumLookupBtn;
	private Button    mFibSumLookupBtn;
	private Button    mCrashBtn;
	private EditText  mNEdtTxt;
	private Resources mRes;
	// This is the application object with the shared memory
	// hash table where StartedSumLooperService2 service
	// places the computed values.
	private StartedSumLooperServiceApp mApp;
	private String    mCatKey;
	private String    mFibKey;
	private String    mNKey;
	private Toast     mToast;
	
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		mCatSumBtn = (Button) findViewById(R.id.btnCatalanSum);
		mCatSumBtn.setOnClickListener(this);
		
		mFibSumBtn = (Button) findViewById(R.id.btnFibSum);
		mFibSumBtn.setOnClickListener(this);
		
		mClearBtn = (Button) findViewById(R.id.btnClear);
		mClearBtn.setOnClickListener(this);
		
		mCatSumLookupBtn = (Button) findViewById(R.id.btnCatalanLookup);
		mCatSumLookupBtn.setOnClickListener(this);
		
		mFibSumLookupBtn = (Button) findViewById(R.id.btnFiboLookup);
		mFibSumLookupBtn.setOnClickListener(this);
		
		mCrashBtn = (Button) findViewById(R.id.btnCrash);
		mCrashBtn.setOnClickListener(this);
		
		mNEdtTxt = (EditText) findViewById(R.id.edtxtN);
		mApp = (StartedSumLooperServiceApp) this.getApplication();
		mRes = getResources();
		
		mCatKey = mRes.getString(R.string.cat_key);
		mFibKey = mRes.getString(R.string.fib_key);
		mNKey   = mRes.getString(R.string.n_key);
		
		mToast  = Toast.makeText(getApplicationContext(), "",
				Toast.LENGTH_LONG);
	}
    
    @Override
	public void onDestroy() {
		super.onDestroy();
		// stop the service on destruction
		stopService(new Intent(getApplicationContext(),
				StartedSumLooperService2.class));
	}
	
	@Override
	public void onClick(View v) {
		switch ( v.getId() ) {
		
		case R.id.btnCatalanSum:
			// 1. Create an intent to run StartedSumLooperService2
			Intent i1 = 
				new Intent(getApplicationContext(), 
						StartedSumLooperService2.class);
			// 2. Parse the long value entered by the user
			long n1 = Long.parseLong(mNEdtTxt.getText().toString());
			// 3. Place key-value pairs (cat_key, true) and (n_key, n1)
			// into the intent's bundle of extras
			i1.putExtra(mCatKey, true);
			i1.putExtra(mNKey, n1);
			// 4. start the service on the created intent
			startService(i1);
			break;
			
		case R.id.btnCatalanLookup:
			// 1. Generate a lookup key for the catalan sum
			String catkey = mApp.genKey(mCatKey,
					Long.parseLong(mNEdtTxt.getText().toString()));
			// 2. Look up the computed value
			long catv = mApp.getComputedValue(catkey);
			// 3. Toast the lookup result
			if ( catv == -1 ) {
				mToast.setText("Value unavailable");
				mToast.show();
			}
			else {
				mToast.setText(""+catv);
				mToast.show();
				mApp.removeKey(catkey);
			}
			break;
			
		case R.id.btnFibSum:
			// Same logic as for btnCatSum above but
			// the extras placed into the bundle require
			// the service to compute the fibonacci sum.
			Intent i2 = 
				new Intent(getApplicationContext(), 
						StartedSumLooperService2.class);
			long n2 = Long.parseLong(mNEdtTxt.getText().toString().toString());
			i2.putExtra(mRes.getString(R.string.fib_key), true);
			i2.putExtra(mRes.getString(R.string.n_key), n2);
			startService(i2);
			break;
			
		case R.id.btnFiboLookup:
			// Lookup the result of the fibonacci sum computation
			// and toast the result.
			String fibkey = mApp.genKey(mFibKey,
					Long.parseLong(mNEdtTxt.getText().toString()));
			long fibv = mApp.getComputedValue(fibkey);
			if ( fibv == -1 ) {
				mToast.setText("Value unavailable");
				mToast.show();
			}
			else {
				mToast.setText(""+fibv);
				mToast.show();
				mApp.removeKey(fibkey);
			}
			break;
			
		case R.id.btnClear:
			mNEdtTxt.setText("");
			break;
			
		case R.id.btnCrash:
			// Kill the current process
			Process.killProcess(Process.myPid());
			break;
		}
	}
}