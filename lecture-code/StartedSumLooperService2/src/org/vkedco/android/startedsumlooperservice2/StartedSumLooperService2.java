package org.vkedco.android.startedsumlooperservice2;

import android.app.Service;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

/*
 ******************************************************************
 * Class: StartedSumLooperService2.java
 * Description: Same as StartedSumLooperService.java except that
 * computed values of catalan and fibonacci sums are stored in
 * the hash table inside StartedSumLooperServiceApp. This example
 * shows how an activity and a background service the activity
 * starts can share information via the Application object. This
 * is a reasonable alternative to posting notification messages
 * on the status bar whose icons may stay there for a while.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 ******************************************************************
 */

public class StartedSumLooperService2 extends Service {
	private Resources mRes;
	private volatile Looper mServiceLooper;
	private volatile ServiceHandler mServiceHandler;
	private final long mSleepInterval = 1000L;
	private StartedSumLooperServiceApp mApp;
	
	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}
		
		public void handleMessage(Message msg) {
			Bundle args = (Bundle)msg.obj;
			String catSumKey = mRes.getString(R.string.cat_key);
			String fibSumKey = mRes.getString(R.string.fib_key);
			String nKey = mRes.getString(R.string.n_key);
			
			if ( (args.containsKey(catSumKey) || args.containsKey(fibSumKey))
					&&
				  args.containsKey(nKey) )  {
				long n = args.getLong(nKey);
				if ( args.containsKey(catSumKey) ) {
					synchronized ( this ) {
						saveComputedValue(catSumKey, n, catalanSum(n));
					}
				}
				else {
					synchronized ( this ) {
						saveComputedValue(fibSumKey, n, fiboSum(n));
					}
				}
			}
			
			synchronized ( this ) {
				try {
					wait(1000L);
				}
				catch ( Exception ex ) {
					
				}
			}
		}
	}
	
	@Override
	public void onCreate() {
		mApp = (StartedSumLooperServiceApp) this.getApplication();
		HandlerThread hthr =
			new HandlerThread("StartedSumLooperService2Thread",
					Process.THREAD_PRIORITY_BACKGROUND);
		hthr.start();
		mServiceLooper = hthr.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
		mRes = getResources();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		msg.arg2 = flags;
		msg.obj  = intent.getExtras();
		mServiceHandler.sendMessage(msg);
		return START_NOT_STICKY;
	}
	
	@Override
	public void onDestroy() {
		mServiceLooper.quit();
		this.stopSelf();	
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	// compID="CatalanSum" or compID="FibonacciSum"
	private void saveComputedValue(String compID, long n, long value) {
		mApp.saveComputedValue(compID, n, value);
	}
	
	private long catalanSum(long n) {
		if ( n == 0L ) {
			return 1L;
		}
		else {
			long prevC = 1L;
			long sum   = 1L;
			for(long i = 1; i <= n; i++) {
				prevC *= (4 * i - 2);
				prevC /= (i + 1);
				sum += prevC;
				synchronized ( this ) {
					try {
						Thread.sleep(mSleepInterval);
					}
					catch ( Exception ex ) {
						
					}
				}
			}
			return sum;
		}
	}
	
	private long fiboSum(long n) {
		if ( n == 0L ) {
			return 1L;
		}
		else if ( n == 1L ) {
			return 2L;
		}
		else {
			long prev1 = 1L;
			long prev2 = 1L;
			long sum = 2L;
			for(int i = 2; i <= n; i++) {
				long next = prev1 + prev2;
				sum += next;
				prev1 = prev2;
				prev2 = next;
				synchronized ( this ) {
					try {
						Thread.sleep(mSleepInterval);
					}
					catch ( Exception ex ) {
						
					}
				}
			}
			return sum;
		}
	}

}
