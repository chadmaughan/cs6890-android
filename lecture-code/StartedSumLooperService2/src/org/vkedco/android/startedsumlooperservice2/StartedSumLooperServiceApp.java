package org.vkedco.android.startedsumlooperservice2;

/*
 ******************************************************************
 * StartedSumLooperServiceApp extends Application. It defines
 * a hashtable where StartedSumLooperService2 stores the
 * computed values of catalan and fibonacci sums and where 
 * StartedSumLooperService2Act looks up those values. This example 
 * shows how an activity and a background service the activity 
 * starts can share information via the Application object. 
 * This is a reasonable alternative to posting notification 
 * messages on the status bar whose icons may stay there for 
 * a while.
 * 
 * Bugs, comments to vladimir dot kulyukin at gmail dot com
 ******************************************************************
 */

import java.util.Hashtable;

import android.app.Application;

// Extend Application with StartedSumLooperServiceApp
public class StartedSumLooperServiceApp extends Application {
	// This is the shared memory data structure
	// where the looper service places its results.
	private Hashtable<String, Long> mTable;
	
	public StartedSumLooperServiceApp() {
		mTable = new Hashtable<String, Long>();
	}
	
	// Lookup the computed value in the shared memory
	// hash table
	public long getComputedValue(String key) {
		Long v = (Long) mTable.get(key);
		if ( v != null )
			return v.longValue();
		else
			return -1;
	}
	
	// remove the key-value pair from the hash table.
	public void removeKey(String key) {
		if ( mTable.containsKey(key) )
			mTable.remove(key);
	}
	
	// save the computed value in the hash table.
	// compID="CatalanSum" or compID="FibonacciSum"
	// the key returned by genKey is "CatalanSum_10" if n_param = 10.
	public void saveComputedValue(String compID, long n_param, long val) {
		mTable.put(genKey(compID, n_param), new Long(val));
	}
	
	public String genKey(String compID, long n_param) {
		return compID +  "_" + n_param;
	}

}
