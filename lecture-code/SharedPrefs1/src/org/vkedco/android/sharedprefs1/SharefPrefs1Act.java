package org.vkedco.android.sharedprefs1;

/*
 ***************************************************************
 * SharedPrefs1Act is the main activity of the SharedPrefs1 
 * application that demonstrates how to use SharedPreferences.
 * The main screen of the application allows the user to
 * enter two values for two preferences and save them (savePreferences). 
 * When the application becomes active again, the saved preferences
 * are loaded (loadPreferences). 
 * 
 * Errors, comments to vladimir dot kulyukin at gmail dot com
 ***************************************************************
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SharefPrefs1Act extends Activity {
	
	private SharedPreferences mPrefs = null;
	private TextView mPref1 = null;
	private EditText mPref1Val = null;
	private TextView mPref2 = null;
	private EditText mPref2Val = null;
	private Button mSavePrefsButton = null;
	private Button mClearButton = null;
	private Button mQuitButton = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Context ctxt = getApplicationContext();
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
        mPref1 = (TextView)findViewById(R.id.pref1);
    	mPref1Val = (EditText)findViewById(R.id.pref1Value);
    	mPref2 = (TextView)findViewById(R.id.pref2);
    	mPref2Val = (EditText)findViewById(R.id.pref2Value);
    	mSavePrefsButton = (Button)findViewById(R.id.savePrefsButton);
    	mClearButton = (Button)findViewById(R.id.clearButton);
    	mQuitButton = (Button)findViewById(R.id.quitButton);
    	
    	loadPrefs();
    	
    	mSavePrefsButton.setOnClickListener(
    				new OnClickListener() {
						@Override
						public void onClick(View v) {
							savePrefs();
						}
    				});
    	
    	mClearButton.setOnClickListener(
    				new OnClickListener() {

						@Override
						public void onClick(View v) {
							mPref1Val.setText("");
							mPref2Val.setText("");
						}
    					
    				}
    			);
    	mQuitButton.setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						finish();
					}
					
				}
			);
    }
    
    private void savePrefs() {
    	Editor prefEditor = mPrefs.edit();
    	prefEditor.putString(mPref1.getText().toString(), mPref1Val.getText().toString());
    	prefEditor.putString(mPref2.getText().toString(), mPref2Val.getText().toString());
    	prefEditor.commit();
    }
    
    private void loadPrefs() {
    	mPref1Val.setText(mPrefs.getString(mPref1.getText().toString(), ""));
    	mPref2Val.setText(mPrefs.getString(mPref2.getText().toString(), ""));
    }
}