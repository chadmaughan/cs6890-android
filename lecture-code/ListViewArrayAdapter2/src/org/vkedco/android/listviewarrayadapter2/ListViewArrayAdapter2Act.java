package org.vkedco.android.listviewarrayadapter2;

/*
 * ********************************************************************
 * This file is an example of how to define a ListActivity
 * that 
 * 1) has a custom child view design (/res/layout/list_item_specs.xml);
 * 2) fills in its array from an XML file of string resources
 * (/res/values/rumi_works.xml) by using a Resources object; 
 * 3) illustrates how to make a Toast object from inside an 
 * OnItemClickListener constructor when it is necessary to 
 * call getApplicationContext();
 * 4) enables run-time word completion in its list view
 * 
 * Bugs, comments, questions to vladimir dot kulyukin at usu dot edu
 * ********************************************************************
 */

import android.app.ListActivity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ListViewArrayAdapter2Act extends ListActivity {
	
	// We define this array which we will fill in in onCreate() below
	// from the rumi_works.xml.
	private static String[] mRumiWorks = null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);
        // 1. Get a Resources object of this application
        Resources res = getResources();
        // 2. Allocate the array
        mRumiWorks = new String[5];
        // 3. Use the resources object to fill in the array
        // with Strings.
        mRumiWorks[0] = res.getString(R.string.spiritual_couplets);
        mRumiWorks[1] = res.getString(R.string.shams_of_tabriz);
        mRumiWorks[2] = res.getString(R.string.in_it);
        mRumiWorks[3] = res.getString(R.string.seven_sessions);
        mRumiWorks[4] = res.getString(R.string.letters);
        // 4. Set the list adapter of this ListActivity directly. Note that
        // we are using a customized design of the each text view in
        // the list view defined in list_item_specs.xml.
        this.setListAdapter(new ArrayAdapter<String>(this, 
        		R.layout.list_item_specs, 
        		mRumiWorks));
 
        // 5. Enable run-time word completion
        this.getListView().setTextFilterEnabled(true);
        
        // 6. Create an OnItemClickListener dynamically and bind it
        // to the list view of this activity.
        this.getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View child, int position, long id) {
				String msg = "";
				msg += "PARENT = " + parent.toString() + "\n";
				msg += "VIEW = " + child.toString() + "\n";
				msg += "VIEW'S TEXT = " + ((TextView)child).getText().toString() + "\n";
				msg += "POSITION = " + Integer.toString(position) + "\n";
				msg += "ID = " + Long.toString(id);
				// 7. This is a subtle point: we cannot specify this as the
				// first argument to makeText, because we are inside OnItemClickListener,
				// not inside the ListActivity where we were in ListViewArrayAdapter1Act.
				// Thus, we have to getApplicationContext() explicitly.
				Toast t = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
				t.show();
			}});
    }
}