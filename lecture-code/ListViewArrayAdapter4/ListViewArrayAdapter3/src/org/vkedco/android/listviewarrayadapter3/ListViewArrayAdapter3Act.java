package org.vkedco.android.listviewarrayadapter3;

/*
 * ****************************************************************
 * ListViewArrayAdapter3Act illustrates how one can:
 * 
 * 1) fill in an array from an XML resource that defines 
 * two arrays (/res/values/rumi_works.xml); 
 * 2) custom design each text view in a list view
 * (/res/layout/list_item_specs.xml)
 * 3) associate an OnItemClickListener with a ListView;
 * 4) display a Toast in onItemClick with the English transliteration
 * of the Persian translation given of the 
 * English title clicked upon in the list view;
 * 5) enable run-time word completion in its list view. 
 * 
 * Bugs, comments to vladimir dot kulyukin at usu dot edu
 * *****************************************************************
 */

import android.app.ListActivity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ListViewArrayAdapter3Act extends ListActivity {
	
	private static String[] mRumiWorksEnglish = null;
	private static String[] mRumiWorksPersian = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main);
        // 1. Get a Resources object
        Resources res = getResources();
        // 2. Inflate two arrays from an XML resource defined in
        // res/values/rumi_works.xml
        mRumiWorksEnglish = res.getStringArray(R.array.rumi_works_english);
        mRumiWorksPersian = res.getStringArray(R.array.rumi_works_persian);
        
        // 3. Associate an array adapter with this ListAcivity
        // that binds mRumiWorks with this ListView. It also
        // uses list_item_specs.xml to customize the appearance
        // of each text view in the list view.
        setListAdapter(new ArrayAdapter<String>(this, 
        		R.layout.list_item_specs, 
        		mRumiWorksEnglish));
        // 4. Enable this list view for word completion
        this.getListView().setTextFilterEnabled(true);
        
        // 5. Set the OnItemClickListener of the list view.
        this.getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View child, int position, long id) {
				// 6. This is a subtle point: we cannot specify this as the
				// first argument to makeText, because we are inside OnItemClickListener,
				// not inside the ListActivity where we were in ListViewArrayAdapter1Act.
				// Thus, we have to getApplicationContext() explicitly.
				Toast t = Toast.makeText(getApplicationContext(), 
						mRumiWorksPersian[position], 
						Toast.LENGTH_LONG);
				t.show();
			}});
    }
}