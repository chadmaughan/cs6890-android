package org.vkedco.android.listviewarrayadapter4;

/*
 * ****************************************************************
 * ListViewArrayAdapter4Act illustrates how one can:
 * 
 * 1) construct an ArrayAdapter straight from an XML resource 
 * (/res/values/rumi_works.xml);
 * 2) Fill in an array from an XML resource (/res/values/rumi_works.xml); 
 * 3) custom design each text view in a list view
 * (/res/layout/list_item_specs.xml);
 * 4) associate an OnItemClickListener with a ListView;
 * 5) display a Toast in onItemClick with the English transliteration 
 * of the Persian translation of the 
 * English title clicked upon in the list view;
 * 6) enable run-time word completion in its list view.
 * 
 * Bugs, comments to vladimir dot kulyukin at usu dot edu
 * *****************************************************************
 */

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ListViewArrayAdapter4Act extends ListActivity {

	private static String[] mRumiWorksPersian = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 1. Create an array adapter directly from an XML resource that
		// defines a string array (/res/values/rumi_works.xml). 
		// The adapter also uses /res/layout/list_item_specs.xml to customize the appearance
		// of each child text view in the parent list view 
		// container. CharSequence is an interface to an ordered 
		// set of characters. An object that implements CharSequence 
		// can be easily converted to Strings with toString().
		ArrayAdapter<CharSequence> adptr
			= ArrayAdapter.createFromResource(this, 
					R.array.rumi_works_english,
					R.layout.list_item_specs);
		this.setListAdapter(adptr);
		
		// 2. Fetch the Persian translations of Rumi's works
		// from the second array defined in the XML resource
		mRumiWorksPersian = getResources().getStringArray(R.array.rumi_works_persian);
		
		// 3. Enable this list view for word completion
		this.getListView().setTextFilterEnabled(true);

		// 4. Set the OnItemClickListener of the list view.
		this.getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View child,
					int position, long id) {
				// 5. This is a subtle point because we cannot specify this as
				// the first argument to makeText, because we are inside
				// OnItemClickListener,
				// not inside the ListActivity where we were in
				// ListViewArrayAdapter1Act.
				// Thus, we have to getApplicationContext() explicitly.
				Toast t = Toast.makeText(getApplicationContext(), 
						mRumiWorksPersian[position],
						Toast.LENGTH_LONG);
				t.show();
			}
		});
	}
}