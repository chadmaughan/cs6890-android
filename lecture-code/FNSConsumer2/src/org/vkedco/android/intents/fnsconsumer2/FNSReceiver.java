package org.vkedco.android.intents.fnsconsumer2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class FNSReceiver extends BroadcastReceiver {

	/*
	 * ====================================================
	 * This is a broadcast receiver that is registered in
	 * AndroidManifest.xml of this application.
	 *
	 * <receiver android:name=".FNSReceiver">
	 *	<intent-filter>
	 *		<action android:name="org.vkedco.android.intents.FIB_SUM_COMPUTED" />
	 *	</intent-filter>
	 * </receiver>
	 *
	 * comments, bugs to vladimir dot kulyukin at gmail dot com
	 * =====================================================  
	*/
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle b = intent.getExtras();
		if ( b.containsKey("FIB_SUM") ) {
			Toast t = Toast.makeText(context, 
					"SOURCE = " + b.getString("SOURCE") + "; " +
					"FIB_SUM = " + Long.toString(b.getLong("FIB_SUM")), 
								Toast.LENGTH_LONG);
			t.show();
		}
	}
}
