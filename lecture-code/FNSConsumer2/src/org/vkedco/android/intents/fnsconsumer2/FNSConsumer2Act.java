package org.vkedco.android.intents.fnsconsumer2;

/*
 * *********************************************************
 * FNSConsumer2Act.java is part a simple broadcast system.
 * This is the application that needs to compute the sum of 
 * the first n Fibonacci numbers. There are two other 
 * applications, FNSProviderIter2 and FNSProviderRec2, that 
 * compute the sum of the first n Fibonacci numbers.
 * 
 * FNSConsumer2 broadcasts its request to compute the sum of 
 * the first n Fibonacci numbers � it is a broadcaster.
 * The second and third applications, FNSProviderIter2 and
 * FNSProviderRec1, register themselves as broadcast receivers 
 * that respond to the specific type of request to compute 
 * the sum of the first n Fibonacci numbers.
 * 
 * Comments, bugs to vladimir dot kulyukin at gmail dot com
 * *****************************************************************
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class FNSConsumer2Act extends Activity {
	private EditText mFibSumN;
	private Button mComputeFibSumButton;
	private EditText mFibSumC;
	
	// 1. The first step in broadcasting an intent is
	// the specification of the action string. By convention,
	// the Java package naming convention is used.
	public static final String COMPUTE_FIB_SUM =
		"org.vkedco.android.intents.COMPUTE_FIB_SUM";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mFibSumN = (EditText)findViewById(R.id.fib_sum_n);
        mComputeFibSumButton = (Button)findViewById(R.id.fib_sum_button);
        mFibSumC = (EditText)findViewById(R.id.fib_sum_criterion);
        
        // 2. This is the OnClickListener that broadcasts 
        // the intent with the value of n and the criterion (beauty,
        // efficiency, or both).
        mComputeFibSumButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 2.1 Create an intent with the action string
				Intent computeFibSumIntent = new Intent(COMPUTE_FIB_SUM);
				// 2.2 Create a Bundle. This bundle specifies two pieces
				// of information: N --> 5; CRITERION --> {"beauty",
				// "efficiency", "both"}
				Bundle computeFibSumBundle = new Bundle();
				// 2.3 Get the value of N supplied by the user
				String input = mFibSumN.getText().toString();
				// 2.4 Parse it into a long
				long n = Long.parseLong(input);
				// 2.5 put N --> n into the Bundle
				computeFibSumBundle.putLong("N", n);
				// 2.6 put CRITERION --> user supplied criterion
				computeFibSumBundle.putString("CRITERION", mFibSumC.getText().toString());
				// 2.7 add the Bundle to the intent
				computeFibSumIntent.putExtras(computeFibSumBundle);
				// 2.8 send the broadcast to Android
				sendBroadcast(computeFibSumIntent);
			}
        	
        });
        
        
    }
}