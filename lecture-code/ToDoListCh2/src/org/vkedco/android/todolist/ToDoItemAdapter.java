package org.vkedco.android.todolist;

/*
* ToDoItemAdapter is part of the Todo List application 
* described in Ch. 2, 4, and 5 of "Professional Android 2 
* Application Development" by Rito Meier.
*/

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ToDoItemAdapter extends ArrayAdapter<ToDoItem> {
	private int mResID;
	
	public ToDoItemAdapter(Context c, int resID, List<ToDoItem> items) {
		super(c, resID, items);
		mResID = resID;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout todoView;
		
		ToDoItem item = getItem(position);
		String taskDescrip = item.getTask();
		Date taskDate = item.getDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
		String dateStr = sdf.format(taskDate);
		
		if ( convertView == null ) {
			todoView = new RelativeLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
			vi.inflate(mResID, todoView, true);
		}
		else {
			todoView = (RelativeLayout)convertView;
		}
		
		TextView dateView = (TextView)todoView.findViewById(R.id.taskDate);
		TextView taskView = (TextView)todoView.findViewById(R.id.taskDescrip);
		dateView.setText(dateStr);
		taskView.setText(taskDescrip);
		return todoView;
		
	}
}
