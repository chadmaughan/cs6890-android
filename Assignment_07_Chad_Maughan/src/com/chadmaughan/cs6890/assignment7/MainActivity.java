package com.chadmaughan.cs6890.assignment7;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.chadmaughan.cs6890.assignment7.database.MathematicianDatabaseAdapter;

public class MainActivity extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // open the database (creates the database)
		MathematicianDatabaseAdapter database = new MathematicianDatabaseAdapter(this);
		database.open();
		
		// clear out any previously downloaded records
		database.empty();
		
		// be responsible, close it up
		database.close();
    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0,1,0,"Download");
		return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch(item.getItemId()) {
			
			// download
			case 1:
				
				try {
		        	DefaultHttpClient hc = new DefaultHttpClient();
			        
			        // load the xml file from the internet
		        	// 	make sure and add the android.permission.INTERNET permission to the AndroidManifest.xml
					Toast.makeText(getApplicationContext(), "Downloading Mathematicians", Toast.LENGTH_SHORT).show();
	
			        HttpGet get = new HttpGet("http://dl.dropbox.com/u/16201259/teaching/ANDROID/mathematicians.xml");
			        HttpResponse httpResponse = hc.execute(get);
			        HttpEntity httpEntity = httpResponse.getEntity();
			        String xml = EntityUtils.toString(httpEntity);
			        
			        Log.v("xml", xml);
	
			        // parse it
			        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			        DocumentBuilder db = dbf.newDocumentBuilder();
					InputSource is = new InputSource();
					is.setCharacterStream(new StringReader(xml));
					
					// build the database
					MathematicianDatabaseAdapter database = new MathematicianDatabaseAdapter(this);
					database.open();
					database.empty();
					
					Document doc = db.parse(is);
					NodeList nodes = doc.getElementsByTagName("mathematician");
					
					// populate the database
					for(int i = 0; i < nodes.getLength(); i++) {
						Node node = nodes.item(i);
	
						NodeList children = node.getChildNodes();
						String firstName = children.item(1).getTextContent();
						String lastName = children.item(3).getTextContent();
						String research = children.item(5).getTextContent();
						String wiki = children.item(7).getTextContent();
	
						database.create(firstName, lastName, research, wiki);
					}
					Intent displayIntent = new Intent(MainActivity.this, DisplayActivity.class);
					startActivity(displayIntent);
				}
				catch(Exception e) {
					
				}
				
				break;
				
			default:
				break;
		}

	   return true;
	}

}