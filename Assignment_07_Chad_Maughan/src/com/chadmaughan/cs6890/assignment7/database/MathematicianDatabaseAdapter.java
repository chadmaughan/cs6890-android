package com.chadmaughan.cs6890.assignment7.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.chadmaughan.cs6890.assignment7.model.Mathematician;

public class MathematicianDatabaseAdapter {
	
	// database columns
	public static final String ID = "id";
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String RESEARCH = "research";
	public static final String WIKI = "wiki";
	
	private static final String DATABASE_TABLE = "mathematicians";
	
	private Context context;
	private SQLiteDatabase database;
	private MathematicianDatabaseHelper helper;

	public MathematicianDatabaseAdapter(Context context) {
		this.context = context;
	}

	public MathematicianDatabaseAdapter open() throws SQLException {
		helper = new MathematicianDatabaseHelper(context);
		database = helper.getWritableDatabase();
		return this;
	}

	public void close() {
		helper.close();
	}

	public long create(String firstName, String lastName, String research, String wiki) {
		ContentValues initialValues = createContentValues(firstName, lastName, research, wiki);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public List<Mathematician> fetchMathematicians() {
		
        List<Mathematician> mathematicians = new ArrayList<Mathematician>();

        Cursor cursor = database.query(DATABASE_TABLE, new String[] { ID, FIRST_NAME, LAST_NAME, RESEARCH, WIKI }, null, null, null, null, null);
        cursor.moveToFirst();
        
        while(cursor.isAfterLast() == false) {
        	
        	Mathematician m = new Mathematician();
        	m.setId(cursor.getLong(0));
        	m.setFirstName(cursor.getString(1));
        	m.setLastName(cursor.getString(2));
        	m.setResearch(cursor.getString(3));
        	m.setUrl(cursor.getString(4));

        	mathematicians.add(m);

       	    cursor.moveToNext();
        }

        cursor.close();

        return mathematicians;
	}

	public int empty() {
		return database.delete(DATABASE_TABLE, null, null);
	}
	
	public Mathematician fetchMathematician(long id) throws SQLException {
		
		Cursor cursor = database.query(true, DATABASE_TABLE, new String[] {
				ID, FIRST_NAME, LAST_NAME, RESEARCH, WIKI },
				ID + "=" + id, null, null, null, null, null);
		
		if (cursor != null) {
			cursor.moveToFirst();
		}

		Mathematician m = new Mathematician();
    	m.setId(cursor.getLong(0));
    	m.setFirstName(cursor.getString(1));
    	m.setLastName(cursor.getString(2));
    	m.setResearch(cursor.getString(3));
    	m.setUrl(cursor.getString(4));

    	cursor.close();
    	
		return m;
	}

	private ContentValues createContentValues(String firstName, String lastName, String research, String wiki) {
		ContentValues values = new ContentValues();
		values.put(FIRST_NAME, firstName);
		values.put(LAST_NAME, lastName);
		values.put(RESEARCH, research);
		values.put(WIKI, wiki);
		return values;
	}
}
