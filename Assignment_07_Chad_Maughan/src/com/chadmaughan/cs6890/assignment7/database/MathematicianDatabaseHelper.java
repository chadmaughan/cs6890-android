package com.chadmaughan.cs6890.assignment7.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MathematicianDatabaseHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "mathematicians";
	
	//	<mathematician>
	//		<first_name>Kurt</first_name>
	//		<last_name>Godel</last_name>
	//		<research>mathematical logic</research>
	//		<wiki>http://en.wikipedia.org/wiki/Kurt_Godel</wiki>
	//	</mathematician>
    private static final String MATHEMATICIAN_TABLE_CREATE =
                "CREATE TABLE mathematicians (" +
                " id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " first_name TEXT, " +
                " last_name TEXT, " +
                " research TEXT, " +
                " wiki TEXT);";

    MathematicianDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(MATHEMATICIAN_TABLE_CREATE);
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
}
