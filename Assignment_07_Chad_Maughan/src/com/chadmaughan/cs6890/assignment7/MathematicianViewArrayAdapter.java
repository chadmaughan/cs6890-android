package com.chadmaughan.cs6890.assignment7;

import java.util.List;

import com.chadmaughan.cs6890.assignment7.model.Mathematician;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MathematicianViewArrayAdapter extends ArrayAdapter<Mathematician> {

	private int resourceId;
	private Context context;

	public MathematicianViewArrayAdapter(Context context, int resourceId, List<Mathematician> items) {
		super(context, resourceId, items);
		this.context = context;
		this.resourceId = resourceId;
	}

	@Override
    public long getItemId(int position) {
		Mathematician m = getItem(position);
        return m.getId(); 
    }

	public View getView(int position, View convertView, ViewGroup parent) {

		// mv is mathematician's view
		RelativeLayout mv;

		Mathematician m = getItem(position);

		if (convertView == null) {
			mv = new RelativeLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(inflater);
			vi.inflate(resourceId, mv, true);
		} 
		else {
			mv = (RelativeLayout) convertView;
		}

		// inflate the last_name and first_name text views of the relative layout
		((TextView) mv.findViewById(R.id.name)).setText(m.getFullName());

		// inflate the image view of the relative layout
		ImageView imgv = (ImageView) mv.findViewById(R.id.image);
		Resources res = context.getResources();

		// set the drawable of the image view to the appropriate image
		if (m.getLastName().equalsIgnoreCase("Church")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.alonzo_church));
		} 
		else if (m.getLastName().equalsIgnoreCase("Hilbert")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.david_hilbert));
		} 
		else if (m.getLastName().equalsIgnoreCase("Cantor")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.georg_cantor));
		} 
		else if (m.getLastName().equalsIgnoreCase("Peano")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.giuseppe_peano));
		} 
		else if (m.getLastName().equalsIgnoreCase("Godel")) {
			imgv.setImageDrawable(res.getDrawable(R.drawable.kurt_godel));
		} 
		else {
			imgv.setImageDrawable(res.getDrawable(R.drawable.menu_icon));
		}

		return mv;
	}
}
