package com.chadmaughan.cs6890.assignment7.model;

public class Mathematician {

	private long id;
	private String firstName;
	private String lastName;
	private String research;
	private String url;
	
	public Mathematician() {	
	}
	
	public Mathematician(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;		
	}

	public Mathematician(String firstName, String lastName, String url) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.url = url;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getResearch() {
		return research;
	}

	public void setResearch(String research) {
		this.research = research;
	}

	public String getFullName() {
		return this.firstName + " " + this.getLastName();
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	public String toString() {
		return firstName + " " + lastName;
	}
}
