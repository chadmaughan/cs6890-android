package com.chadmaughan.cs6890.assignment7;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.chadmaughan.cs6890.assignment7.database.MathematicianDatabaseAdapter;
import com.chadmaughan.cs6890.assignment7.model.Mathematician;

public class DisplayActivity extends ListActivity {

	private final static int OPEN_LINK = 200;
	private final static int SHOW_LINK = 201;
	
	private MathematicianDatabaseAdapter database;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        
        try {

        	database = new MathematicianDatabaseAdapter(this);
			database.open();
			
	        // fetch the mathematicians from the database
	        List<Mathematician> mathematicians = database.fetchMathematicians();

            // populate the view list
	        MathematicianViewArrayAdapter adapter = new MathematicianViewArrayAdapter(this, R.layout.mathematician_view, mathematicians);
	        this.getListView().setAdapter(adapter);
	        		
	        // allow for long-click context menu
	        registerForContextMenu(this.getListView());

	        // allow for single click
	        this.getListView().setOnItemClickListener(new OnItemClickListener() {
	        	
				public void onItemClick(AdapterView<?> parent, View target, int position, long id) {
					
					Log.v("MainActivity", "list id: " + id);
					
					// get the mathematician from the database
					Mathematician m = database.fetchMathematician(id);
					
					String url = m.getUrl();

					Log.v("MainActivity", "displaying: " + url);

					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(browserIntent);
				}        	
	        });

        }
        catch(Exception e) {
        	e.printStackTrace();
        }
    }
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {

		// get the mathematician from the database
		AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
		Mathematician m = database.fetchMathematician(adapterContextMenuInfo.id);

		Log.v("MainActivity", "Displaying menu for: " + m.getFullName());

		menu.setHeaderTitle(m.getFullName());
		menu.add(200, OPEN_LINK, Menu.NONE, "Go to wiki page");
		menu.add(200, SHOW_LINK , Menu.NONE, "Show wiki link");
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {		

		// get the mathematician from the database
		AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		Mathematician m = database.fetchMathematician(adapterContextMenuInfo.id);

		String url = m.getUrl();
		
		switch(item.getItemId()) {
		
			// open browser
			case OPEN_LINK:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(browserIntent);
				break;
	
			// show link
			case SHOW_LINK:
				Toast t = Toast.makeText(getApplicationContext(), url, Toast.LENGTH_SHORT);
				t.show();
				break;
				
			default:
				break;
		}

		return true;
	}
}