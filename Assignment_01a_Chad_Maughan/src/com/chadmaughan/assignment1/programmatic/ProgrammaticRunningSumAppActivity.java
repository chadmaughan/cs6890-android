package com.chadmaughan.assignment1.programmatic;

import java.util.logging.Level;
import java.util.logging.Logger;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class ProgrammaticRunningSumAppActivity extends Activity {

	private static Logger logger = Logger.getLogger(ProgrammaticRunningSumAppActivity.class.getName());

	private LinearLayout ll;
	private LinearLayout buttons;
	
	private TextView fromLabel;
	private EditText from;

	private TextView upToLabel;
	private EditText upTo;

	private TextView runSumLabel;
	private EditText runSum;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        LayoutParams lp = new LayoutParams(75, ViewGroup.LayoutParams.WRAP_CONTENT);

        ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        
        fromLabel = new TextView(this);
        fromLabel.setText("From:");
        ll.addView(fromLabel);

        from = new EditText(this);
        from.setText("");
        from.setLayoutParams(lp);
        from.setInputType(InputType.TYPE_CLASS_NUMBER);
        ll.addView(from);
        
        upToLabel = new TextView(this);
        upToLabel.setText("Up To:");
        ll.addView(upToLabel);

        upTo = new EditText(this);
        upTo.setText("");
        upTo.setLayoutParams(lp);
        upTo.setInputType(InputType.TYPE_CLASS_NUMBER);
        ll.addView(upTo);

        runSumLabel = new TextView(this);
        runSumLabel.setText("Run Sum:");
        ll.addView(runSumLabel);

        runSum = new EditText(this);
        runSum.setText("");
        runSum.setLayoutParams(lp);
        runSum.setInputType(InputType.TYPE_CLASS_NUMBER);
        runSum.setFocusable(false);
        runSum.setClickable(false);
        ll.addView(runSum);

        LayoutParams blp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final Button sum = new Button(this);
        sum.setText("Sum");
        sum.setLayoutParams(blp);
        sum.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            	int sum = 0;
            	
            	try {
	            	int fromInt = Integer.parseInt(from.getText().toString().trim());
	            	int toInt = Integer.parseInt(upTo.getText().toString().trim());
	
	            	// poor man's input validation
	            	if(fromInt < 0) {
	            		fromInt = fromInt * -1;
	            		from.setText(Integer.toString(fromInt));
	            	}
	            	
	            	// more poor man's input validation
	            	if(toInt < 0) {
	            		toInt = toInt * -1;
	            		upTo.setText(Integer.toString(toInt));
	            	}
	            	
	            	// swap them if they need to be
	            	if(fromInt > toInt) {
	            		int temp = fromInt;

	            		fromInt = toInt;
	            		from.setText(Integer.toString(fromInt));

	            		toInt = temp;
	            		upTo.setText(Integer.toString(toInt));
	            	}

	            	// calculate the sum
	            	if((toInt - fromInt) > 0) {
	            		for(int i = fromInt; i <= toInt; i++) {
	            			sum += i;
	            		}
	            	}
            	}
            	catch(NumberFormatException nfe) {
            		logger.log(Level.SEVERE, "Error parsing integers", nfe);
            	}

            	runSum.setText(Integer.toString(sum));
            }
        });

        final Button cancel = new Button(this);
        cancel.setText("Cancel");
        cancel.setLayoutParams(blp);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	resetFields();
            }
        });

        buttons = new LinearLayout(this);
        buttons.setOrientation(LinearLayout.HORIZONTAL);
        buttons.addView(sum);
        buttons.addView(cancel);

        ll.addView(buttons);
        
        setContentView(ll);
    }
    
    private void resetFields() {
    	this.from.setText("");
    	this.upTo.setText("");
    	this.runSum.setText("");
    }
}