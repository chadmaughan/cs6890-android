package com.chadmaughan.cs6890.assignment3;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

public class DisplayActivity extends ListActivity {

	private static String[] text = null;

	@Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        setContentView(R.layout.display);

		Intent i = this.getIntent();
		String key = i.getExtras().getString("key");

		Resources resources = getResources();
        String[] rumiNumbers = resources.getStringArray(R.array.list);

		Log.v("DisplayActivity", "using key: " + key);

        // q77
		if(key.equals(rumiNumbers[0])) {
			text = resources.getStringArray(R.array.q77);
		}
		// q82
		else if(key.equals(rumiNumbers[1])) {
			text = resources.getStringArray(R.array.q82);
		}
		// q116
		else if(key.equals(rumiNumbers[2])) {
			text = resources.getStringArray(R.array.q116);
		}
		// q494
		else if(key.equals(rumiNumbers[3])) {
			text = resources.getStringArray(R.array.q494);
		}
		// q1082
		else if(key.equals(rumiNumbers[4])) {
			text = resources.getStringArray(R.array.q1082);
		}
		else {
			Log.v("DisplayActivity", "unknown key: " + key);
		}
		
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, text);
        this.getListView().setAdapter(adapter);

	}    
}
