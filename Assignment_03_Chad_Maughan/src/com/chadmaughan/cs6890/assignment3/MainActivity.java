package com.chadmaughan.cs6890.assignment3;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

	private static String[] rumiNumbers = null;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Resources resources = getResources();
        rumiNumbers = resources.getStringArray(R.array.list);
        
        this.getListView().setTextFilterEnabled(true);
        this.getListView().setOnItemClickListener(new OnItemClickListener() {
        	
			public void onItemClick(AdapterView<?> parent, View target, int position, long id) {

				CharSequence text = ((TextView) target).getText();
				Toast t = Toast.makeText(getApplicationContext(), "Displaying " + text, Toast.LENGTH_SHORT);
				t.show();

				Log.v("MainActivity", "displaying: " + text + ", position: " + position + ", id:= " + id);

				Intent displayIntent = new Intent(MainActivity.this, DisplayActivity.class);
				displayIntent.putExtra("key", text);
				
				startActivity(displayIntent);
			}        	
        });
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, rumiNumbers);
        this.getListView().setAdapter(adapter);
    }
}