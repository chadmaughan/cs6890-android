package com.chadmaughan.assignment1.declaritive;

import java.util.logging.Level;
import java.util.logging.Logger;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class XMLRunningSumAppActivity extends Activity {
	
	private static Logger logger = Logger.getLogger(XMLRunningSumAppActivity.class.getName());
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

    	super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        final Button sum = (Button) findViewById(R.id.sum);
        sum.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            	int sum = 0;
            	
            	try {
                	EditText from = (EditText) findViewById(R.id.from);
                	EditText upTo = (EditText) findViewById(R.id.upTo);

                	int fromInt = Integer.parseInt(from.getText().toString().trim());
	            	int toInt = Integer.parseInt(upTo.getText().toString().trim());
	
	            	// poor man's input validation
	            	if(fromInt < 0) {
	            		fromInt = fromInt * -1;
	            		from.setText(Integer.toString(fromInt));
	            	}
	            	
	            	// more poor man's input validation
	            	if(toInt < 0) {
	            		toInt = toInt * -1;
	            		upTo.setText(Integer.toString(toInt));
	            	}
	            	
	            	// swap them if they need to be
	            	if(fromInt > toInt) {
	            		int temp = fromInt;

	            		fromInt = toInt;
	            		from.setText(Integer.toString(fromInt));

	            		toInt = temp;
	            		upTo.setText(Integer.toString(toInt));
	            	}

	            	// calculate the sum
	            	if((toInt - fromInt) > 0) {
	            		for(int i = fromInt; i <= toInt; i++) {
	            			sum += i;
	            		}
	            	}
            	}
            	catch(NumberFormatException nfe) {
            		logger.log(Level.SEVERE, "Error parsing integers", nfe);
            	}

            	EditText runSum = (EditText) findViewById(R.id.runSum);
            	runSum.setText(Integer.toString(sum));
            }
        });

        final Button cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	resetFields();
            }
        });
    }
    
    private void resetFields() {
    	EditText from = (EditText) findViewById(R.id.from);
    	EditText upTo = (EditText) findViewById(R.id.upTo);
    	EditText runSum = (EditText) findViewById(R.id.runSum);
    	
    	from.setText("");
    	upTo.setText("");
    	runSum.setText("");
    }
}